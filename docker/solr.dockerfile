ARG SOLR_VERSION=9.4.0

FROM alpine/git as clone

WORKDIR /harvester
ARG HARVESTER_BRANCH=main

ARG GITLAB_TOKEN=glpat-huRHAixVbqkzKYiQvpGX
RUN git clone --branch ${HARVESTER_BRANCH} --single-branch https://oauth2:$GITLAB_TOKEN@git.rwth-aachen.de/nfdi4earth/onestop4all/onestop4all-harvester.git

FROM solr:$SOLR_VERSION

COPY --from=clone /harvester/onestop4all-harvester/index/solr/metadata /var/solr/data/metadata

USER root

# include a very basic security.json file
COPY ./docker/security.json /var/solr/data/security.json

RUN chown -R solr /var/solr/data

USER solr
