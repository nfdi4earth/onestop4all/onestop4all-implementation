const fs = require("fs");

const KNOWLEDGE_HUB_BASE_URI = "{{_KNOWLEDGE_HUB_ENDPOINT_REPLACE_}}/objects/n4e/";

// the path to the index.html which is served by the SPA
const indexFile = "/usr/share/nginx/html/index.html";
const html = fs.readFileSync(indexFile);

function serializeLinkValue(value, relation, relType, profile) {
    /* build the invidual entries for the signposting link HTTP header*/
    let linkStr = `<${value}> ; rel="${relation}"`;
    if (relType !== undefined && typeof relType === "string" && relType.length > 0) {
        linkStr += ` ; type="${relType}"`;
    }
    if (profile !== undefined && typeof profile === "string" && profile.length > 0) {
        linkStr += ` ; profile="${profile}"`;
    }
    return linkStr;
}

async function addSignposting(r) {
    /* this function is called by the NGINX js_content directive which passes the
     * HTTP request as r argument */
    // execute the functionality to add custom HTTP headers only if the requested
    // URL matches the path under which the landing pages of dataset are accessed.
    // Otherwise return the default response without any changes (see below)
    if (r.uri.startsWith("/result/")) {
        const id = r.uri.replace("/result/", "");
        const apiURI = KNOWLEDGE_HUB_BASE_URI + id;
        const links = [serializeLinkValue(apiURI, "describedby", "application/ld+json")];
        if (links !== undefined && Array.isArray(links)) {
            r.headersOut["Link"] = links.join(", ");
        }
    }
    r.headersOut["Content-Type"] = "text/html";
    r.return(200, html);
}

export default { addSignposting };
