import os

from fastapi import FastAPI, Request
import requests
from requests.auth import HTTPBasicAuth
from uvicorn import run

SOLR_URL = os.getenv(
    "SOLR_URL",
    "https://onestop4all.test.n4e.geo.tu-dresden.de/solr",
)
SOLR_CORE = os.getenv("SOLR_CORE", "metadata")
SOLR_USER = os.getenv("SOLR_USER")
SOLR_PASS = os.getenv("SOLR_PASS")

app = FastAPI()

# evaluate if the environment variables are set
for env_var in [SOLR_URL, SOLR_USER, SOLR_PASS]:
    if env_var is None:
        raise ValueError(f"{env_var} environment variable is not set")


@app.get("/")
@app.get("/fastapi")
@app.get("/search")
def search(request: Request):
    """
    Search the Solr index via API.
    Passes all query parameters directly to Solr, including multi-value parameters.
    """
    params = []
    for key, value in request.query_params.multi_items():
        params.append((key, value))
    params.append(("wt", "json"))

    kwargs = {"params": params}
    # add authentication if user and password are set
    if all([SOLR_USER, SOLR_PASS]):
        kwargs["auth"] = HTTPBasicAuth(SOLR_USER, SOLR_PASS)

    response = requests.get(
        f"{SOLR_URL.rstrip('/')}/{SOLR_CORE}/select",
        **kwargs,
    )
    return response.json()


if __name__ == "__main__":

    run(app, host="0.0.0.0", port=8000)
