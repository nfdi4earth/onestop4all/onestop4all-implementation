FROM python:3.11-slim

WORKDIR /app

COPY ./docker/fastapi.py ./app.py
COPY ./docker/fastapi-requirements.txt .

RUN pip install -r fastapi-requirements.txt

CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "8000"]