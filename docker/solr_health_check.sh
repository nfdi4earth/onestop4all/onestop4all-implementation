#!/bin/bash
URL=http://localhost:8983/solr/metadata/admin/ping
# check if SOLR_AUTH is defined and it to curl-command accordingly
if [ -n "$SOLR_AUTH" ]; then
    curl -u "$SOLR_AUTH" --fail $URL || exit 1
else
    curl --fail $URL || exit 1
fi
