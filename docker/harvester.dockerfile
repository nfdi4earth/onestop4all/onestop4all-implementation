# Clone project
# #############
FROM alpine/git as clone

WORKDIR /harvester

ARG HARVESTER_BRANCH "main"

ARG GITLAB_TOKEN=glpat-huRHAixVbqkzKYiQvpGX
RUN git clone --branch ${HARVESTER_BRANCH} --single-branch https://oauth2:$GITLAB_TOKEN@git.rwth-aachen.de/nfdi4earth/onestop4all/onestop4all-harvester.git

# Setup Harvester
# ###############
FROM python:3.13.1-slim-bullseye

# setting of env-vars is obsolete when started via docker compose
ENV SOLR_URL "http://localhost:8983/solr/"
ENV SOLR_CORE "metadata"

# get source code of package
COPY --from=clone /harvester/onestop4all-harvester /harvester

# set workdir
WORKDIR /harvester

# install package
RUN pip install setuptools .

# Install Crontab

RUN apt-get update && \
    apt-get -y install cron

COPY docker/crontab /etc/cron.d/crontab

# adapt permissions crontab && give crontab the relevant config file
RUN chmod 0644 /etc/cron.d/crontab && crontab /etc/cron.d/crontab

# init logs file
RUN touch /tmp/harvest.log

#   cron reads envs only here -> start cron -> forward logs as container output
CMD env >> /etc/environment && cron && tail -f /tmp/harvest.log
