FROM node:20-slim AS base
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable
RUN corepack prepare pnpm@8.15.7 --activate
COPY . /app
WORKDIR /app
RUN pnpm install --frozen-lockfile
RUN pnpm build

FROM nginx:alpine
ARG CORDRA_ENDPOINT

ENV SOLR_URL="http://localhost:8983/solr"
ENV SOLR_CORE_SELECTOR="metadata"
COPY ./docker/signposting.js /usr/share/nginx/njs/signposting.js
# for signposting configuration install nginx additional module
RUN apk add nginx-module-njs && \
    # make sure the nginx njs extension is loaded
    sed -i.old '1s;^;load_module modules/ngx_http_js_module.so\;\n;' /etc/nginx/nginx.conf && \
    # replace placeholder for KH Cordra endpoint
    sed -i "s%{{_KNOWLEDGE_HUB_ENDPOINT_REPLACE_}}%$CORDRA_ENDPOINT%g" /usr/share/nginx/njs/signposting.js
# copy nginx config for OneStop4All entry
COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf
# copy build
COPY --from=base /app/dist/www /usr/share/nginx/html
# copy bootstrap script
COPY docker/bootstrap.sh /docker-entrypoint.d/
RUN chmod 0775 /docker-entrypoint.d/bootstrap.sh
