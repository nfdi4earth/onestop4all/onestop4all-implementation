# OneStop4All
The OneStop4All is the primary visual and user-friendly [NFDI4Earth](https://www.nfdi4earth.de/) access point. It offers a coherent view on and points to all relevant Earth System Sciences (ESS) RDM resources provided by NFDI4Earth members and the ESS community, such as data repositories, software tools, information on Research Data Management (RDM), education and training materials. Learn more about our [Mission](https://nfdi4earth.de/about-us) and the [Resources](https://nfdi4earth.de/2facilitate/onestop4all).

You can install the OneStop4All using Docker or by installing the backend and frontend separately.

## Architecture Diagram

The components are implemented as Docker containers (see [docker-compose.yml](docker-compose.yml)). The diagram provides a brief overview of the data flow and orchestration of the containers.

```mermaid

%%{ init: {  'themeVariables': { 'edgeLabelBackground': 'white'} } }%%


graph LR;
    A[User - Browser] -->|HTTPS request| B[Frontend - PORT: 80];
    B[Frontend - PORT: 80] -->|HTTPS response| A[User - Browser];

    B -->|request data| C[FastAPI - PORT: 8877];
    C[FastAPI - PORT: 8877] -->|retrieve data| B;

    C -->|query & authenticate| D[Index - PORT: 8983];
    D[Index - PORT: 8983] -->|retrieve data| C;

    E[Indexer] -->|index data| D;
    H(SPARQL endpoint) -->|retrieve data| E;

    subgraph KnowledgeHub
        direction LR
        G[Triplestore] <-->|query| H(SPARQL endpoint);
    end
    style KnowledgeHub fill:#fff,stroke:#333,stroke-width:1px;
    classDef default fill:#f3f3f3,stroke:#000,stroke-width:1px,color:#000;
```

## Very quick start

```bash
$ git clone https://git.rwth-aachen.de/nfdi4earth/onestop4all/onestop4all-implementation.git
$ cd onestop4all-implementation
$ git checkout develop
$ docker compose build --no-cache //Only needed if there any changes in the source Code
$ docker compose up
```

You also need to install and start cors-anywhere to avoid missing content (e.g. Living Handbook articles not showing up). Follow the steps mentioned [here](https://git.rwth-aachen.de/nfdi4earth/onestop4all/onestop4all-implementation/-/issues/111).

The application should be accessible under localhost.

## Not so quick start

Ensure that you have [Node](https://nodejs.org/en/) (Version 16 or later) and [pnpm](https://pnpm.io/) (Version 8.x) installed.

Then execute the following commands to get started:

Step 1: Install backend. First, you need to create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#clone-repository-using-personal-access-token). Then, you can clone the repository using the following command.
```bash
$ git clone https://<username>@git.rwth-aachen.de/nfdi4earth/onestop4all/onestop4all-harvester.git
```
### Update backend

The stable branch is `main` and current developments can be seen in `feature/new_harvesters`. Updates in the main branch require the following steps:

1_1 `docker volume ls` to find the exact name of the volume.

1_2 `docker volume rm <onestop4all-harvester_devcontainer_data>` to remove the volume. If the volume is in use by a container, run `docker stop <CONTAINER ID>` and `docker rm <CONTAINER ID>`. Then, again `docker volume rm <onestop4all-harvester_devcontainer_data>`.

1_3 `git pull`

Then, start Docker Desktop (In case you're using Windows) and open the folder "onestop4all-harvester" in Visual Studio Code, press F1 and "Rebuild and reopen in Container".
Next, open the file "harvest.py", press F5 and "Debug the currently active Python file". These steps will build the indices based on the data from the Knowledge Hub. Currently, building indicses takes around 10 seconds.

Step 2: Install web app. Finally, you need to switch to the branch "develop", which should be more or less stable.
```bash
$ git clone https://git.rwth-aachen.de/nfdi4earth/onestop4all/onestop4all-implementation.git
$ cd onestop4all-implementation
$ git checkout develop
$ pnpm install
```
If you are using Windows, you will now need to change line 35 in the file `.eslintrc` from `"linebreak-style": ["error", "unix"]` to `"linebreak-style": ["error", "windows"]`
```bash
$ pnpm run dev
```

The app should now be available under [http://localhost:5173/](http://localhost:5173/).

### With docker (currently under development)

```bash
docker build -t "onestop4all-ui:latest" .
docker run --rm -p 80:80 -e SOLR_URL="http://localhost:8080/http://localhost:8983/solr"  onestop4all-ui:latest
```

## Solr

### BasicAuth

The Solr index is 'protected' with the built-in [BasicAuth plugin](https://solr.apache.org/guide/solr/latest/deployment-guide/basic-authentication-plugin.html).

Things to observe regarding this:

#### Default setup

The docker compose setup ships automatically with a basic setup for that (see [security.json](docker/security.json)), BUT:

- the access is not restricted: `"blockUnknown": false,`
- it contains only the default credentials from the [docu](https://solr.apache.org/guide/solr/latest/deployment-guide/basic-authentication-plugin.html): `solr:SolrRocks`
- the actual credentials of Test- & Prod-Instance are defined during deployment and overwrite the default setup

#### docker compose healthheck

The healthcheck uses a dedicated script-file and distinguishes wether the env-variable `SOLR_AUTH` is defined or not, see:

- `test: ["CMD", "/bin/bash", "/solr_health_check.sh"]` (see [docker-compose.yaml](docker-compose.yaml))
- [solr_health_check.sh](docker/solr_health_check.sh)

#### Frontend

The fronted needs to know the credentials:

- `"auth": "solr:SolrRocks",` (see [config.json](src/public/config.json))

#### Indexer (a.k.a. Harvester)

The Indexer needs to know the credentials and the can be defined in the config or via env-variable:

- `- SOLR_AUTH=${SOLR_AUTH}` (see [docker-compose.yml](docker-compose.yml))

### FastAPI

The `fastapi` container is responsible for handling API requests. It is built using the FastAPI framework and is configured to run on port 8877. The container communicates with the Solr index and requires the following environment variables:

- `SOLR_URL`: The URL of the Solr instance.
- `SOLR_USER`: The username for Solr authentication.
- `SOLR_PASS`: The password for Solr authentication.

The `fastapi` container is defined in the `docker-compose.yml` file and is part of the `index_network` network. It can be accessed via port 8877 on the host machine.

The `fastapi` container is necessary to prevent exposing Solr credentials through UI requests. FastAPI ensures that credentials remain secure and are not exposed to the client side, by handling the authentication at Solr.

### Resources

#### BasicAuth

- [Documentation on Plugin](https://solr.apache.org/guide/solr/latest/deployment-guide/basic-authentication-plugin.html)
- [Documentation on predefined permissions](https://solr.apache.org/guide/solr/latest/deployment-guide/rule-based-authorization-plugin.html#predefined-permissions)
- [Online Solr password encryption tool](https://clemente-biondo.github.io)

## Development Guideline

1. always work on a seperate branch, with a name indicating whether it is a bug or a feature, e.g.: feature/harvest_datasets
2. add a short description to the section 'Latest features and bugfixes' of the [Changelog](Changelog)

## License

[Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0)

## Contact

[Markus Konkol, 52°North](m.konkol@52north.org)

## Contributors

Christin Henzen, Auriol Degbelo, Markus Konkol, Simon Jirka, Jan Schulte, Arne Vogt, Christoph Wagner, Jonas Grieb, Ralf Klammer, Tom Niers.

## Funding

This work has been funded by the German Research Foundation (DFG) through the project NFDI4Earth ( TA2 M2.1 , DFG project no. 460036893, https://www.nfdi4earth.de/) within the German National Research Data Infrastructure (NFDI, https://www.nfdi.de/).
