import { Box, ChakraProvider, Container, Flex } from "@open-pioneer/chakra-integration";
import { createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";
import { useMeasure } from "react-use";

import { BaseMenu } from "./components/BaseMenu/BaseMenu";
import { Footer } from "./components/Footer/Footer";
import { Header } from "./components/Header/Header";
import { Theme } from "./Theme";
import { FourOFour } from "./views/FourOFour/FourOFour";
import { Result } from "./views/Result/Result";
import { Redirecter } from "./views/Result/Redirecter";
import { SearchView } from "./views/Search/Search";
import { SearchState } from "./views/Search/SearchState";
import { Faq } from "./views/Start/Faq/Faq";
import { HowToEntryContent } from "./views/Start/HowTo/HowToEntryContent";
import { Wizard } from "./views/Start/HowTo/Wizard";
import { StartView } from "./views/Start/Start";
import { Data } from "./views/Start/HowTo/TBEPs/Data";
import { Tools } from "./views/Start/HowTo/TBEPs/Tools";
import { Education } from "./views/Start/HowTo/TBEPs/Education";

const basePath = "/";

const router = createBrowserRouter([
    {
        path: `${basePath}`,
        element: <Layout />,
        children: [
            {
                path: ``,
                element: <StartView />
            },
            {
                path: `search`,
                element: <SearchView />
            },
            {
                path: `result/:id`,
                element: <Result />
            },
            {
                path: `faq`,
                element: <Faq />
            },
            {
                path: `faq/:key`,
                element: <Faq />
            },
            {
                path: `howtoentry/:content`,
                element: <HowToEntryContent />
            },
            {
                path: `howtoentry/data`,
                element: <Data />
            },
            {
                path: `howtoentry/tools`,
                element: <Tools />
            },
            {
                path: `howtoentry/education`,
                element: <Education />
            },
            {
                path: `howtoentry/wizard`,
                element: <Wizard />
            },
            {
                path: `redirect/:markdown`,
                element: <Redirecter />
            },
            {
                path: "*",
                element: <FourOFour />
            }
        ]
    }
]);

export function AppUI() {
    return <RouterProvider router={router}></RouterProvider>;
}

function Layout() {
    const [headerRef, { height }] = useMeasure<HTMLElement>();
    return (
        <>
            <ChakraProvider theme={Theme}>
                <BaseMenu></BaseMenu>

                <Flex
                    as="header"
                    position="fixed"
                    w="100%"
                    bg="white"
                    zIndex="1001"
                    ref={headerRef}
                >
                    <Container maxW={{ base: "100%", custombreak: "80%" }}>
                        <Header></Header>
                    </Container>
                </Flex>

                <Box as="main" w="100%" pt={height + 15}>
                    <SearchState>
                        <Outlet />
                    </SearchState>
                </Box>

                <Box as="footer" w="100%">
                    <Footer></Footer>
                </Box>
            </ChakraProvider>
        </>
    );
}
