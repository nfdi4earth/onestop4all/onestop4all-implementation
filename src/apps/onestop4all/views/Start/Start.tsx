import { Box, Container, Flex, Image } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";
import { useEffect, useRef, useState } from "react";

import { SearchBar } from "../../components/SearchBar";
import { HowToEntries } from "./HowTo/HowToEntries";
import { ParticipateEntries } from "./ParticipateEntries/ParticipateEntries";
import { ResourceEntries } from "./ResourceEntry/ResourceEntries";

import { restoreScrollPosition } from "./ScrollPosition";
import { useSearchState } from "../Search/SearchState";
import { useTimeout } from "react-use";

export function StartView() {
    const scrollState = useRef<number>();
    const intl = useIntl();
    const searchState = useSearchState();

    const richTextIntl = {
        bold: (chunks: string[]) =>
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            chunks.map((chunk, i) => (<b key={`bold_${i}`}>{chunks[0]}</b>) as any)
    };

    const resourcesSectionRef = useRef<HTMLInputElement>(null);
    const scrollToResources = () => {
        if (resourcesSectionRef.current) {
            resourcesSectionRef.current.scrollIntoView({
                behavior: "smooth",
                block: "center"
            });
        }
    };

    useEffect(()=>{
        searchState.setSearchTerm("");
        searchState.setSelectedResourceTypes([]);
        searchState.setSelectedSubjects([]);
        searchState.setSelectedDataAccessTypes([]);
        searchState.setSelectedDataUploadTypes([]);
        searchState.setSelectedSupportedMetadataStandards([]);
        searchState.setSelectedLicenses([]);
        searchState.setSelectedCountries([]);
        searchState.setSelectedPublisher([]);
        searchState.setSelectedApi([]);
        searchState.setSpatialFilter([]);
        searchState.setTemporalFilter(undefined);
        searchState.setDoiOption(false);
        searchState.setSelectedDataFormatTypes([]);
        searchState.setSelectedFacet("find");
        searchState.setPrevPath("/");
    },[]);

    const missionLinksIntl = {
        NFDI4EarthLink: (
            <a
                className="link"
                target="_blank"
                key="1"
                rel="noreferrer"
                href={intl.formatMessage({
                    id: "start.mission.links.NFDI4EarthLinkUrl"
                })}
            >
                {intl.formatMessage({
                    id: "start.mission.links.NFDI4EarthLinkLabel"
                })}
            </a>
        ),
        MissionLink: (
            <a
                className="link"
                target="_blank"
                rel="noreferrer"
                key="2"
                href={intl.formatMessage({
                    id: "start.mission.links.MissionLinkUrl"
                })}
            >
                {intl.formatMessage({
                    id: "start.mission.links.MissionLinkLabel"
                })}
            </a>
        ),
        ResourcesLink: (
            <a
                className="link"
                target="_blank"
                rel="noreferrer"
                key="3"
                onClick={scrollToResources}
                style={{ cursor: "pointer" }}
            >
                {intl.formatMessage({
                    id: "start.mission.links.ResourcesLinkLabel"
                })}
            </a>
        )
    };

    const resourcesLinksIntl = {
        MembersLink: (
            <a
                className="link"
                target="_blank"
                key="1"
                rel="noreferrer"
                href={intl.formatMessage({
                    id: "start.resources.links.MembersLinkUrl"
                })}
            >
                {intl.formatMessage({
                    id: "start.resources.links.MembersLinkLabel"
                })}
            </a>
        ),
        CommunityLink: (
            <a
                className="link"
                target="_blank"
                key="2"
                rel="noreferrer"
                href={intl.formatMessage({
                    id: "start.resources.links.CommunityLinkUrl"
                })}
            >
                {intl.formatMessage({
                    id: "start.resources.links.CommunityLinkLabel"
                })}
            </a>
        )
    };

    /**Saves the scroll position when leaving the start page and restores it when you return */
    useEffect(() => {
        // Key to locate the value in the sessionStorage
        const key = "start-view-scroll-position";

        if (sessionStorage.getItem(key) != null) {
            // 100ms timeout so that everything is rendered
            setTimeout(() => {
                restoreScrollPosition(key);
            }, 100);
        }

        // saves new scroll position after every scroll
        const onScroll = (event: Event) => {
            scrollState.current = window.scrollY;
        };
        addEventListener("scroll", onScroll);

        /** when leaving the site, the old scroll position is saved in a ref,
         right now the component is rendered twice, the first time with scrollState.current = undefined,
         which is why the sessionStorage gets changed just for scrollState=some number */
        return () => {
            if (scrollState.current || scrollState.current == 0) {
                sessionStorage.setItem(key, scrollState.current.toString());
            }
            window.removeEventListener("scroll", onScroll);
        };
    }, []);

    return (
        <Box className="start-view">
            <Box position="relative">
                <Box className="header-image" />
                <Box w="100%" position="absolute" top="0">
                    <Container maxW={{ base: "100%", custombreak: "80%" }}>
                        <Flex
                            pt="3vh"
                            textAlign="center"
                            justifyContent="flex-end"
                        >
                            <Box
                                maxW={{ base: "70%", custombreak: "60%" }}
                                fontSize={{ base: "16px", custombreak: "24px" }}
                                color="white"
                            >
                                {intl.formatMessage({ id: "start.banner.slogan" }, richTextIntl)}
                            </Box>
                        </Flex>
                    </Container>
                </Box>
            </Box>

            <Box
                position="absolute"
                width="100%"
                marginTop={{ base: "-40px", custombreak: "-70px" }}
            >
                <Container maxW={{ base: "100%", custombreak: "80%" }}>
                    <SearchBar />
                </Container>
            </Box>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                <Box height="80px"></Box>

                <Box className="mission">
                    <Image className="bg-icon" alt="Bg icon" src="/bg-icon.png" />
                    <Box className="mission-text text-centered-box">
                    </Box>
                </Box>

                <HowToEntries/>
            </Container>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                <Box className="resources" ref={resourcesSectionRef}>
                    <Box className="text-centered-box">
                        <Box className="text-centered-box-header">
                            {intl.formatMessage({ id: "start.resources.title" })}
                        </Box>
                    </Box>
                    <Box pt="32px" className="resources-elements"> 
                        <ResourceEntries/>
                    </Box>
                </Box>
            </Container>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                {/*<Box className="graph">
                    <Box className="text-centered-box">
                        <Box className="text-centered-box-header">
                            {intl.formatMessage({ id: "start.graph.title" })}
                        </Box>
                        <Box className="text-centered-box-text">
                            {intl.formatMessage({ id: "start.graph.description" })}
                        </Box>
                    </Box>
                    <Box pt={"16px"}>
                        <ParentSize>
                            {(parent) => <NetworkGraph width={parent.width} height={500} />}
                        </ParentSize>
                    </Box>
                            </Box>*/}
                <ParticipateEntries lang={intl.locale} />
            </Container>
        </Box>
    );
}
