/* eslint-disable */
import { Box, SimpleGrid } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";

import { HowToEntry } from "./HowToEntry";

export const HowToEntries = () => {
    const intl = useIntl();

    return (
        <Box className="how-to">
            <Box className="text-centered-box" marginBottom={{ base: "5%", custombreak: "0%" }}>
                <Box className="text-centered-box-header">
                    {intl.formatMessage({ id: "start.how-to.title" })}
                </Box>
            </Box>
            <SimpleGrid
                columns={[1, null, 3]}
                spacing={5}
                //padding={"0px 0px"}
                marginTop={"1%"}
            >
                <HowToEntry 
                    title={"Discover datasets and repositories"} 
                    subheading={"Find resources, search data or take a guided tour on repositories"} 
                    id={"data"} 
                />
                <HowToEntry 
                    title={"Explore tools and services"} 
                    subheading={"Learn more about existing services and software, and how to use them"} 
                    id={"tools"} 
                />
                <HowToEntry 
                    title={"Improve your skills and start learning"} 
                    subheading={"Improve your skills with interactive self-learning materials and events"} 
                    id={"education"} 
                />
            </SimpleGrid>
        </Box>
    );
};
