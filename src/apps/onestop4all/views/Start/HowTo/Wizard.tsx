import { Box, Container, Flex, Spacer, Radio, RadioGroup, Stack, Button } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";
import { useEffect, useRef, useState } from "react";
import { createSearchParams, useNavigate, useSearchParams } from "react-router-dom";

import { ResultPaging } from "../../Search/ResultPaging/ResultPaging";
import { SearchResult } from "../../Search/SearchResult/SearchResult";
import {
    DEFAULT_PAGE_SIZE,
    UrlSearchParameterType,
    UrlSearchParams,
    useSearchState
} from "../../Search/SearchState";
import { SubjectFacet } from "../../Search/Facets/SubjectFacet/SubjectFacet";
import { SearchBar } from "../../../components/SearchBar";
import { DataAccessTypeFacet } from "../../Search/Facets/DataAccessTypeFacet/DataAccessTypeFacet";
import { MobileFilterMenu } from "../../Search/Facets/MobileFilterMenu/MobileFilterMenu";
import { DoiOptionFacet } from "../../Search/Facets/DoiOptionFacet/DoiOptionFacet";
import { DataFormatTypeFacet } from "../../Search/Facets/DataFormatTypeFacet/DataFormatTypeFacet";
import { ResourceType } from "../../../services/ResourceTypeUtils";

export function Wizard() {
    const searchState = useSearchState();
    const resultsHeaderRef = useRef(null);
    const [searchParams] = useSearchParams();
    const navigate = useNavigate();

    const intl = useIntl();
    const loadingText = intl.formatMessage({ id: "resource-type-header.loading" });

    useEffect(() => {
        const onBackButtonEvent = (e: any) => {
            e.preventDefault();
            navigate("/howtoentry/data");
        };
        window.addEventListener("popstate", onBackButtonEvent);
        searchState.setSelectedResourceTypes([ResourceType.Repos]);
        return () => {
            window.removeEventListener("popstate", onBackButtonEvent);
        };
    }, [navigate]);

    useEffect(() => {
        if (searchState.selectedResourceTypes[0] === ResourceType.Repos) {
            searchState.search();
        }
    }, [searchParams]);

    useEffect(() => {
        const params: UrlSearchParams = {}; 

        if (searchState.searchTerm) {
            params[UrlSearchParameterType.Searchterm] = searchState.searchTerm;
        }

        if (searchState.selectedResourceTypes.length > 0) {
            params[UrlSearchParameterType.ResourceType] = searchState.selectedResourceTypes;
        }

        if (searchState.selectedSubjects.length > 0) {
            params[UrlSearchParameterType.Subjects] = searchState.selectedSubjects;
        }

        if (searchState.selectableDataAccessTypes.length > 0) {
            params[UrlSearchParameterType.DataAccessTypes] = searchState.selectedDataAccessTypes;
        }

        if (searchState.selectableDataFormatTypes.length > 0) {
            params[UrlSearchParameterType.DataFormatTypes] = searchState.selectedDataFormatTypes;
        }

        if (searchState.pageSize && searchState.pageSize !== DEFAULT_PAGE_SIZE) {
            params[UrlSearchParameterType.PageSize] = `${searchState.pageSize}`;
        }

        if (searchState.pageStart && searchState.pageStart !== 0) {
            params[UrlSearchParameterType.PageStart] = `${searchState.pageStart}`;
        }

        if (searchState.doiOption) {
            params[UrlSearchParameterType.DoiOption] = `${searchState.doiOption}`;
        }

        navigate({
            search: `?${createSearchParams({ ...params })}`
        });
    }, [
        searchState.searchTerm,
        searchState.selectedSubjects,
        searchState.selectedResourceTypes,
        searchState.selectedDataAccessTypes,
        searchState.selectedDataFormatTypes,
        searchState.pageSize,
        searchState.pageStart,
        searchState.doiOption
    ]);

    const [openMenu, setOpenMenu] = useState(false);

    function switchFacet(){
        searchState.selectedFacet === "find" ? searchState.setSelectedFacet("publish") : searchState.setSelectedFacet("find");
        searchState.setSelectedSubjects([]);
        searchState.setSelectedDataFormatTypes([]);
        searchState.setDoiOption(false);
        searchState.setSelectedDataAccessTypes([]);
    }

    return (
        <Box className="search-view">
            <Box position="relative">
                <Box className="header-image" />
            </Box>

            <Box
                position="absolute"
                width="100%"
                marginTop={{ base: "-40px", custombreak: "-50px" }}
            >
                <Container maxW={{ base: "100%", custombreak: "80%" }}>
                    <SearchBar />
                </Container>
            </Box>

            <Box height={{ base: "100px", custombreak: "80px" }}></Box>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                <Box>
                    <h1>
                        {intl.formatMessage({
                            id: "search.wizard.title"
                        })}
                    </h1>
                </Box>
                <Box paddingY={"12px"}>
                    <Box>
                        <b>
                            {intl.formatMessage({
                                id: "search.wizard.question"
                            })}
                        </b>
                    </Box>
                    <RadioGroup onChange={() => {switchFacet();}} value={searchState.selectedFacet} pt={3}>
                        <Stack direction="column">
                            <Radio value="find">
                                {intl.formatMessage({
                                    id: "search.wizard.findData"
                                })}
                            </Radio>
                            <Radio value="publish">
                                {intl.formatMessage({
                                    id: "search.wizard.publishData"
                                })}
                            </Radio>
                        </Stack>
                    </RadioGroup>
                </Box>
                <Box>
                    {searchState.selectedFacet === "find" ? 
                        <Box>
                            <h2>
                                {intl.formatMessage({
                                    id: "search.wizard.findDataTitle"
                                })}
                            </h2>
                            <Box w={"70%"}>
                                <SubjectFacet 
                                    expanded={true} 
                                    wizard={true}
                                    fontSize={"15px"}
                                    opacity={0.9}
                                    question={"search.facets.subject-help.questionFindData"}
                                    note={"search.facets.subject-help.note"}
                                />
                            </Box>
                            <Box pt={5} w={"70%"}>
                                <DataFormatTypeFacet 
                                    expanded={true} 
                                    wizard={true}
                                    fontSize={"15px"}
                                    opacity={0.9}
                                    question={"search.facets.data-format-type-help.questionFindData"}
                                    note={"search.facets.data-format-type-help.noteFindData"}
                                />
                            </Box>
                            <Box pt={5} w={"70%"}>
                                <DoiOptionFacet 
                                    expanded={true} 
                                    wizard={true}
                                    fontSize={"15px"}
                                    opacity={0.9}
                                    question={"search.facets.doiOption-help.questionFindData"}
                                    note={"search.facets.doiOption-help.note"}
                                />
                            </Box>
                            <Box pt={5} w={"70%"}>
                                <DataAccessTypeFacet 
                                    expanded={true} 
                                    wizard={true}
                                    selectedWizard={searchState.selectedFacet}
                                    fontSize={"15px"}
                                    opacity={0.9}
                                    question={"search.facets.data-access-type-help.questionFindData"}
                                    note={"search.facets.data-access-type-help.note"}
                                />
                            </Box>
                        </Box>
                        :   <Box>
                            <h2>
                                {intl.formatMessage({
                                    id: "search.wizard.publishDataTitle"
                                })}
                            </h2>
                            <Box w={"70%"}>
                                <SubjectFacet 
                                    expanded={true} 
                                    wizard={true}
                                    fontSize={"15px"}
                                    opacity={0.9}
                                    question={"search.facets.subject-help.questionPublishData"}
                                    note={"search.facets.subject-help.note"}
                                />
                            </Box>
                            <Box pt={5} w={"70%"}>
                                <DataFormatTypeFacet 
                                    expanded={true} 
                                    wizard={true}
                                    fontSize={"15px"}
                                    opacity={0.9}
                                    question={"search.facets.data-format-type-help.questionPublishData"}
                                    note={"search.facets.data-format-type-help.notePublishData"}
                                />
                            </Box>
                            <Box pt={5} w={"70%"}>
                                <DoiOptionFacet 
                                    expanded={true} 
                                    wizard={true}
                                    fontSize={"15px"}
                                    opacity={0.9}
                                    question={"search.facets.doiOption-help.questionPublishData"}
                                    note={"search.facets.doiOption-help.note"}
                                />
                            </Box>
                            <Box pt={5} w={"70%"}>
                                <DataAccessTypeFacet 
                                    expanded={true} 
                                    wizard={true}
                                    selectedWizard={searchState.selectedFacet}
                                    fontSize={"15px"}
                                    opacity={0.9}
                                    question={"search.facets.data-access-type-help.questionPublishData"}
                                    note={"search.facets.data-access-type-help.note"}
                                />
                            </Box>
                        </Box>
                    }
                </Box>
                <Flex marginTop={20}>
                    {searchState.isLoaded ? (
                        <Box flex="1 1 100%" overflow="hidden">
                            <Flex flexDirection={{ base: "column", custombreak: "row" }}>
                                <Box className="results-count" ref={resultsHeaderRef}>
                                    {searchState.searchResults?.count}{" "}
                                    {intl.formatMessage({
                                        id: "search.results-header"
                                    })}
                                </Box>
                                <Box hideFrom="custombreak" padding="20px 0px">
                                    <ResultPaging refer={resultsHeaderRef}/>
                                </Box>
                                <Spacer />
                            </Flex>
                            <Box>
                                {searchState.searchResults?.results.map((e) => {
                                    return (
                                        <Box key={e.id}>
                                            <Box className="seperator" />
                                            <Box padding={{ base: "40px 0px" }}>
                                                <SearchResult item={e} />
                                            </Box>
                                        </Box>
                                    );
                                })}
                            </Box>
                            <Box className="seperator" />
                            <Box hideFrom="custombreak" padding="40px 0px">
                                <ResultPaging refer={resultsHeaderRef}/>
                            </Box>
                        </Box>
                    ) : (
                        <Box flex="1 1 100%" overflow="hidden">
                            {loadingText}
                        </Box>
                    )}

                    <Flex flex="0 0 30%" hideBelow="custombreak" flexDirection="column">
                        <Box>
                            <ResultPaging refer={resultsHeaderRef}/>
                        </Box>
                        <Spacer />
                        <Box>
                            <ResultPaging refer={resultsHeaderRef}/>
                        </Box>
                    </Flex>
                </Flex>
                <MobileFilterMenu
                    openMenu={openMenu}
                    menuClosed={() => setOpenMenu(false)}
                ></MobileFilterMenu>
            </Container>
        </Box>
    );
}
