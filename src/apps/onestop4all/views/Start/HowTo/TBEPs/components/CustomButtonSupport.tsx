import { Box } from "@open-pioneer/chakra-integration";
import { useLocation } from "react-router-dom";
import { useSearchState } from "../../../../Search/SearchState";
import { ResourceType } from "../../../../../services/ResourceTypeUtils";
import { useEffect, useState } from "react";
import { SupportForm } from "../../../../../components/SupportForm/SupportForm";

export const CustomButtonSupport = (props: { button_text: string; button_sub_text: string; type?:string}) => {
    const {button_text, button_sub_text, type} = props;
    const searchState = useSearchState();
    const location = useLocation();
    const [hovered, setHovered] = useState(false);
    const [openSupportForm, setOpenSupportForm] = useState(false);


    useEffect(() => {
        window.history.pushState(null, "", window.location.href);
        const handlePopState = (event:any) => {
            window.location.href = "/";
        };
        window.addEventListener("popstate", handlePopState);
        return () => {
            window.removeEventListener("popstate", handlePopState);
        };
    }, []);

    return (
        <Box w={"100%"}
            className={`how-to-entry ${hovered ? "hover" : "default"}`}
            backgroundColor={hovered ? "gray.100" : "white"}
            boxShadow="md"
            onMouseLeave={() => setHovered(false)}
            onMouseEnter={() => setHovered(true)}
            onClick={() => {
                searchState.setPrevPath(location.pathname);
                if (type === "repo")
                    searchState.setSelectedResourceTypes([ResourceType.Repos]);
                else if (type === "data")
                    searchState.setSelectedResourceTypes([ResourceType.Datasets]);
                else if (type === "edu")
                    searchState.setSelectedResourceTypes([ResourceType.Learning_Resource]);
                else if (type === "tools")
                    searchState.setSelectedResourceTypes([ResourceType.Tools]);
                setOpenSupportForm(true); 
            }}
        >
        
            <Box className="frame">
                <Box className="heading">
                    {button_text}
                </Box>
                <Box className="abstract">
                    {button_sub_text}
                </Box>
                <SupportForm openForm={openSupportForm} menuClosed={() => { setOpenSupportForm(false); setHovered(false); }} />
            </Box>
        </Box>
    );
};
