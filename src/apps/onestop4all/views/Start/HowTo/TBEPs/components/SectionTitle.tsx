import { Heading } from "@open-pioneer/chakra-integration";

export const SectionTitle = (props: {section_title:string}) => {
    const section_title = props.section_title;
    return (
        <Heading p="10px" as="h5" size="sm" mb={5} color="#5A5A5A">
            {section_title}
        </Heading>
    );
};