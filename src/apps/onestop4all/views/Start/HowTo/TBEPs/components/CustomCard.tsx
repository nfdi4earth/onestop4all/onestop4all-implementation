import { Card, CardBody, Text, Image, Flex } from "@open-pioneer/chakra-integration";
import { Link } from "react-router-dom";

export const CustomCard = (props: { thumbnail_link: string; card_type: string; card_title: string; content_link: string; minH?: string; open_externally?: boolean }) => {
    const {thumbnail_link, card_type, card_title, content_link, minH, open_externally} = props;
    return (
        <Card 
            //maxW="250px" 
            border="1px" 
            borderColor="gray.200" 
            borderRadius={"md"}
            as={Link}
            to={content_link}
            target={open_externally ? "_blank" : ""}
            style={{ textDecoration: "none" }}
        >
            <Flex 
                minHeight={minH ? minH : "150px"}
                alignItems="center" 
                justifyContent="center"
            >
                <Image src={thumbnail_link} padding="5px 5px 5px" />
            </Flex>
            <CardBody>
                <Text color="GrayText" mb={4}>
                    {card_type}
                </Text>
                <Text as="b" color="#05668d">
                    {card_title}
                </Text>
            </CardBody>
        </Card>
    );
};
