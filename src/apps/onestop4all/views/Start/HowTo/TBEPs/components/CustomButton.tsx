import { Text, Box } from "@open-pioneer/chakra-integration";
import { Link, useLocation } from "react-router-dom";
import { useSearchState } from "../../../../Search/SearchState";
import { ResourceType } from "../../../../../services/ResourceTypeUtils";
import { useEffect, useState } from "react";

export const CustomButton = (props: { button_text: string; button_sub_text: string; button_link: string; open_externally?: boolean, type?:string}) => {
    const {button_text, button_sub_text, button_link, open_externally, type} = props;
    const searchState = useSearchState();
    const location = useLocation();
    const [hovered, setHovered] = useState(false);

    useEffect(() => {
        window.history.pushState(null, "", window.location.href);
        const handlePopState = (event:any) => {
            window.location.href = "/";
        };
        window.addEventListener("popstate", handlePopState);
        return () => {
            window.removeEventListener("popstate", handlePopState);
        };
    }, []);

    return (
        <Box w={"100%"}
            className={`how-to-entry ${hovered ? "hover" : "default"}`}
            backgroundColor={hovered ? "gray.100" : "white"}
            boxShadow="md"
            onMouseLeave={() => setHovered(false)}
            onMouseEnter={() => setHovered(true)}
            onClick={() => {
                searchState.setPrevPath(location.pathname);
                if (type === "repo")
                    searchState.setSelectedResourceTypes([ResourceType.Repos]);
                else if (type === "data")
                    searchState.setSelectedResourceTypes([ResourceType.Datasets]);
                else if (type === "edu")
                    searchState.setSelectedResourceTypes([ResourceType.Learning_Resource]);
                else if (type === "tools")
                    searchState.setSelectedResourceTypes([ResourceType.Tools]);
            }}
        >
            <Link style={{ color: "#05668D", textDecoration: "none"}} to={button_link} target={open_externally ? "_blank" : ""}>
                <Box className="frame">
                    <Box className="heading">
                        {button_text}
                    </Box>
                    <Box className="abstract">
                        {button_sub_text}
                    </Box>
                </Box>
            </Link>
        </Box>
    );
};
