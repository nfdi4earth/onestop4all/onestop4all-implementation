import { Stack, Text } from "@open-pioneer/chakra-integration";

export const PageTitle = (props: {tbep_title: string}) => {
    const {tbep_title} = props;
    return (
        <Stack p="10px">
            <Text fontSize="2xl" as="b" color="#05668D">
                {tbep_title}
            </Text>
            <br />
        </Stack>
    );
};
