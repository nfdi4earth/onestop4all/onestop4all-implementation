/* eslint-disable */
import { Box, Container, SimpleGrid } from "@open-pioneer/chakra-integration";

import { SearchBar } from "../../../../components/SearchBar";
import { BackToStartingpage } from "../../../../components/BackToStartingpage/BackToStartingpage";
import { PageTitle } from "./components/PageTitle";
import { CustomButton } from "./components/CustomButton";
import { CustomCard } from "./components/CustomCard";
import { SectionTitle } from "./components/SectionTitle";


export const Education = () => {
    const minHeight = "240px";
    return (
        <Box className="search-view" w={"100%"}>
            <Box position="relative">
                <Box className="header-image" />
            </Box>

            <Box
                position="absolute"
                width="100%"
                marginTop={{ base: "-40px", custombreak: "-50px" }}
            >
                <Container maxW={{ base: "100%", custombreak: "80%" }}>
                    <SearchBar />
                </Container>
            </Box>

            <Box height={{ base: "100px", custombreak: "80px" }}></Box>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                <BackToStartingpage />

                <div>
                    <div>
                        <PageTitle
                            tbep_title="Improve your skills and start learning"
                        />
                        <SimpleGrid
                            p="10px"
                            columns={[1, 2]}
                            spacing="5"
                            width={"100%"}
                        >
                            <CustomButton
                                button_text="Enter the interactive learning portal"
                                button_sub_text="Start learning in our NFDI4Earth EduTrain portal to improve your spatial data analysis and management skills"
                                button_link="https://edutrain.nfdi4earth.de/"
                                open_externally={true}
                            />
                            <CustomButton
                                button_text="Discover curated community resources"
                                button_sub_text="Find open reliable educational resources from sources such as MIT open courseware, EarthLab, and NASA’s Earthdata"
                                button_link="/search?resourcetype=Learning+Resource&sort="
                                type="edu"
                            />
                        </SimpleGrid>
                    </div>
                    <br />
                    <div>
                        <SectionTitle section_title="Start learning with highlighted courses" />
                        <SimpleGrid
                            p="10px"
                            columns={[1, 2, 3, 4, 5]}
                            spacing={5}
                            width={"100%"}
                        >
                            <CustomCard
                                thumbnail_link="https://edutrain.nfdi4earth.de/asset-v1:NFDI4Earth+20230827PO+Self-Paced+type@asset+block@course_cards__7_.JPG"
                                card_type="Course"
                                card_title="Python onboarding"
                                content_link="https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230827PO+Self-Paced/about"
                                open_externally={true}
                            />
                            <CustomCard
                                thumbnail_link="https://edutrain.nfdi4earth.de/asset-v1:NFDI4Earth+20230502CP+self-paced+type@asset+block@Slide5.JPG"
                                card_type="Course"
                                card_title="How to create publishable netcdf-data"
                                content_link="https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230502CP+self-paced/about"
                                open_externally={true}
                            />
                            <CustomCard
                                thumbnail_link="https://edutrain.nfdi4earth.de/asset-v1:NFDI4Earth+20230412FS+self-paced+type@asset+block@course_cards__4_.JPG"
                                card_type="Course"
                                card_title="Image processing"
                                content_link="https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230412FS+self-paced/about"
                                open_externally={true}
                            />
                            <CustomCard
                                thumbnail_link="https://edutrain.nfdi4earth.de/asset-v1:NFDI4Earth+20230828PFSDA+Self-Paced+type@asset+block@Slide8.JPG"
                                card_type="Course"
                                card_title="Python for spatial data analysis"
                                content_link="https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230828PFSDA+Self-Paced/about"
                                open_externally={true}
                            />
                            <CustomCard
                                thumbnail_link="https://edutrain.nfdi4earth.de/asset-v1:NFDI4Earth+20230828UDC+Self-Paced+type@asset+block@course_cards__2_.JPG"
                                card_type="Course"
                                card_title="Understanding data chunking"
                                content_link="https://edutrain.nfdi4earth.de/courses/course-v1:NFDI4Earth+20230828UDC+Self-Paced/about"
                                open_externally={true}
                            />
                        </SimpleGrid>
                    </div>
                    <br />
                    <div>
                        <SectionTitle section_title="Attend virtual or on-site training events" />
                        <SimpleGrid
                            p="10px"
                            columns={[1, 2, 3, 4, 5]}
                            spacing={5}
                            width={"100%"}
                        >
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Academy_LegalWorkshop.png"
                                card_type="Training event"
                                card_title="Legal Workshop Series April 2025 – April 2026 (virtual, open events)"
                                content_link="https://www.nfdi4earth.de/?view=article&id=419&catid=9"
                                minH={minHeight}
                                open_externally={true}
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Academy_WinterSchool.png"
                                card_type="Training event"
                                card_title="Winter School on RDM 18th – 20th March 2025 (Cologne, 2nd cohort event)"
                                content_link="https://www.nfdi4earth.de/?view=article&id=417&catid=9"
                                minH={minHeight}
                                open_externally={true}
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Academy_Coffee_lecture.jpg"
                                card_type="Training event"
                                card_title="Coffee Lectures October 2024 – March 2025 (virtual, open events)"
                                content_link="https://www.nfdi4earth.de/?view=article&id=366&catid=9"
                                minH={minHeight}
                                open_externally={true}
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Academy_Workshop_Datacubes.jpg"
                                card_type="Training event"
                                card_title="Workshop DataCubes 28th – 29th August 2024 (virtual, Academy event)"
                                content_link="https://www.nfdi4earth.de/?view=article&id=401&catid=9"
                                minH={minHeight}
                                open_externally={true}
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Academy_Hackathon.jpg"
                                card_type="Training event"
                                card_title="Cross-Community Hackathon 23rd – 5th December 2023 (open event)"
                                content_link="https://www.nfdi4earth.de/?view=article&id=365&catid=9"
                                minH={minHeight}
                                open_externally={true}
                            />
                        </SimpleGrid>
                    </div>
                    <br />
                    <div>
                        <SectionTitle section_title="Learn more about education and training" />
                        <SimpleGrid
                            p="10px"
                            columns={[1, 2, 3]}
                            spacing={5}
                            width={"100%"}
                        >
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                                card_type="Article"
                                card_title="What is the NFDI4Earth Academy"
                                content_link="/result/lhb-docs-N4E_Academy.md"
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                                card_type="Article"
                                card_title="Explore the Academy and connect with peers"
                                content_link="/result/lhb-docs-Participate_Academy.md"
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/contribute.png"
                                card_type="Article"
                                card_title="Suggest your learning material"
                                content_link="/result/lhb-docs-N4E_Suggest_an_OER.md"
                            />
                        </SimpleGrid>
                    </div>
                </div>
            </Container>
        </Box>
    );
};


