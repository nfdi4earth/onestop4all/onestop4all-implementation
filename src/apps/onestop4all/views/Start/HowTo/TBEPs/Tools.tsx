/* eslint-disable */
import { Box, Container, SimpleGrid } from "@open-pioneer/chakra-integration";

import { SearchBar } from "../../../../components/SearchBar";
import { BackToStartingpage } from "../../../../components/BackToStartingpage/BackToStartingpage";
import { PageTitle } from "./components/PageTitle";
import { CustomButton } from "./components/CustomButton";
import { CustomButtonSupport } from "./components/CustomButtonSupport";
import { CustomCard } from "./components/CustomCard";
import { SectionTitle } from "./components/SectionTitle";


export const Tools = () => {
    const minHeight = "190px";
    return (
        <Box className="search-view" w={"100%"}>
            <Box position="relative">
                <Box className="header-image" />
            </Box>

            <Box
                position="absolute"
                width="100%"
                marginTop={{ base: "-40px", custombreak: "-50px" }}
            >
                <Container maxW={{ base: "100%", custombreak: "80%" }}>
                    <SearchBar />
                </Container>
            </Box>

            <Box height={{ base: "100px", custombreak: "80px" }}></Box>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                <BackToStartingpage />

            <div>
                <div>
                    <PageTitle
                        tbep_title="Explore tools and services"
                    />
                    <SimpleGrid
                        p="10px"
                        columns={[1, 2, 3]}
                        spacing={5}
                        width={"100%"}
                    >
                        <CustomButton
                            button_text="Discover semantic resources"
                            button_sub_text="Discover relevant resources as linked data from the NFDI4Earth Knowledge Hub service using SPARQL"
                            button_link="https://knowledgehub.nfdi4earth.de/"
                            open_externally={true}
                        />
                        <CustomButton
                            button_text="Discover community tools and software"
                            button_sub_text="Find and reuse tools and software to facilitate your Earth System Science data research"
                            button_link="/search?resourcetype=Tool+%2F+Software&sort="
                            type="tools"
                        />
                        <CustomButtonSupport
                            button_text="Get research data management support"
                            button_sub_text="Connect with the NFDI4Earth Helpdesk team to get individual support for your RDM problems"
                        />
                    </SimpleGrid>
                </div>
                <br />
                <div>
                    <SectionTitle section_title="Explore highlighted showcases" />
                    <SimpleGrid
                        p="10px"
                        columns={[1, 2, 3, 4, 5, 6]}
                        spacing={5}
                        width={"100%"}
                    >
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Pilot_Lexcube_fig1.png"
                            card_title="Lexcube"
                            card_type="Community tool"
                            content_link="/result/lhb-docs-Pilot_Lexcube.md"
                            minH={minHeight}
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/MetBase_Logo.png"
                            card_title="Metbase"
                            card_type="Community tool"
                            content_link="/result/lhb-docs-Pilot_Metbase.md"
                            minH={minHeight}
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/logo-osgeo.svg"
                            card_type="Community tool"
                            card_title="OSS4Geo"
                            content_link="/result/lhb-docs-OSS4geo_ENG.md"
                            minH={minHeight}
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Hero_Logo.png"
                            card_type="Community tool"
                            card_title="Graph-based visual search engine"
                            content_link="/result/lhb-docs-Showcase_VESA.md"
                            minH={minHeight}
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Incubator_GeoQAMap_Thumbnail.png"
                            card_type="Community tool"
                            card_title="GeoQA Map"
                            content_link="/result/lhb-docs-Showcase_GeoQAMap.md"
                            minH={minHeight}
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Galaxy_for_Earth_System_Thumbnail.png"
                            card_type="Community tool"
                            card_title="Galaxy for Earth System"
                            content_link="/result/lhb-docs-Galaxy_for_Earth_System.md"
                            minH={minHeight}
                        />
                    </SimpleGrid>
                </div>
                <br />
                <div>
                    <SectionTitle section_title="Learn more about software, tools, and services" />
                    <SimpleGrid
                        p="10px"
                        //columns={[1, 2, 3, null, null, null, 7]}
                        spacing={5}
                        width={"100%"}
                        minChildWidth={"200px"}
                    >
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                            card_type="Article"
                            card_title="HPC and Cloud Computing - What is it and when shoud we use it?"
                            content_link="/result/lhb-docs-CloudComputingvsHPC_ENG.md"
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                            card_type="Article"
                            card_title="Introduction to handling raster time series in Julia, Python and R"
                            content_link="/result/lhb-docs-Intro_Raster_Data_Analysis_ENG.md"
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                            card_type="Article"
                            card_title="Tools from NFDI4Earth pilot and incubator projects"
                            content_link="/result/lhb-docs-Collection_tools_pilots_incubators.md"
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                            card_type="Article"
                            card_title="DMP Services"
                            content_link="/result/lhb-docs-DMP-Services.md"
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                            card_type="Article"
                            card_title="RDM Support Services"
                            content_link="/result/lhb-docs-RDM-Services.md"
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/contribute.png"
                            card_type="Article"
                            card_title="Suggest your service"
                            content_link="/result/lhb-docs-N4E_Suggest_A_Service.md"
                        />
                        <CustomCard
                            thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/contribute.png"
                            card_type="Article"
                            card_title="Suggest your community tool as showcase"
                            content_link="/result/lhb-docs-N4E_Suggest_A_Tool_Showcase.md"
                        />
                    </SimpleGrid>
                </div>
            </div>
        </Container>
        </Box>
    );
}


