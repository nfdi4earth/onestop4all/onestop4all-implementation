/* eslint-disable */
import { Box, Container, SimpleGrid } from "@open-pioneer/chakra-integration";

import { SearchBar } from "../../../../components/SearchBar";
import { BackToStartingpage } from "../../../../components/BackToStartingpage/BackToStartingpage";
import { PageTitle } from "./components/PageTitle";
import { CustomButton } from "./components/CustomButton";
import { CustomButtonBeta } from "./components/CustomButtonBeta";
import { CustomCard } from "./components/CustomCard";
import { SectionTitle } from "./components/SectionTitle";


export const Data = () => {
    const minHeight = "340px";
    return (
        <Box className="search-view" w={"100%"}>
            <Box position="relative">
                <Box className="header-image" />
            </Box>

            <Box
                position="absolute"
                width="100%"
                marginTop={{ base: "-40px", custombreak: "-50px" }}
            >
                <Container maxW={{ base: "100%", custombreak: "80%" }}>
                    <SearchBar />
                </Container>
            </Box>

            <Box height={{ base: "100px", custombreak: "80px" }}></Box>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                <BackToStartingpage />

                <div>
                    <div>
                        <PageTitle
                            tbep_title="Discover datasets and repositories"
                        />
                        <SimpleGrid
                            p="10px"
                            columns={[1, 2, 3]}
                            spacing="5"
                            width={"100%"}
                        >
                            <CustomButton
                                button_text="Take a guided repository search"
                                button_sub_text="Discover repositories that fit your needs by answering questions"
                                button_link="/howToEntry/wizard"
                            />
                            <CustomButton 
                                button_text="Explore a curated collection of repositories" 
                                button_sub_text="Search research data repositories and tailor the list by using specific filters"
                                button_link="/search?resourcetype=Repository+%2F+Archive&sort="
                                type="repo"
                            />
                            <CustomButtonBeta
                                button_text="Discover datasets"
                                button_sub_text="Search datasets for your research from an initial list of harvested repositories"
                                button_link="/search?resourcetype=Dataset&sort="
                                type="data"
                            />
                        </SimpleGrid>
                    </div>
                    <br />
                    <br />

                    <br />
                    <div>
                        <SectionTitle section_title="Explore highlighted showcases" />
                        <SimpleGrid
                            p="10px"
                            columns={[1, 2, 3, null, null, 6]}
                            spacing={5}
                            width={"100%"}
                        >
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Pilot_GeoFRESH_logo_small.png?ref_type=heads"
                                card_type="Community dataset"
                                card_title="The GeoFRESH online platform"
                                content_link="/result/lhb-docs-ShowCase_GeoFRESH.md"
                                minH={minHeight}
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/3f66631a4dfe79885a386c88d5fd19c51cd09657/docs/img/QBO_thumbnail_resize.png"
                                card_type="Community dataset"
                                card_title="Monthly Tropical Stratospheric Zonal Winds from Radiosondes"
                                content_link="/result/lhb-docs-QBO.md"
                                minH={minHeight}
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/ShowCase_WHFD_Thumbnail.png"
                                card_title="World Heatflow Database Project"
                                card_type="Community dataset"
                                content_link="/result/lhb-docs-ShowCase_WHFD.md"
                                minH={minHeight}
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Pilot_WSF_logo.png?ref_type=heads"
                                card_type="Community dataset"
                                card_title="World Settlement Footprint"
                                content_link="/result/lhb-docs-Pilot_WSF.md"
                                minH={minHeight}
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/Success_Story_large_datasets_thumbnail.png"
                                card_type="Community dataset"
                                card_title="FAIR multi-terabyte dataset"
                                content_link="/result/lhb-docs-SuccessStory_Large_Datasets.md"
                                minH={minHeight}
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/ShowCase_CAMELSDE_Thumbnail.png"
                                card_type="Community dataset"
                                card_title="CAMELS-DE: A national hydrometeorological data set"
                                content_link="/result/lhb-docs-Showcase_CAMELS_DE.md"
                                minH={minHeight}
                            />
                        </SimpleGrid>
                    </div>
                    <br />
                    <br />

                    <div>
                        <SectionTitle section_title="Learn more about data-related topics" />
                        <SimpleGrid
                            p="10px"
                            columns={[1, 2, 3, null, null, 6]}
                            spacing={5}
                            width={"100%"}
                        >
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                                card_type="Article"
                                card_title="Who owns research data?"
                                content_link="/result/lhb-docs-WhoOwnsTheData_ENG.md"
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                                card_type="Article"
                                card_title="Digital Long-Term Preservation"
                                content_link="/result/lhb-docs-LTA_Introduction.md"
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                                card_type="Article"
                                card_title="Governmental Data"
                                content_link="/result/lhb-docs-SuccessStory_governmental_agencies.md"
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/article.png"
                                card_type="Article"
                                card_title="Data Journals"
                                content_link="/result/lhb-docs-Data_journals_ENG.md"
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/contribute.png"
                                card_type="Article"
                                card_title="Suggest your community dataset as showcase"
                                content_link="/result/lhb-docs-N4E_Suggest_A_Dataset_Showcase.md"
                            />
                            <CustomCard
                                thumbnail_link="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/img/contribute.png"
                                card_type="Article"
                                card_title="Suggest your data repository"
                                content_link="/result/lhb-docs-N4E_Suggest_A_Repository.md"
                            />
                        </SimpleGrid>
                    </div>
                </div>
            </Container>
        </Box>
    );
};


