/* eslint-disable */
import { Box, Button, Container, Flex } from "@open-pioneer/chakra-integration";
import { Link, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import remarkGfm from "remark-gfm";
import { unified } from "unified";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import rehypeCitation from "rehype-citation";
import rehypeStringify from "rehype-stringify";
import parse from "html-react-parser";
import { parseMarkdown, getTags, getLinkType } from "../../../services/MarkdownUtils";

import { useService } from "open-pioneer:react-hooks";
import { SearchBar } from "../../../components/SearchBar";
import { RelatedContent } from "../../../components/ResourceType/RelatedContent/RelatedContent";
import { BackToStartingpage } from "../../../components/BackToStartingpage/BackToStartingpage";
import Skeleton from "../../../components/Skeleton/Skeleton";
import { SearchService, SolrSearchResultItem } from "../../../services/SearchService";

export interface HowToDocs {
    id: string;
}
export interface HowToResponse {
    docs: HowToDocs[];
}

export const HowToEntryContent = () => {
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);

    const id = useParams().content;
    const [howToEntry, setHowToEntry] = useState<SolrSearchResultItem>();
    const [markdownContent, setMdCon] = useState("");

    const opts = searchSrvc.getRehypeCitationOptions();

    const [bibliography, setBibliography] = useState(opts.proxy + opts.bib);

    const [citationFileFormat, setCitationFileFormat] = useState(opts.cff);
    const rehypeCitationOptions = { bibliography, citationFileFormat };

    useEffect(() => {
        if (id) {
            searchSrvc.getMetadata(id).then((res) => {
                if (res.results.length > 0 && res.results[0]) {
                    let entry = res.results[0];
                    if (entry.relatedContent?.length > 0) {
                        const relatedResources = [] as object[];
                        entry.relatedContent.forEach((relatedResourceId: string) => {
                            searchSrvc.getMetadata(relatedResourceId).then((res2) => {
                                const relResTmp = relatedResources;
                                relResTmp.push(res2.results);
                                if (relatedResources.length === entry.relatedContent.length) {
                                    entry.relatedResources = relResTmp;
                                    setHowToEntry(entry);
                                    setLoading(false);
                                }
                            });
                        });
                    } else {
                        setHowToEntry(entry);
                        setLoading(false);
                    }
                }
            });
        }
    }, []);

    useEffect(() => {
        if (howToEntry && howToEntry.articleBody && howToEntry.articleBody[0]) {
            console.log(howToEntry.id);
            unified()
                .use(remarkParse)
                .use(remarkGfm)
                .use(remarkRehype, {})
                .use(rehypeCitation, rehypeCitationOptions)
                .use(rehypeStringify)
                .process(howToEntry.articleBody[0])
                .then((file) => {
                    const html = parseMarkdown(file.value as string);
                    const htmlTags = getTags(html);
                    if (html && htmlTags && htmlTags.length > 0) {
                        for (let i = 0; i < htmlTags.length; i++) {
                            const tmp = htmlTags[i];
                            const tag = html.getElementsByTagName("a")[i];
                            const link = tag?.href;
                            const url = html.URL;
                            const linkType = getLinkType(link as string, url as string);
                            if (tmp && linkType) {
                                tmp.target = "_blank";
                                tmp.rel = "noopener";
                                if (linkType == "mail" || linkType == "url") {
                                    setMdCon(html.body.innerHTML as string);
                                }
                                if (linkType == "markdown") {
                                    const markdownLink = html
                                        .getElementsByTagName("a")
                                        [i]?.href.split("/")
                                        .pop();
                                    if (markdownLink) {
                                        searchSrvc.getChapter(markdownLink).then((result) => {
                                            const res = result.response as HowToResponse;
                                            const id =
                                                res && res.docs && res.docs[0]
                                                    ? res.docs[0].id
                                                    : "";
                                            tmp.href = "/result/" + id;
                                            setMdCon(html.body.innerHTML as string);
                                        });
                                    }
                                }
                                if (linkType == "cordra") {
                                    const id = tmp.href.split("/").pop();
                                    tmp.href = "/result/" + id;
                                    setMdCon(html.body.innerHTML as string);
                                }
                                if (linkType == "pdf_docx") {
                                    const id = tmp.href.split("/").pop();
                                    tmp.href = opts.pdfFolder + id;
                                    setMdCon(html.body.innerHTML as string);
                                }
                            }
                        }
                    }
                    setMdCon(html.body.innerHTML as string);
                });
        }
    }, [howToEntry]);

    function backToStart() {
        navigate({ pathname: "/" });
    }

    return (
        <Box className="search-view" w={"100%"}>
            <Box position="relative">
                <Box className="header-image" />
            </Box>

            <Box
                position="absolute"
                width="100%"
                marginTop={{ base: "-40px", custombreak: "-50px" }}
            >
                <Container maxW={{ base: "100%", custombreak: "80%" }}>
                    <SearchBar />
                </Container>
            </Box>

            <Box height={{ base: "100px", custombreak: "80px" }}></Box>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                <BackToStartingpage />

                {loading ? (
                    <Skeleton />
                ) : markdownContent ? (
                    <Flex direction={{ base: "column", custombreak: "row" }}>
                        <Box w="70%" padding="3%">
                            {parse(markdownContent, {
                                replace: (domNode: any) => {
                                    if (domNode.type === "tag" && domNode.name === "img") {
                                        const srcUrl =
                                            //"https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/raw/main/docs/" +
                                            domNode.attribs.src; // Replace with the correct URL
                                        return (
                                            <Box _hover={{ cursor: "pointer" }}>
                                                <img src={srcUrl}>{domNode.children[0]?.data}</img>
                                            </Box>
                                        );
                                    }
                                }
                            })}
                        </Box>
                        <Box padding={{ base: "3%", custombreak: "4%" }}>
                            {howToEntry && howToEntry.id === "lhb-docs-TBEP_IAmLookingForData_ENG.md" ? (
                                <Flex direction={"column"} w={"310px"}>
                                    <Link to={"/howtoentry/wizard"}>
                                        <Button marginBottom="5%" minW={"310px"}>
                                            Start guided search
                                        </Button>
                                    </Link>
                                    <Link
                                        to={
                                            "http://localhost:5173/search?resourcetype=Repository+%2F+Archive&sort="
                                        }
                                        target="_blank"
                                    >
                                        <Button>Show all repositories</Button>
                                    </Link>
                                </Flex>
                            ) : null}
                        </Box>
                    </Flex>
                ) : (
                    <div className="notFound">No content found</div>
                )}
                {howToEntry &&
                howToEntry.relatedResources &&
                howToEntry.relatedResources.length > 0 ? (
                    <Box marginTop={{ base: "40px", custombreak: "80px" }}>
                        <RelatedContent relatedContentItems={howToEntry.relatedResources} />
                    </Box>
                ) : null}
            </Container>
        </Box>
    );
};
