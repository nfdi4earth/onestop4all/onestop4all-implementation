export function restoreScrollPosition(key: string) {
    const scrollPosition = sessionStorage.getItem(key);
    if (scrollPosition !== null) {
        window.scrollTo(0, parseFloat(scrollPosition));
    }
}
