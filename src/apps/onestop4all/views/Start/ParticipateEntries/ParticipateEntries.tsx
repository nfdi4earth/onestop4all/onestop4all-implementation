/* eslint-disable */
import { Box, Flex, SimpleGrid } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";
import { useEffect, useState } from "react";
import { useService } from "open-pioneer:react-hooks";

import yaml from "js-yaml";

import { LhbStructure } from "../../../components/ResourceType/TOC/TOC";
import { ParticipateEntry } from "./ParticipateEntry";
import { SearchService } from "../../../services";

export const ParticipateEntries = (props: { lang: string }) => {
    const intl = useIntl();
    const language = props.lang;

    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const [participateEntries, setEntries] = useState(new Array<string>());

    const getParticipateEntriesList = (sections: object[]) => {
        let foundList = new Array<string>();
        sections.forEach((elem) => {
            if (Object.keys(elem)[0] === "Participate") {
                foundList = Object.values(elem)[0];
            }
        });
        return foundList;
    };

    useEffect(() => {
        searchSrvc.getLhbStructure().then((result) => {
            const parsedYaml = yaml.load(result) as LhbStructure;
            let participateEntriesList = getParticipateEntriesList(parsedYaml.nav);
            /*participateEntriesList =
                language === "en"
                    ? participateEntriesList.filter((str) => str.includes("_ENG.md"))
                    : language === "de"
                    ? participateEntriesList.filter((str) => str.includes("_DEU.md"))
                    : participateEntriesList;*/
            setEntries(participateEntriesList);
        });
    }, []);

    if (participateEntries.length > 2) {
        return (
            <Box className="get-involved">
                <Box className="text-centered-box">
                    <Box className="text-centered-box-header">
                        {intl.formatMessage({ id: "start.get-involved.title" })}
                    </Box>
                </Box>
                <SimpleGrid columns={[1, 2, 3]} spacing={10} pt="32px">
                    {participateEntries.slice(1).map((participateEntry, index) => (
                        <Flex key={index} style={{justifyContent:"center"}}>
                            <Box className="participateEntries">
                                <ParticipateEntry howToEntryTitle={participateEntry} key={index} />
                            </Box>
                        </Flex>
                    ))}
                </SimpleGrid>
            </Box>
        );
    } else {
        return null;
    }
};
