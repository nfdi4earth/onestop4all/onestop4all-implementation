import { Box } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useService } from "open-pioneer:react-hooks";
import { useNavigate } from "react-router-dom";

import Skeleton from "../../../components/Skeleton/Skeleton";
import { HowToEntryMetadata, HowToEntryResult } from "../HowTo/HowToEntry";
import { SearchService } from "../../../services";

export const ParticipateEntry = (props: { howToEntryTitle: string }) => {
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);

    const howToEntryTitle = props.howToEntryTitle;
    const [howToEntryMetadata, setEntryMetadata] = useState<HowToEntryMetadata>();

    const opts = searchSrvc.getImageUrl();

    useEffect(() => {
        searchSrvc.getChapter(howToEntryTitle).then((result: HowToEntryResult) => {
            if (
                result &&
                result.response &&
                result.response.docs &&
                result.response.docs.length > 0
            ) {
                setEntryMetadata({
                    name: result.response.docs[0].mainTitle,
                    description: result.response.docs[0].description,
                    id: result.response.docs[0].id
                });
                setLoading(false);
            }
        });
    }, [howToEntryTitle]);

    const handleClick = (id: string) => {
        navigate(`/howtoentry/` + id);
        window.scroll(0, 0);
    };

    return (
        <Box _hover={{ cursor: "pointer" }}>
            {loading ? (
                <div style={{ width: "150px" }}>
                    <Skeleton />
                </div>
            ) : howToEntryMetadata ? (
                <div
                    className="participate-entry"
                    onClick={() => {
                        handleClick(howToEntryMetadata.id);
                    }}
                >
                    <div className="overlap">
                        <div className="circle-group">
                            <div className="circle circle-1 icon-base" />
                            <div className="circle circle-2 div" />
                            <div className="circle circle-3 icon-base-2" />
                        </div>
                        <img
                            className="image"
                            src={opts.imageUrl + howToEntryTitle.replace(".md", "") + ".png"}
                        />
                    </div>
                    <div className="label">{howToEntryMetadata.name}</div>
                </div>
            ) : null}
        </Box>
    );
};
