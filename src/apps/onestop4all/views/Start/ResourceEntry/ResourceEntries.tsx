import { Box, Flex, SimpleGrid } from "@open-pioneer/chakra-integration";
import { useService } from "open-pioneer:react-hooks";
import { useEffect, useState } from "react";

import { ResourceEntry, ResourceEntryProps } from "./ResourceEntry";
import { SearchService } from "../../../services";
import { ResourceType } from "../../../services/ResourceTypeUtils";

export const ResourceEntries = () => {
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;

    const [resources, setResources] = useState<ResourceEntryProps[]>([]);

    useEffect(() => {
        searchSrvc.doSearch({ pageSize: 0 }).then((res) => {
            const order = [
                ResourceType.Datasets,
                ResourceType.Repos,
                ResourceType.Service,
                ResourceType.DataService,
                ResourceType.Learning_Resource,
                ResourceType.Tools,
                ResourceType.LHB_Articles,
                ResourceType.Standards,
                ResourceType.Publications
            ];

            const entries: ResourceEntryProps[] = res.facets.resourceType
                .filter(r => r.resourceType !== ResourceType.Organisations)
                .map((r) => ({
                    resourceType: r.resourceType,
                    resultCount: r.count
                }))
                .sort((a, b) => order.indexOf(a.resourceType) - order.indexOf(b.resourceType));
            setResources(entries);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <SimpleGrid columns={[1, 2, 3]} spacing={10}>
            {resources.map((e, i) => (
                <Flex key={i} style={{ justifyContent: "center" }}>
                    <Box className="resourceEntries">
                        <ResourceEntry {...e}></ResourceEntry>
                    </Box>
                </Flex>
            ))}
        </SimpleGrid>
    );
};
