/* eslint-disable */
import { Box, Button, Container, IconButton } from "@open-pioneer/chakra-integration";
import { useIntl, useService } from "open-pioneer:react-hooks";
import { useEffect, useRef, useState } from "react";

import remarkGfm from "remark-gfm";
import { unified } from "unified";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import rehypeStringify from "rehype-stringify";
import parse from "html-react-parser";
import {
    Accordion,
    AccordionItem,
    AccordionButton,
    AccordionPanel,
    AccordionIcon
} from "@chakra-ui/react";

import { SearchBar } from "../../../components/SearchBar";
import { parseMarkdown, getLinkType, getTags } from "../../../services/MarkdownUtils";
import { BackToStartingpage } from "../../../components/BackToStartingpage/BackToStartingpage";
import { useParams } from "react-router-dom";
import { CopyLinkButton } from "./CopyLinkButton";
import { SearchService } from "../../../services";

interface FaqItem {
    id: string;
    title: string;
    content: string;
    key: number;
}

export function FaqContent() {
    const urlNamesArray = [
        "publish-data",
        "fair-principles",
        "research-data-management-plan",
        "cite",
        "publishing-duration",
        "license",
        "metadata",
        "doi-pid",
        "find-repository",
        "keep-or-delete",
        "storing-publishing",
        "nfdi4earth",
        "nfdi4earth-offer",
        "contribute",
        "source"
    ];
    // Fill Map with key-value pairs
    const urlNames = new Map();
    for (let i = 0; i < urlNamesArray.length; i++) {
        urlNames.set(i, urlNamesArray[i]);
    }
    function getByValue(map: Map<number, string>, searchValue: string) {
        for (let [key, value] of map.entries()) {
            if (value === searchValue) return key;
        }
    }

    const faqId = getByValue(urlNames, useParams().key as string);
    const myRef = useRef<HTMLDivElement>(null);
    const [faqList, setFaqList] = useState<any>([]);
    const intl = useIntl();
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const opts = searchSrvc.getRehypeCitationOptions();

    const scroll = () => {
        if (myRef.current != null) myRef.current.scrollIntoView();
    };

    useEffect(scroll);

    const removeYaml = (md: string) => {
        const yamlRegex = /^---\n([\s\S]*?)\n---\n/;
        const mdWithoutYaml = md.replace(yamlRegex, "");
        return mdWithoutYaml;
    };

    useEffect(() => {
        searchSrvc.getFaqList().then((res) => {
            const markdownWithoutYaml = removeYaml(res);
            const regex = /\[([^\]]+)\]\(([^)]+)\)/g;
            const links = [];

            let match;
            while ((match = regex.exec(markdownWithoutYaml)) !== null) {
                const title = match[1];
                const idMatch = match[2] ? /FAQ_(\w+)\.md/.exec(match[2]) : null;
                const id = idMatch ? idMatch[0] : null;
                id && title ? links.push({ id, title }) : null;
            }

            let faqChapters: FaqItem[] = [];
            links.forEach((link, key) => {
                const id = link.id;
                const title = link.title;
                searchSrvc.getFaq(id).then((res) => {
                    const content = removeYaml(res);
                    unified()
                        .use(remarkParse)
                        .use(remarkGfm)
                        .use(remarkRehype, {})
                        .use(rehypeStringify)
                        .process(content)
                        .then((file) => {
                            const html = parseMarkdown(file.value as string);
                            const htmlTags = getTags(html);
                            if (html && htmlTags && htmlTags.length > 0) {
                                for (let i = 0; i < htmlTags.length; i++) {
                                    const tmp = htmlTags[i];
                                    const tag = html.getElementsByTagName("a")[i];
                                    const link = tag?.href;
                                    const url = html.URL;
                                    const linkType = getLinkType(link as string, url as string);
                                    if (tmp && linkType) {
                                        tmp.target = "_blank";
                                        tmp.rel = "noopener";
                                        if (linkType == "markdown") {
                                            const markdownLink = html
                                                .getElementsByTagName("a")
                                                [i]?.href.split("/")
                                                .pop();
                                            tmp.href = "/redirect/" + markdownLink?.split(".")[0];
                                        }
                                        if (linkType == "cordra") {
                                            const id = tmp.href.split("/").pop();
                                            tmp.href = "/result/" + id;
                                        }
                                        if (linkType == "pdf_docx") {
                                            const id = tmp.href.split("/").pop();
                                            tmp.href = opts.pdfFolder + id;
                                        }
                                    }
                                }
                                faqChapters.push({
                                    key: key,
                                    id: id,
                                    title: title,
                                    content: html.body.innerHTML as string
                                });
                            } else {
                                faqChapters.push({
                                    key: key,
                                    id: id,
                                    title: title,
                                    content: html.body.innerHTML as string
                                });
                            }
                            if (faqChapters.length === links.length) {
                                faqChapters.sort((a, b) => (a.key < b.key ? -1 : 1));
                                faqChapters = faqChapters.map((chapter) => ({
                                    ...chapter, // Keep the existing properties
                                    content: chapter.content.replace(/<h1.*?>.*?<\/h1>/, '') // Remove <h1> tags from content
                                }));
                                setFaqList(faqChapters);
                            }
                        });
                });
            });
        });
    }, []);

    return (
        <Box>
            {" "}
            <Accordion defaultIndex={[Number(faqId)]} allowMultiple>
                {faqList.map((faq: FaqItem, i: number) =>
                    i == faqId ? (
                        <Box ref={myRef} className="scrollBox">
                            <AccordionItem key={i}>
                                <h2>
                                    <AccordionButton>
                                        <Box as="span" flex="1" textAlign="left">
                                            <b>{faq.title}</b>
                                            <CopyLinkButton id={i} urlNames={urlNames} />
                                        </Box>
                                        <AccordionIcon />
                                    </AccordionButton>
                                </h2>
                                <AccordionPanel pb={4}>{parse(faq.content)}</AccordionPanel>
                                <p>{faq.content}</p>
                            </AccordionItem>
                        </Box>
                    ) : (
                        <Box>
                            <AccordionItem key={i}>
                                <h2>
                                    <AccordionButton>
                                        <Box as="span" flex="1" textAlign="left">
                                            <b>{faq.title}</b>
                                            <CopyLinkButton id={i} urlNames={urlNames} />
                                        </Box>
                                        <AccordionIcon />
                                    </AccordionButton>
                                </h2>
                                <AccordionPanel pb={4} style={{marginTop:"-1%"}}>{parse(faq.content)}</AccordionPanel>
                            </AccordionItem>
                        </Box>
                    )
                )}
            </Accordion>
        </Box>
    );
}
