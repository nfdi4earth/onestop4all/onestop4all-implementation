/* eslint-disable */
import { Box, Container } from "@open-pioneer/chakra-integration";
import { useIntl, useService } from "open-pioneer:react-hooks";
import { useEffect, useState } from "react";

import remarkGfm from "remark-gfm";
import { unified } from "unified";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import rehypeStringify from "rehype-stringify";
import parse from "html-react-parser";
import {
    Accordion,
    AccordionItem,
    AccordionButton,
    AccordionPanel,
    AccordionIcon
} from "@chakra-ui/react";

import { SearchBar } from "../../../components/SearchBar";
import { parseMarkdown, getLinkType, getTags } from "../../../services/MarkdownUtils";
import { BackToStartingpage } from "../../../components/BackToStartingpage/BackToStartingpage";
import { FaqContent } from "./FaqContent";
import { SearchService } from "../../../services";

interface FaqItem {
    id: string;
    title: string;
    content: string;
    key: number;
}

export function Faq() {
    const [faqList, setFaqList] = useState<any>([]);
    const intl = useIntl();
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const opts = searchSrvc.getRehypeCitationOptions();

    const removeYaml = (md: string) => {
        const yamlRegex = /^---\n([\s\S]*?)\n---\n/;
        const mdWithoutYaml = md.replace(yamlRegex, "");
        return mdWithoutYaml;
    };

    useEffect(() => {
        searchSrvc.getFaqList().then((res) => {
            const markdownWithoutYaml = removeYaml(res);
            const regex = /\[([^\]]+)\]\(([^)]+)\)/g;
            const links = [];

            let match;
            while ((match = regex.exec(markdownWithoutYaml)) !== null) {
                const title = match[1];
                const idMatch = match[2] ? /FAQ_(\w+)\.md/.exec(match[2]) : null;
                const id = idMatch ? idMatch[0] : null;
                id && title ? links.push({ id, title }) : null;
            }

            const faqChapters: FaqItem[] = [];
            links.forEach((link, key) => {
                const id = link.id;
                const title = link.title;
                searchSrvc.getFaq(id).then((res) => {
                    const content = removeYaml(res);
                    unified()
                        .use(remarkParse)
                        .use(remarkGfm)
                        .use(remarkRehype, {})
                        .use(rehypeStringify)
                        .process(content)
                        .then((file) => {
                            const html = parseMarkdown(file.value as string);
                            const htmlTags = getTags(html);
                            if (html && htmlTags && htmlTags.length > 0) {
                                for (let i = 0; i < htmlTags.length; i++) {
                                    const tmp = htmlTags[i];
                                    const tag = html.getElementsByTagName("a")[i];
                                    const link = tag?.href;
                                    const url = html.URL;
                                    const linkType = getLinkType(link as string, url as string);
                                    if (tmp && linkType) {
                                        tmp.target = "_blank";
                                        tmp.rel = "noopener";
                                        if (linkType == "markdown") {
                                            const markdownLink = html
                                                .getElementsByTagName("a")
                                                [i]?.href.split("/")
                                                .pop();
                                            tmp.href = "/redirect/" + markdownLink?.split(".")[0];
                                        }
                                        if (linkType == "cordra") {
                                            const id = tmp.href.split("/").pop();
                                            tmp.href = "/result/" + id;
                                        }
                                        if (linkType == "pdf_docx") {
                                            const id = tmp.href.split("/").pop();
                                            tmp.href = opts.pdfFolder + id;
                                        }
                                    }
                                }
                                faqChapters.push({
                                    key: key,
                                    id: id,
                                    title: title,
                                    content: html.body.innerHTML as string
                                });
                            } else {
                                faqChapters.push({
                                    key: key,
                                    id: id,
                                    title: title,
                                    content: html.body.innerHTML as string
                                });
                            }
                            if (faqChapters.length === links.length) {
                                faqChapters.sort((a, b) => (a.key < b.key ? -1 : 1));
                                setFaqList(faqChapters);
                            }
                        });
                });
            });
        });
    }, []);

    return (
        <Box className="search-view">
            <Box position="relative">
                <Box className="header-image" />
            </Box>

            <Box
                position="absolute"
                width="100%"
                marginTop={{ base: "-40px", custombreak: "-50px" }}
            >
                <Container maxW={{ base: "100%", custombreak: "80%" }}>
                    <SearchBar />
                </Container>
            </Box>

            <Box height={{ base: "50px", custombreak: "80px" }} />

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                <Box w="100%" paddingTop={{ base: "5%", custombreak: "0%" }}>
                    <BackToStartingpage />
                    <Box marginTop={"1%"}>
                        <h1>
                            {intl.formatMessage({
                                id: "faq.faq"
                            })}
                        </h1>
                        <Box paddingBottom={"1%"} paddingTop={"1%"}>
                            {intl.formatMessage({
                                id: "faq.desc"
                            })}
                        </Box>
                    </Box>
                    <FaqContent />
                </Box>
            </Container>
        </Box>
    );
}
