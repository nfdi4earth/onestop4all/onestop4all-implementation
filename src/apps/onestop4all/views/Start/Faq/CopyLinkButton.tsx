import { IconButton, Tooltip } from "@open-pioneer/chakra-integration";
import { PrimaryColor } from "../../../Theme";
import { MdContentCopy } from "react-icons/md";

import { useIntl, useService } from "open-pioneer:react-hooks";
import { SearchService } from "../../../services";

interface CopyLinkButtonProps {
    id: number;
    urlNames: Map<number, string>;
}
export function CopyLinkButton(props: CopyLinkButtonProps) {
    const { id, urlNames } = props;
    const intl = useIntl();
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;

    const copyLink = (event: any) => {
        const url = urlNames.get(id);
        const base_url = searchSrvc.getBaseUrl();
        navigator.clipboard.writeText(base_url + "faq/" + url);
        event.stopPropagation();
    };
    return (
        <Tooltip
            label={intl.formatMessage({ id: "faq.copy" })}
            placement="right"
            color="white"
            bg="rgba(5, 102, 141, 0.8)"
            borderRadius="10px"
        >
            <IconButton
                aria-label="Copy link"
                bg="transparent"
                color={PrimaryColor}
                _hover={{ color: "black" }}
                icon={<MdContentCopy />}
                onClick={copyLink}
            />
        </Tooltip>
    );
}
