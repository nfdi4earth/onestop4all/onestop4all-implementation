import { useService } from "open-pioneer:react-hooks";
import { createContext, PropsWithChildren, useContext, useState } from "react";
import { useSearchParams } from "react-router-dom";

import { ResourceType } from "../../services/ResourceTypeUtils";
import {
    SearchResult,
    SearchService,
    SubjectEntry,
    TemporalFacet,
    TemporalFilter
} from "../../services/SearchService";

export enum UrlSearchParameterType {
    Searchterm = "searchterm",
    ResourceType = "resourcetype",
    Subjects = "subjects",
    DataAccessTypes = "dataAccessTypes",
    DataFormatTypes = "contentType",
    DataUploadTypes = "dataUploadTypes",
    Countries = "countryName",
    Publisher = "publisher",
    Api = "hasAPI_conformsTo",
    DataLicense = "dataLicense",
    SupportedMetadataStandards = "supportedMetadataStandards",
    Licenses = "licenses",
    SpatialFilter = "spatialfilter",
    PageSize = "pageSize",
    PageStart = "pageStart",
    TemporalFilter = "temporalfilter",
    SortingFilter = "sort",
    DoiOption = "assignsIdentifierScheme"
}

export interface UrlSearchParams {
    [UrlSearchParameterType.Searchterm]?: string;
    [UrlSearchParameterType.ResourceType]?: string[];
    [UrlSearchParameterType.Subjects]?: string[];
    [UrlSearchParameterType.DataAccessTypes]?: string[];
    [UrlSearchParameterType.DataFormatTypes]?: string[];
    [UrlSearchParameterType.DataUploadTypes]?: string[];
    [UrlSearchParameterType.Countries]?: string[];
    [UrlSearchParameterType.Publisher]?: string[];
    [UrlSearchParameterType.Api]?: string[];
    [UrlSearchParameterType.DataLicense]?: string[];
    [UrlSearchParameterType.SupportedMetadataStandards]?: string[];
    [UrlSearchParameterType.Licenses]?: string[];
    [UrlSearchParameterType.SpatialFilter]?: string;
    [UrlSearchParameterType.PageSize]?: string;
    [UrlSearchParameterType.PageStart]?: string;
    [UrlSearchParameterType.TemporalFilter]?: string;
    [UrlSearchParameterType.SortingFilter]?: string;
    [UrlSearchParameterType.DoiOption]?: string;
}

export const SpatialFilterEnableForResourceTypes = [
    ResourceType.Organisations, 
    ResourceType.Datasets
];
export const DataAccessTypeFilterEnable = [ResourceType.Repos];
export const DataFormatTypeFilterEnable = [ResourceType.Repos];
export const DataUploadTypeFilterEnable = [ResourceType.Repos];
export const CountryFilterEnable = [ResourceType.Organisations];
export const PublisherFilterEnable = [ResourceType.Repos];
export const ApiFilterEnable = [ResourceType.Repos];
export const DataLicenseFilterEnable = [ResourceType.Repos];
export const SupportedMetadataStandardFilterEnable = [ResourceType.Repos];
export const LicensesFilterEnable = [ResourceType.Tools];
export const TemporalFilterEnableForResourceTypes = [
    ResourceType.Publications,
    ResourceType.Learning_Resource,
    ResourceType.Datasets
];

export const SortOptions: SortOption[] = [
    { label: "search.relevance", term: "" }, // these are the ids in the .yaml file
    { label: "search.title-az", term: "mainTitle asc" },
    { label: "search.title-za", term: "mainTitle desc" }
];

export const TemporalFacetStartYear = 2000;
export const TemporalFacetEndYear = 2023;
export const TemporalFacetGap = "+1YEAR";

export interface SelectableResourceType {
    resourceType: ResourceType;
    count: number;
    selected: boolean;
}

export interface SelectableSubject {
    label: string;
    children: SelectableSubject[];
    count?: number;
    selected?: boolean;
}

export interface SelectableDataAccessType {
    label: string;
    dataAccessType: SelectableDataAccessType[];
    count: number;
    selected?: boolean;
}

export interface SelectableDataFormatType {
    label: string;
    dataFormatType: SelectableDataFormatType[];
    count: number;
    selected?: boolean;
}

export interface SelectableDataUploadType {
    label: string;
    dataUploadType: SelectableDataUploadType[];
    count: number;
    selected?: boolean;
}

export interface SelectableCountry {
    label: string;
    country: SelectableCountry[];
    count: number;
    selected?: boolean;
}

export interface SelectablePublisher {
    label: string;
    publisher: SelectablePublisher[];
    count: number;
    selected?: boolean;
}

export interface SelectableApi {
    label: string;
    hasAPI_conformsTo: SelectableApi[];
    count: number;
    selected?: boolean;
}

export interface SelectableDataLicense {
    label: string;
    dataLicense: SelectableDataLicense[];
    count: number;
    selected?: boolean;
}

export interface SelectableSupportedMetadataStandard {
    label: string;
    supportedMetadataStandard: SelectableSupportedMetadataStandard[];
    count: number;
    selected?: boolean;
}

export interface SelectableLicense {
    label: string;
    license: SelectableLicense[];
    count: number;
    selected?: boolean;
}

export interface SortOption {
    label: string;
    term: string;
}

export interface ISearchState {
    searchTerm: string;
    setSearchTerm(searchTerm: string): void;
    selectedResourceTypes: string[];
    setSelectedResourceTypes(types: string[]): void;
    selectableResourceTypes: SelectableResourceType[];

    selectedSubjects: string[];
    setSelectedSubjects(subjects: string[]): void;
    selectableSubjects: SelectableSubject[];

    selectedDataAccessTypes: string[];
    setSelectedDataAccessTypes(dataAccessTypes: string[]): void;
    selectableDataAccessTypes: SelectableDataAccessType[];

    selectedCountries: string[];
    setSelectedCountries(countries: string[]): void;
    selectableCountries: SelectableCountry[];

    selectedPublisher: string[];
    setSelectedPublisher(publisher: string[]): void;
    selectablePublisher: SelectablePublisher[];

    selectedApi: string[];
    setSelectedApi(api: string[]): void;
    selectableApi: SelectableApi[];

    selectedDataLicense: string[];
    setSelectedDataLicense(dataLicense: string[]): void;
    selectableDataLicense: SelectableDataLicense[];

    selectedDataFormatTypes: string[];
    setSelectedDataFormatTypes(dataFormatTypes: string[]): void;
    selectableDataFormatTypes: SelectableDataFormatType[];

    selectedDataUploadTypes: string[];
    setSelectedDataUploadTypes(dataUploadTypes: string[]): void;
    selectableDataUploadTypes: SelectableDataUploadType[];

    selectedSupportedMetadataStandards: string[];
    setSelectedSupportedMetadataStandards(supportedMetadataStandards: string[]): void;
    selectableSupportedMetadataStandards: SelectableSupportedMetadataStandard[];

    selectedLicenses: string[];
    setSelectedLicenses(supportedLicenses: string[]): void;
    selectableLicenses: SelectableLicense[];

    spatialFilter: number[];
    setSpatialFilter(sf: number[]): void;
    spatialFilterDisabled: boolean;
    dataAccessTypeFilterDisabled: boolean;
    dataFormatTypeFilterDisabled: boolean;
    dataUploadTypeFilterDisabled: boolean;
    countryFilterDisabled: boolean;
    publisherFilterDisabled: boolean;
    apiFilterDisabled: boolean;
    dataLicenseFilterDisabled: boolean;
    supportedMetadataStandardFilterDisabled: boolean;
    licensesFilterDisabled: boolean;

    temporalFilter: TemporalFilter | undefined;
    setTemporalFilter(tf?: TemporalFilter): void;
    temporalFacets: TemporalFacet[];
    temporalFilterDisabled: boolean;
    pageSize: number;
    setPageSize(pageSize: number): void;
    pageStart: number;
    setPageStart(pageSize: number): void;
    searchResults: SearchResult | undefined;
    isLoaded: boolean;
    sorting: SortOption | undefined;
    setSorting(sortOption: SortOption): unknown;
    search(): void;

    doiOption: boolean;
    setDoiOption(doiOption: boolean): void;

    selectedFacet: string;
    setSelectedFacet(selectedFacet: string): void;

    prevPath: string;
    setPrevPath(prevPath: string): void;
}

export const SearchStateContext = createContext<ISearchState | undefined>(undefined);

export interface SearchFilter {
    searchTerm?: string;
}

export const useSearchState = () => {
    const context = useContext(SearchStateContext);
    if (context === undefined) {
        throw new Error(
            "SearchStateContext was not provided. Make sure your component is a child of SearchState."
        );
    }
    return context;
};

export const DEFAULT_PAGE_SIZE = 20;
export const SELECTABLE_PAGES_SIZES = [20, 50, 100, 200, 500];

export const SearchState = (props: PropsWithChildren) => {
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const [searchParams] = useSearchParams();

    // init search results and loading state
    const [searchResults, setSearchResults] = useState<SearchResult>();
    const [isLoaded, setIsLoaded] = useState(false);

    // init search term
    const [searchTerm, setSearchTerm] = useState<string>(
        searchParams.get(UrlSearchParameterType.Searchterm) || ""
    );

    // init page size
    const pSize = parseInt(
        searchParams.get(UrlSearchParameterType.PageSize) || `${DEFAULT_PAGE_SIZE}`
    );
    const [pageSize, setPageSize] = useState<number>(pSize);

    // init page start
    const pStart = parseInt(searchParams.get(UrlSearchParameterType.PageStart) || "0");
    const [pageStart, setPageStart] = useState<number>(pStart);

    // init selectable resourceTypes
    const [selectableResourceTypes, setSelecteableResourceTypes] = useState<
        SelectableResourceType[]
    >([]);

    // init selected resourceTypes
    const sRt: string[] = [];
    const urlRt = searchParams.getAll(UrlSearchParameterType.ResourceType);
    if (urlRt?.length) {
        urlRt.forEach((e) => e && sRt.push(e));
    }
    const [selectedResourceTypes, setSelectedResourceTypes] = useState<string[]>(sRt);

    // check disabling spatial filter
    const matches = SpatialFilterEnableForResourceTypes.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const spatialFilterDisabled = matches.length === 0 && selectedResourceTypes.length > 0;

    // init subjects
    const [selectableSubjects, setSelecteableSubjects] = useState<SelectableSubject[]>([]);
    const subjects: string[] = [];
    const urlSubs = searchParams.getAll(UrlSearchParameterType.Subjects);
    if (urlSubs?.length) {
        urlSubs.forEach((e) => e && subjects.push(e));
    }
    const [selectedSubjects, setSelectedSubjects] = useState<string[]>(subjects);

    // init data access types
    const [selectableDataAccessTypes, setSelectableDataAccessTypes] = useState<
        SelectableDataAccessType[]
    >([]);
    const dataAccessTypes: string[] = [];
    const urlDataAccessTypes = searchParams.getAll(UrlSearchParameterType.DataAccessTypes);
    if (urlDataAccessTypes?.length) {
        urlDataAccessTypes.forEach((e) => e && dataAccessTypes.push(e));
    }
    const [selectedDataAccessTypes, setSelectedDataAccessTypes] =
        useState<string[]>(dataAccessTypes);
    const foundDataAccessTypes = DataAccessTypeFilterEnable.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const dataAccessTypeFilterDisabled =
        foundDataAccessTypes.length === 0 && selectedResourceTypes.length > 0;

    // init data format types
    const [selectableDataFormatTypes, setSelectableDataFormatTypes] = useState<
        SelectableDataFormatType[]
    >([]);
    const dataFormatTypes: string[] = [];
    const urlDataFormatTypes = searchParams.getAll(UrlSearchParameterType.DataFormatTypes);
    if (urlDataFormatTypes?.length) {
        urlDataFormatTypes.forEach((e) => e && dataFormatTypes.push(e));
    }
    const [selectedDataFormatTypes, setSelectedDataFormatTypes] =
        useState<string[]>(dataFormatTypes);
    const foundDataFormatTypes = DataFormatTypeFilterEnable.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const dataFormatTypeFilterDisabled =
        foundDataFormatTypes.length === 0 && selectedResourceTypes.length > 0;

    // init data upload types
    const [selectableDataUploadTypes, setSelectableDataUploadTypes] = useState<
        SelectableDataUploadType[]
    >([]);
    const dataUploadTypes: string[] = [];
    const urlDataUploadTypes = searchParams.getAll(UrlSearchParameterType.DataUploadTypes);
    if (urlDataUploadTypes?.length) {
        urlDataUploadTypes.forEach((e) => e && urlDataUploadTypes.push(e));
    }
    const [selectedDataUploadTypes, setSelectedDataUploadTypes] =
        useState<string[]>(dataUploadTypes);
    const foundDataUploadTypes = DataUploadTypeFilterEnable.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const dataUploadTypeFilterDisabled =
        foundDataUploadTypes.length === 0 && selectedResourceTypes.length > 0;

    // init countries
    const [selectableCountries, setSelectableCountries] = useState<
        SelectableCountry[]
    >([]);
    const countries: string[] = [];
    const urlCountries = searchParams.getAll(UrlSearchParameterType.Countries);
    if (urlCountries?.length) {
        urlCountries.forEach((e) => e && urlCountries.push(e));
    }
    const [selectedCountries, setSelectedCountries] =
        useState<string[]>(countries);
    const foundCountries = CountryFilterEnable.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const countryFilterDisabled =
        foundCountries.length === 0 && selectedResourceTypes.length > 0;

    // init publisher
    const [selectablePublisher, setSelectablePublisher] = useState<
        SelectablePublisher[]
    >([]);
    const publisher: string[] = [];
    const urlPublisher = searchParams.getAll(UrlSearchParameterType.Publisher);
    if (urlPublisher?.length) {
        urlPublisher.forEach((e) => e && urlPublisher.push(e));
    }
    const [selectedPublisher, setSelectedPublisher] =
        useState<string[]>(publisher);
    const foundPublisher = PublisherFilterEnable.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const publisherFilterDisabled =
        foundPublisher.length === 0 && selectedResourceTypes.length > 0;

    // init api
    const [selectableApi, setSelectableApi] = useState<
        SelectableApi[]
    >([]);
    const api: string[] = [];
    const urlApi = searchParams.getAll(UrlSearchParameterType.Api);
    if (urlApi?.length) {
        urlApi.forEach((e) => e && urlApi.push(e));
    }
    const [selectedApi, setSelectedApi] =
        useState<string[]>(api);
    const foundApi = ApiFilterEnable.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const apiFilterDisabled =
        foundApi.length === 0 && selectedResourceTypes.length > 0;

    // init data license 
    const [selectableDataLicense, setSelectableDataLicense] = useState<
        SelectableDataLicense[]
    >([]);
    const dataLicense: string[] = [];
    const urlDataLicense = searchParams.getAll(UrlSearchParameterType.DataLicense);
    if (urlDataLicense?.length) {
        urlDataLicense.forEach((e) => e && urlDataLicense.push(e));
    }
    const [selectedDataLicense, setSelectedDataLicense] =
        useState<string[]>(dataLicense);
    const foundDataLicense = DataLicenseFilterEnable.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const dataLicenseFilterDisabled =
        foundDataLicense.length === 0 && selectedResourceTypes.length > 0;

    // init metadata standards
    const [selectableSupportedMetadataStandards, setSelectableSupportedMetadataStandards] =
        useState<SelectableSupportedMetadataStandard[]>([]);
    const supportedMetadataStandards: string[] = [];
    const urlSupportedMetadataStandards = searchParams.getAll(
        UrlSearchParameterType.SupportedMetadataStandards
    );
    if (urlSupportedMetadataStandards?.length) {
        urlSupportedMetadataStandards.forEach((e) => e && urlSupportedMetadataStandards.push(e));
    }
    const [selectedSupportedMetadataStandards, setSelectedSupportedMetadataStandards] = useState<
        string[]
    >(supportedMetadataStandards);
    const foundSupportedMetadataStandards = SupportedMetadataStandardFilterEnable.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const supportedMetadataStandardFilterDisabled =
        foundSupportedMetadataStandards.length === 0 && selectedResourceTypes.length > 0;

    // init licenses
    const [selectableLicenses, setSelectableLicenses] = useState<SelectableLicense[]>([]);
    const licenses: string[] = [];
    const urlLicenses = searchParams.getAll(UrlSearchParameterType.Licenses);
    if (urlLicenses?.length) {
        urlLicenses.forEach((e) => e && urlLicenses.push(e));
    }
    const [selectedLicenses, setSelectedLicenses] = useState<string[]>(licenses);
    const foundLicenses = LicensesFilterEnable.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const licensesFilterDisabled = foundLicenses.length === 0 && selectedResourceTypes.length > 0;

    // init spatial filter
    let sp: number[] = [];
    const coordString = searchParams.get(UrlSearchParameterType.SpatialFilter);
    if (coordString) {
        const coords = coordString.split(",").map((c) => Number.parseFloat(c));
        if (coords.length === 4) {
            sp = coords;
        }
    }
    const [spatialFilter, setSpatialFilter] = useState(sp);

    // init temporal filter
    const tempFilterString = searchParams.get(UrlSearchParameterType.TemporalFilter);
    let tf: TemporalFilter | undefined = undefined;
    if (tempFilterString) {
        const [s, e] = tempFilterString.split(",");
        if (s && e) {
            const start = parseInt(s, 10);
            const end = parseInt(e, 10);
            if (!isNaN(start) && !isNaN(end)) {
                tf = { startYear: start, endYear: end };
            }
        }
    }
    const [temporalFilter, setTemporalFilter] = useState<TemporalFilter | undefined>(tf);
    const [temporalFacets, setTemporalFacets] = useState<TemporalFacet[]>([]);

    // check disabling spatial filter
    const tempMatches = TemporalFilterEnableForResourceTypes.filter(
        (res) => selectedResourceTypes.findIndex((e) => e === res) >= 0
    );
    const temporalFilterDisabled = tempMatches.length === 0 && selectedResourceTypes.length > 0;

    // sorting
    const sortString = searchParams.get(UrlSearchParameterType.SortingFilter);
    const sortMatch = SortOptions.find((so) => so.term === sortString);
    const sort = sortMatch || SortOptions[0];
    const [sorting, setSorting] = useState<SortOption | undefined>(sort);

    const [doiOption, setDoiOption] = useState<boolean>(false);
    const [selectedFacet, setSelectedFacet] = useState<string>("find");
    const [prevPath, setPrevPath] = useState<string>("/");

    function search() {
        setIsLoaded(false);
        searchSrvc
            .doSearch({
                searchTerm,
                resourceTypes: selectedResourceTypes,
                subjects: selectedSubjects,
                dataAccessTypes: selectedDataAccessTypes,
                dataFormatTypes: selectedDataFormatTypes,
                dataUploadTypes: selectedDataUploadTypes,
                supportedMetadataStandards: selectedSupportedMetadataStandards,
                licenses: selectedLicenses,
                countryNames: selectedCountries,
                publisher: selectedPublisher,
                hasAPI_conformsTo: selectedApi,
                dataLicense: selectedDataLicense,
                spatialFilter,
                doiOption,
                pageSize,
                pageStart,
                temporalFilter,
                temporalConfig: {
                    startYear: TemporalFacetStartYear,
                    endYear: TemporalFacetEndYear,
                    gap: TemporalFacetGap
                },
                sorting: sorting?.term
            })
            .then((result) => {
                setIsLoaded(true);
                setSearchResults(result);
                const resourceTypeFacet = result.facets.resourceType.map((e) => {
                    return {
                        resourceType: e.resourceType,
                        count: e.count,
                        selected: selectedResourceTypes.findIndex((r) => r === e.resourceType) >= 0
                    } as SelectableResourceType;
                });

                const countryFacet = result.facets.countries.map((e) => {
                    return {
                        count: e.count,
                        label: e.label,
                        selected: selectedCountries.findIndex((r) => r === e.label) >= 0
                    } as SelectableCountry;
                });

                const publisherFacet = result.facets.publisher.map((e) => {
                    return {
                        count: e.count,
                        label: e.label,
                        selected: selectedPublisher.findIndex((r) => r === e.label) >= 0
                    } as SelectablePublisher;
                });

                const apiFacet = result.facets.hasAPI_conformsTo.map((e) => {
                    return {
                        count: e.count,
                        label: e.label,
                        selected: selectedApi.findIndex((r) => r === e.label) >= 0
                    } as SelectableApi;
                });

                const dataLicenseFacet = result.facets.dataLicense.map((e) => {
                    return {
                        count: e.count,
                        label: e.label,
                        selected: selectedDataLicense.findIndex((r) => r === e.label) >= 0
                    } as SelectableDataLicense;
                });

                const dataAccessTypeFacet = result.facets.dataAccessType.map((e) => {
                    return {
                        count: e.count,
                        label: e.label,
                        selected: selectedDataAccessTypes.findIndex((r) => r === e.label) >= 0
                    } as SelectableDataAccessType;
                });

                const dataFormatTypeFacet = result.facets.dataFormatType.map((e) => {
                    return {
                        count: e.count,
                        label: e.label,
                        selected: selectedDataFormatTypes.findIndex((r) => r === e.label) >= 0
                    } as SelectableDataFormatType;
                });

                const dataUploadTypeFacet = result.facets.dataUploadType.map((e) => {
                    return {
                        count: e.count,
                        label: e.label,
                        selected: selectedDataUploadTypes.findIndex((r) => r === e.label) >= 0
                    } as SelectableDataUploadType;
                });

                const supportedMetadataStandardFacet = result.facets.supportedMetadataStandard.map(
                    (e) => {
                        return {
                            count: e.count,
                            label: e.label,
                            selected:
                                selectedSupportedMetadataStandards.findIndex(
                                    (r) => r === e.label
                                ) >= 0
                        } as SelectableSupportedMetadataStandard;
                    }
                );

                const licensesFacet = result.facets.licenses.map((e) => {
                    return {
                        count: e.count,
                        label: e.label,
                        selected: selectedLicenses.findIndex((r) => r === e.label) >= 0
                    } as SelectableLicense;
                });

                setSelecteableResourceTypes(resourceTypeFacet);
                setSelectableDataAccessTypes(dataAccessTypeFacet);
                setSelectableDataFormatTypes(dataFormatTypeFacet);
                setSelectableDataUploadTypes(dataUploadTypeFacet);
                setSelectableSupportedMetadataStandards(supportedMetadataStandardFacet);
                setSelectableLicenses(licensesFacet);
                setSelectableCountries(countryFacet);
                setSelectablePublisher(publisherFacet);
                setSelectableApi(apiFacet);
                setSelectableDataLicense(dataLicenseFacet);
                handleSubjects(result.facets.subjects);
                handleTemporalFacets(result.facets.temporal);
            })
            .catch((error) => {
                setIsLoaded(true);
                console.error(error);
            });

        function handleSubjects(subjects: SubjectEntry[]) {
            function removeZeroCounts(subjectConf: SelectableSubject[]): SelectableSubject[] {
                subjectConf.forEach((e) => (e.children = removeZeroCounts(e.children)));
                return subjectConf.filter((e) => e.count !== undefined);
            }

            function adjustEntry(entry: SubjectEntry, tree: SelectableSubject[]) {
                tree.find((e) => adjustEntry(entry, e.children));
                const match = tree.find((e) => e.label === entry.label);
                if (match) {
                    match.count = entry.count;
                    match.selected = selectedSubjects.findIndex((s) => s === entry.label) >= 0;
                }
            }

            fetch("/subject-config.json").then((result) => { 
                result.json().then((subjectConf: SelectableSubject[]) => {
                    subjects.forEach((entry) => adjustEntry(entry, subjectConf));
                    removeZeroCounts(subjectConf);
                    setSelecteableSubjects(subjectConf);
                });
            });
        }

        function handleTemporalFacets(facets: TemporalFacet[]) {
            setTemporalFacets(facets);
        }
    }

    const state: ISearchState = {
        searchTerm,
        setSearchTerm: (value) => {
            setSearchTerm(value);
            setPageStart(0);
        },
        selectedResourceTypes,
        setSelectedResourceTypes: (values) => {
            setSelectedResourceTypes(values);
            setPageStart(0);
        },
        selectableResourceTypes,
        spatialFilter,
        setSpatialFilter: (value) => {
            setSpatialFilter(value);
            setPageStart(0);
        },
        spatialFilterDisabled,
        dataAccessTypeFilterDisabled,
        dataFormatTypeFilterDisabled,
        dataUploadTypeFilterDisabled,
        supportedMetadataStandardFilterDisabled,
        publisherFilterDisabled,
        apiFilterDisabled,
        dataLicenseFilterDisabled,
        countryFilterDisabled,
        licensesFilterDisabled,
        temporalFilter,
        setTemporalFilter: (tf) => {
            setTemporalFilter(tf);
            setPageStart(0);
        },
        temporalFacets,
        temporalFilterDisabled,
        pageSize,
        setPageSize: (value) => {
            setPageSize(value);
            setPageStart(0);
        },
        pageStart,
        setPageStart,
        searchResults,
        isLoaded,
        sorting,
        setSorting(sortOption) {
            setSorting(sortOption);
            setPageStart(0);
        },
        search,

        selectedSubjects,
        setSelectedSubjects: (value) => {
            setSelectedSubjects(value);
            setPageStart(0);
        },
        selectableSubjects,

        selectedDataAccessTypes,
        setSelectedDataAccessTypes: (value) => {
            setSelectedDataAccessTypes(value);
            !dataAccessTypeFilterDisabled ? setPageStart(0) : null;
        },
        selectableDataAccessTypes,

        selectedCountries,
        setSelectedCountries: (value) => {
            setSelectedCountries(value);
            !countryFilterDisabled ? setPageStart(0) : null;
        },
        selectableCountries,

        selectedPublisher,
        setSelectedPublisher: (value) => {
            setSelectedPublisher(value);
            !publisherFilterDisabled ? setPageStart(0) : null;
        },
        selectablePublisher,

        selectedApi,
        setSelectedApi: (value) => {
            setSelectedApi(value);
            !apiFilterDisabled ? setPageStart(0) : null;
        },
        selectableApi,

        selectedDataLicense,
        setSelectedDataLicense: (value) => {
            setSelectedDataLicense(value);
            !dataLicenseFilterDisabled ? setPageStart(0) : null;
        },
        selectableDataLicense,

        selectedDataFormatTypes,
        setSelectedDataFormatTypes: (value) => {
            setSelectedDataFormatTypes(value);
            !dataFormatTypeFilterDisabled ? setPageStart(0) : null;
        },
        selectableDataFormatTypes,

        selectedDataUploadTypes,
        setSelectedDataUploadTypes: (value) => {
            setSelectedDataUploadTypes(value);
            !dataUploadTypeFilterDisabled ? setPageStart(0) : null;
        },
        selectableDataUploadTypes,

        selectedSupportedMetadataStandards,
        setSelectedSupportedMetadataStandards: (value) => {
            setSelectedSupportedMetadataStandards(value);
            !supportedMetadataStandardFilterDisabled ? setPageStart(0) : null;
        },
        selectableSupportedMetadataStandards,

        selectedLicenses,
        setSelectedLicenses: (value) => {
            setSelectedLicenses(value);
            !licensesFilterDisabled ? setPageStart(0) : null;
        },
        selectableLicenses,

        doiOption,
        setDoiOption: (value) => {
            setDoiOption(value);
            setPageStart(0);
        },

        selectedFacet,        
        setSelectedFacet,

        prevPath,
        setPrevPath
    };

    return (
        <>
            <SearchStateContext.Provider value={state}>
                {props.children}
            </SearchStateContext.Provider>
        </>
    );
};
