import { Box, Button } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { SelectableCountry, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";

import { useIntl } from "open-pioneer:react-hooks";

export function CountryFacet(props: { expanded?: boolean; fontSize?: string; opacity?: number;}) {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectableCountry[]>([]);
    const [disabled, setDisable] = useState(false);
    const [showMore, setShowMore] = useState(false);
    const { expanded, fontSize, opacity } = props;

    const intl = useIntl();

    useEffect(() => {
        if (searchState.countryFilterDisabled) {
            searchState.setSelectedCountries([]);
            setDisable(searchState.countryFilterDisabled);
        } else {
            setDisable(false);
            if (searchState.selectableCountries) {
                if (searchState.selectableCountries.length > 10) {
                    setEntries(
                        searchState.selectableCountries.sort((a, b) => b.count - a.count)
                    );
                } else {
                    setEntries(
                        searchState.selectableCountries.sort((a, b) =>
                            a.label.toLocaleUpperCase().localeCompare(b.label.toLocaleUpperCase())
                        )
                    );
                }
            }
        }
    }, [searchState.selectableCountries, searchState.countryFilterDisabled]);

    function countriesToggled(checked: boolean, country: string) {
        if (checked) {
            searchState.setSelectedCountries([
                ...searchState.selectedCountries,
                country
            ]);
        } else {
            searchState.setSelectedCountries(
                searchState.selectedCountries.filter((e) => e !== country)
            );
        }
    }

    const displayedEntries = showMore ? entries : entries.slice(0, 10);

    return !disabled ? (
        <Box className="facet-space-rest">
            <FacetBase
                title={intl.formatMessage({ id: "search.facets.country" })}
                expanded={expanded}
                fontSize={fontSize ? fontSize : "12px"}
                opacity={opacity ? opacity : 0.5}
            >
                {displayedEntries.map((entry, i) => (
                    <Box key={i} padding="4px 0px">
                        <FacetCheckbox
                            label={entry.label}
                            count={entry.count}
                            isChecked={entry.selected}
                            onChange={(event) =>
                                countriesToggled(event.target.checked, entry.label)
                            }
                        />
                    </Box>
                ))}
                {entries.length > 10 && (
                    <Button
                        onClick={() => setShowMore(!showMore)}
                        size="sm"
                        marginTop="8px"
                    >
                        {showMore ? intl.formatMessage({ id: "search.facets.showLess" }) : intl.formatMessage({ id: "search.facets.showMore" })}
                    </Button>
                )}
            </FacetBase>
        </Box>
    ) : (
        <></>
    );
}
