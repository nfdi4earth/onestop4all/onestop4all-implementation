import { Box } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { SelectableSubject, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { SubjectTreeEntry } from "./SubjectTreeEntry";

import { useIntl } from "open-pioneer:react-hooks";

export function SubjectFacet(props: { expanded?: boolean; wizard?: boolean; fontSize?: string; opacity?: number; question?: string; note?: string}) {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectableSubject[]>([]);
    const { expanded, wizard, fontSize, opacity, question, note } = props;

    const intl = useIntl();

    const richTextIntl = {
        bold: (chunks: string[]) =>
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            chunks.map((chunk, i) => (<b key={`bold_${i}`}>{chunks[0]}</b>) as any)
    };

    const wizardLinks = {
        subjectLink: (
            <a
                className="link"
                target="_blank"
                key="1"
                rel="noreferrer"
                href={intl.formatMessage({
                    id: "search.facets.subject-help.links.subjectLinkUrl"
                })}
            >
                {intl.formatMessage({
                    id: "search.facets.subject-help.links.subjectLinkLabel"
                })}
            </a>
        )
    };

    useEffect(() => {
        if (searchState.selectableSubjects) {
            setEntries(
                searchState.selectableSubjects.sort((a, b) =>
                    a.label.toLocaleUpperCase().localeCompare(b.label.toLocaleUpperCase())
                )
            );
        }
    }, [searchState.selectableSubjects]);

    return (
        <Box className="facet-space-rest">
            <FacetBase 
                title={
                    wizard
                        ? intl.formatMessage({ id: question })
                        : intl.formatMessage({ id: "search.facets.subject" })
                }                
                expanded={expanded}
                fontSize={fontSize ? fontSize : "12px"}
                opacity={opacity ? opacity : 0.5}
            >
                {wizard ? <Box paddingY={3}>{intl.formatMessage(
                    { id: note },
                    {
                        ...richTextIntl,
                        ...wizardLinks
                    }
                )}</Box> : null}
                {entries.map((entry, i) => (
                    <Box key={i} padding="4px 0px">
                        <SubjectTreeEntry entry={entry}></SubjectTreeEntry>
                    </Box>
                ))}
            </FacetBase>
        </Box>
    );
}
