import { Box, Button } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { SelectableDataFormatType, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";

import { useIntl } from "open-pioneer:react-hooks";

export function DataFormatTypeFacet(props: { expanded?: boolean; wizard?: boolean; fontSize?: string; opacity?: number; question?: string; note?: string}) {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectableDataFormatType[]>([]);
    const [disabled, setDisable] = useState(false);
    const [showMore, setShowMore] = useState(false);
    const { expanded, wizard, fontSize, opacity, question, note } = props;

    const intl = useIntl();

    useEffect(() => {
        if (searchState.dataFormatTypeFilterDisabled) {
            searchState.setSelectedDataFormatTypes([]);
            setDisable(searchState.dataFormatTypeFilterDisabled);
        } else {
            setDisable(false);
            if (searchState.selectableDataFormatTypes) {
                if (searchState.selectableDataFormatTypes.length > 10) {
                    setEntries(
                        searchState.selectableDataFormatTypes.sort((a, b) => b.count - a.count)
                    );
                } else {
                    setEntries(
                        searchState.selectableDataFormatTypes.sort((a, b) =>
                            a.label.toLocaleUpperCase().localeCompare(b.label.toLocaleUpperCase())
                        )
                    );
                }
            }
        }
    }, [searchState.selectableDataFormatTypes, searchState.dataFormatTypeFilterDisabled]);

    function dataFormatTypeToggled(checked: boolean, dataFormatType: string) {
        if (checked) {
            searchState.setSelectedDataFormatTypes([
                ...searchState.selectedDataFormatTypes,
                dataFormatType
            ]);
        } else {
            searchState.setSelectedDataFormatTypes(
                searchState.selectedDataFormatTypes.filter((e) => e !== dataFormatType)
            );
        }
    }

    const displayedEntries = showMore ? entries : entries.slice(0, 10);

    return !disabled ? (
        <Box className="facet-space-rest">
            <FacetBase
                title={
                    wizard
                        ? intl.formatMessage({ id: question })
                        : intl.formatMessage({ id: "search.facets.data-format-type" })
                }
                expanded={expanded}
                fontSize={fontSize ? fontSize : "12px"}
                opacity={opacity ? opacity : 0.5}
            >
                {wizard ? 
                    <Box paddingY={3}>
                        {intl.formatMessage({ id: note })}
                    </Box> : null}
                {displayedEntries.map((entry, i) => (
                    <Box key={i} padding="4px 0px">
                        <FacetCheckbox
                            label={entry.label}
                            count={entry.count}
                            isChecked={entry.selected}
                            onChange={(event) =>
                                dataFormatTypeToggled(event.target.checked, entry.label)
                            }
                        />
                    </Box>
                ))}
                {entries.length > 10 && (
                    <Button
                        onClick={() => setShowMore(!showMore)}
                        size="sm"
                        marginTop="8px"
                    >
                        {showMore ? intl.formatMessage({ id: "search.facets.showLess" }) : intl.formatMessage({ id: "search.facets.showMore" })}
                    </Button>
                )}
            </FacetBase>
        </Box>
    ) : (
        <></>
    );
}
