import { Box, Button } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { SelectableDataLicense, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";

import { useIntl } from "open-pioneer:react-hooks";

export function DataLicenseFacet(props: { expanded?: boolean; fontSize?: string; opacity?: number; }) {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectableDataLicense[]>([]);
    const [disabled, setDisable] = useState(false);
    const [showMore, setShowMore] = useState(false);
    const { expanded, fontSize, opacity } = props;

    const intl = useIntl();

    useEffect(() => {
        if (searchState.dataLicenseFilterDisabled) {
            searchState.setSelectedDataLicense([]);
            setDisable(searchState.dataLicenseFilterDisabled);
        } else {
            setDisable(false);
            if (searchState.selectableDataLicense) {
                if (searchState.selectableDataLicense.length > 10) {
                    setEntries(
                        searchState.selectableDataLicense.sort((a, b) => b.count - a.count)
                    );
                } else {
                    setEntries(
                        searchState.selectableDataLicense.sort((a, b) =>
                            a.label.toLocaleUpperCase().localeCompare(b.label.toLocaleUpperCase())
                        )
                    );
                }
            }
        }
    }, [searchState.selectableDataLicense, searchState.dataLicenseFilterDisabled]);

    function dataLicenseToggled(checked: boolean, dataLicense: string) {
        console.log(dataLicense);
        if (checked) {
            searchState.setSelectedDataLicense([
                ...searchState.selectedDataLicense,
                dataLicense
            ]);
        } else {
            searchState.setSelectedDataLicense(
                searchState.selectedDataLicense.filter((e) => e !== dataLicense)
            );
        }
    }

    const displayedEntries = showMore ? entries : entries.slice(0, 10);

    return !disabled ? (
        <Box className="facet-space-rest">
            <FacetBase
                title={intl.formatMessage({ id: "search.facets.dataLicense" })}
                expanded={expanded}
                fontSize={fontSize || "12px"}
                opacity={opacity || 0.5}
            >
                {displayedEntries.map((entry, i) =>
                    <Box key={i} padding="4px 0px">
                        <FacetCheckbox
                            label={
                                entry.label.length > 50
                                    ? `${entry.label.substring(0, 50)}...`
                                    : entry.label
                            }
                            count={entry.count}
                            isChecked={entry.selected}
                            onChange={(event) =>
                                dataLicenseToggled(event.target.checked, entry.label)
                            }
                            title={entry.label}
                        />
                    </Box>
                )}
                {entries.length > 10 && (
                    <Button
                        onClick={() => setShowMore(!showMore)}
                        size="sm"
                        marginTop="8px"
                    >
                        {showMore ? intl.formatMessage({ id: "search.facets.showLess" }) : intl.formatMessage({ id: "search.facets.showMore" })}
                    </Button>
                )}
            </FacetBase>
        </Box>
    ) : null;
}
