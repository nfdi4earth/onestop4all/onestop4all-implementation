import { Box, Button } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { SelectableApi, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";

import { useIntl } from "open-pioneer:react-hooks";

export function ApiFacet(props: { expanded?: boolean; fontSize?: string; opacity?: number; }) {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectableApi[]>([]);
    const [disabled, setDisable] = useState(false);
    const [showMore, setShowMore] = useState(false);
    const { expanded, fontSize, opacity } = props;

    const intl = useIntl();

    useEffect(() => {
        if (searchState.apiFilterDisabled) {
            searchState.setSelectedApi([]);
            setDisable(searchState.apiFilterDisabled);
        } else {
            setDisable(false);
            if (searchState.selectableApi) {
                if (searchState.selectableApi.length > 10) {
                    setEntries(
                        searchState.selectableApi.sort((a, b) => b.count - a.count)
                    );
                } else {
                    setEntries(
                        searchState.selectableApi.sort((a, b) =>
                            a.label.toLocaleUpperCase().localeCompare(b.label.toLocaleUpperCase())
                        )
                    );
                }
            }
        }
    }, [searchState.selectableApi, searchState.apiFilterDisabled]);

    function apiToggled(checked: boolean, api: string) {
        if (checked) {
            searchState.setSelectedApi([
                ...searchState.selectedApi,
                api
            ]);
        } else {
            searchState.setSelectedApi(
                searchState.selectedApi.filter((e) => e !== api)
            );
        }
    }

    const displayedEntries = showMore ? entries : entries.slice(0, 10);

    return !disabled ? (
        <Box className="facet-space-rest">
            <FacetBase
                title={intl.formatMessage({ id: "search.facets.api" })}
                expanded={expanded}
                fontSize={fontSize || "12px"}
                opacity={opacity || 0.5}
            >
                {displayedEntries.map((entry, i) =>
                    <Box key={i} padding="4px 0px">
                        <FacetCheckbox
                            label={
                                entry.label.length > 50
                                    ? `${entry.label.substring(0, 50)}...`
                                    : entry.label
                            }
                            count={entry.count}
                            isChecked={entry.selected}
                            onChange={(event) =>
                                apiToggled(event.target.checked, entry.label)
                            }
                            title={entry.label}
                        />
                    </Box>
                )}
                {entries.length > 10 && (
                    <Button
                        onClick={() => setShowMore(!showMore)}
                        size="sm"
                        marginTop="8px"
                    >
                        {showMore ? intl.formatMessage({ id: "search.facets.showLess" }) : intl.formatMessage({ id: "search.facets.showMore" })}
                    </Button>
                )}
            </FacetBase>
        </Box>
    ) : null;
}
