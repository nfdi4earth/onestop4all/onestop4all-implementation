import { Box, Button } from "@open-pioneer/chakra-integration";
import { useEffect, useState, useMemo } from "react";
import { SelectableDataAccessType, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";
import { useIntl } from "open-pioneer:react-hooks";

export function DataAccessTypeFacet({
    expanded,
    wizard,
    fontSize = "12px",
    opacity = 0.5,
    question,
    note,
    selectedWizard
}: {
    expanded?: boolean;
    wizard?: boolean;
    fontSize?: string;
    opacity?: number;
    question?: string;
    note?: string;
    selectedWizard?: string;
}) {
    const searchState = useSearchState();
    const intl = useIntl();
    const [showMore, setShowMore] = useState(false);
    
    const wizardLabels = {
        find: {
            open: "I need open data",
            restricted: "I am ok with registration",
            embargoed: "I am ok with embargoed data",
            closed: "I am ok with closed data",
        },
        publish: {
            open: "I want to publish my data openly",
            restricted: "I need to publish my data with registration",
            embargoed: "I need to publish my data with an embargo period",
            closed: "I need to publish my data in a closed way",
        }
    };

    useEffect(() => {
        if (searchState.dataAccessTypeFilterDisabled) {
            searchState.setSelectedDataAccessTypes([]);
        }
    }, [searchState.dataAccessTypeFilterDisabled]);

    const sortedEntries = useMemo(() => {
        const entries = searchState.selectableDataAccessTypes || [];
        return entries.length > 10
            ? [...entries].sort((a, b) => b.count - a.count)
            : [...entries].sort((a, b) => a.label.localeCompare(b.label, undefined, { sensitivity: "base" }));
    }, [searchState.selectableDataAccessTypes]);

    const displayedEntries = showMore ? sortedEntries : sortedEntries.slice(0, 10);

    function getLabel(entry: SelectableDataAccessType): string {
        if (!wizard) return entry.label;
    
        const selectedKey = selectedWizard === "find" || selectedWizard === "publish" ? selectedWizard : null;
        return selectedKey ? wizardLabels[selectedKey][entry.label as keyof typeof wizardLabels.find] || entry.label : entry.label;
    }

    function toggleDataAccessType(checked: boolean, dataAccessType: string) {
        searchState.setSelectedDataAccessTypes(checked
            ? [...searchState.selectedDataAccessTypes, dataAccessType]
            : searchState.selectedDataAccessTypes.filter(e => e !== dataAccessType)
        );
    }

    if (searchState.dataAccessTypeFilterDisabled) return null;

    return (
        <Box className="facet-space-rest">
            <FacetBase
                title={wizard ? intl.formatMessage({ id: question }) : intl.formatMessage({ id: "search.facets.data-access-type" })}
                expanded={expanded}
                fontSize={fontSize}
                opacity={opacity}
            >
                {wizard && note && <Box paddingY={3}>{intl.formatMessage({ id: note })}</Box>}
                
                {displayedEntries.map((entry, i) => (
                    <Box key={entry.label} padding="4px 0px">
                        <FacetCheckbox
                            label={getLabel(entry)}
                            count={entry.count}
                            isChecked={entry.selected}
                            onChange={(event) => toggleDataAccessType(event.target.checked, entry.label)}
                        />
                    </Box>
                ))}

                {sortedEntries.length > 10 && (
                    <Button onClick={() => setShowMore(!showMore)} size="sm" marginTop="8px">
                        {intl.formatMessage({ id: showMore ? "search.facets.showLess" : "search.facets.showMore" })}
                    </Button>
                )}
            </FacetBase>
        </Box>
    );
}
