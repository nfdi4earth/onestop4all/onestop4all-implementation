import { Box, FormControl, FormLabel } from "@open-pioneer/chakra-integration";
import { Switch } from "@chakra-ui/react";
import { useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";

import { useIntl } from "open-pioneer:react-hooks";

export function DoiOptionFacet(props: { expanded?: boolean; wizard?: boolean; fontSize?: string; opacity?: number; question?: string, note?: string}) {
    const searchState = useSearchState();
    const { expanded, wizard, fontSize, opacity, question, note } = props;

    const handleSwitcherChange = () => {
        searchState.setDoiOption(!searchState.doiOption);
    };

    const intl = useIntl();

    const richTextIntl = {
        bold: (chunks: string[]) =>
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            chunks.map((chunk, i) => (<b key={`bold_${i}`}>{chunks[0]}</b>) as any)
    };

    const wizardLinks = {
        doiOptionLink: (
            <a
                className="link"
                target="_blank"
                key="1"
                rel="noreferrer"
                href={intl.formatMessage({
                    id: "search.facets.doiOption-help.links.doiOptionLinkUrl"
                })}
            >
                {intl.formatMessage({
                    id: "search.facets.doiOption-help.links.doiOptionLinkLabel"
                })}
            </a>
        )
    };

    return (
        <Box className="facet-space-rest">
            <FacetBase 
                title={
                    wizard
                        ? intl.formatMessage({ id: question })
                        : intl.formatMessage({ id: "search.facets.doiOption" })
                }                
                expanded={expanded}
                fontSize={fontSize ? fontSize : "12px"}
                opacity={opacity ? opacity : 0.5}
            >
                {wizard ? <Box paddingY={3}>{intl.formatMessage(
                    { id: note },
                    {
                        ...richTextIntl,
                        ...wizardLinks
                    }
                )}</Box> : null}
                <FormControl display='flex' alignItems='center'>
                    <FormLabel htmlFor='download-links' mb='0' style={{color: "#05668D", fontSize: "14px", fontWeight: "400"}}>
                        {searchState.doiOption ? intl.formatMessage({ id: "yes" }) : intl.formatMessage({ id: "no" })}
                    </FormLabel>
                    <Switch id='download-links' onChange={handleSwitcherChange} isChecked={searchState.doiOption ? true : false} />
                </FormControl>
            </FacetBase>
        </Box>
    );
}