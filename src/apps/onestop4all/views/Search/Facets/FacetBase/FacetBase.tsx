import {
    Accordion,
    AccordionButton,
    AccordionIcon,
    AccordionItem,
    AccordionPanel,
    Box,
    SystemStyleObject
} from "@open-pioneer/chakra-integration";
import { CSSProperties, PropsWithChildren } from "react";

import { DropdownArrowIcon } from "../../../../components/Icons";

export interface FacetBaseProps {
    title: string;
    expanded?: boolean;
    fontSize?: string;
    opacity?: number;
}

export function FacetBase(props: PropsWithChildren<FacetBaseProps>) {
    const { title, children, expanded, fontSize, opacity } = props;

    const labelStyles: CSSProperties = {
        paddingInline: 0,
        flex: 1,
        textAlign: "left",
        fontSize: fontSize ? fontSize : "12px",
        fontWeight: 700,
        letterSpacing: "0.6px",
        textTransform: "uppercase",
        opacity: opacity ? opacity : 0.5
    };

    function getIconStyles(expanded: boolean): SystemStyleObject {
        return {
            transform: expanded ? "rotate(-180deg)" : undefined,
            transition: "transform 0.2s",
            transformOrigin: "center"
        };
    }

    return (
        <Accordion allowMultiple defaultIndex={expanded ? [0] : []}>
            <AccordionItem border="0px">
                {({ isExpanded }) => (
                    <>
                        <AccordionButton padding="0">
                            <Box as='span' flex='1' textAlign='left' style={labelStyles}>
                                {title}
                            </Box>
                            <DropdownArrowIcon  __css={getIconStyles(isExpanded)}/>
                        </AccordionButton>
                        <AccordionPanel padding="0">
                            <Box padding="8px 0">{children}</Box>
                            <Box className="seperator"></Box>
                        </AccordionPanel>
                    </>
                )}
            </AccordionItem>
        </Accordion>
    );
}