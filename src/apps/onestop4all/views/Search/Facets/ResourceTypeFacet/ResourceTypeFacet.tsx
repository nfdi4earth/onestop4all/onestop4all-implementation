import { Box } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { ResourceType } from "../../../../services/ResourceTypeUtils";
import { SelectableResourceType, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";

import { useIntl } from "open-pioneer:react-hooks";

export function ResourceTypeFacet() {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectableResourceType[]>([]);

    const intl = useIntl();

    useEffect(() => {
        if (searchState.selectableResourceTypes) {
            setEntries(
                searchState.selectableResourceTypes.sort((a, b) =>
                    a.resourceType
                        .toLocaleUpperCase()
                        .localeCompare(b.resourceType.toLocaleUpperCase())
                )
            );
        }
    }, [searchState.selectableResourceTypes]);

    function resourceTypeToggled(checked: boolean, resourceType: ResourceType) {
        if (checked) {
            searchState.setSelectedResourceTypes([
                ...searchState.selectedResourceTypes,
                resourceType
            ]);
        } else {
            searchState.setSelectedResourceTypes(
                searchState.selectedResourceTypes.filter((e) => e !== resourceType)
            );
        }
    }

    return (
        <Box className="facet-space-top">
            <FacetBase title={intl.formatMessage({ id: "search.facets.resource-type" })} expanded>
                {entries.map((entry, i) => {
                    return (
                        <Box key={i} padding="4px 0px">
                            <FacetCheckbox
                                label={intl.formatMessage({
                                    id: `resource-types.${entry.resourceType}`
                                })}
                                count={entry.count}
                                isChecked={entry.selected}
                                onChange={(event) =>
                                    resourceTypeToggled(event.target.checked, entry.resourceType)
                                }
                            />
                        </Box>
                    );
                })}
            </FacetBase>
        </Box>
    );
}
