import { Box, Button } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { SelectablePublisher, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";

import { useIntl } from "open-pioneer:react-hooks";

export function PublisherFacet(props: { expanded?: boolean; fontSize?: string; opacity?: number; }) {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectablePublisher[]>([]);
    const [disabled, setDisable] = useState(false);
    const [showMore, setShowMore] = useState(false);
    const { expanded, fontSize, opacity } = props;

    const intl = useIntl();

    useEffect(() => {
        if (searchState.publisherFilterDisabled) {
            searchState.setSelectedPublisher([]);
            setDisable(searchState.publisherFilterDisabled);
        } else {
            setDisable(false);
            if (searchState.selectablePublisher) {
                if (searchState.selectablePublisher.length > 10) {
                    setEntries(
                        searchState.selectablePublisher.sort((a, b) => b.count - a.count)
                    );
                } else {
                    setEntries(
                        searchState.selectablePublisher.sort((a, b) =>
                            a.label.toLocaleUpperCase().localeCompare(b.label.toLocaleUpperCase())
                        )
                    );
                }
            }
        }
    }, [searchState.selectablePublisher, searchState.publisherFilterDisabled]);

    function publisherToggled(checked: boolean, publisher: string) {
        if (checked) {
            searchState.setSelectedPublisher([
                ...searchState.selectedPublisher,
                publisher
            ]);
        } else {
            searchState.setSelectedPublisher(
                searchState.selectedPublisher.filter((e) => e !== publisher)
            );
        }
    }

    const displayedEntries = showMore ? entries : entries.slice(0, 10);

    return !disabled ? (
        <Box className="facet-space-rest">
            <FacetBase
                title={intl.formatMessage({ id: "search.facets.publisher" })}
                expanded={expanded}
                fontSize={fontSize || "12px"}
                opacity={opacity || 0.5}
            >
                {displayedEntries.map((entry, i) =>
                    !entry.label.startsWith("https://cordra.knowledgehub") ? (
                        <Box key={i} padding="4px 0px">
                            <FacetCheckbox
                                label={
                                    entry.label.length > 50
                                        ? `${entry.label.substring(0, 50)}...`
                                        : entry.label
                                }
                                count={entry.count}
                                isChecked={entry.selected}
                                onChange={(event) =>
                                    publisherToggled(event.target.checked, entry.label)
                                }
                                title={entry.label}
                            />
                        </Box>
                    ) : null
                )}
                {entries.length > 10 && (
                    <Button
                        onClick={() => setShowMore(!showMore)}
                        size="sm"
                        marginTop="8px"
                    >
                        {showMore ? intl.formatMessage({ id: "search.facets.showLess" }) : intl.formatMessage({ id: "search.facets.showMore" })}
                    </Button>
                )}
            </FacetBase>
        </Box>
    ) : null;
}
