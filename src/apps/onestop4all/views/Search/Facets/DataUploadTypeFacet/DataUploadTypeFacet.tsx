import { Box, Button } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { SelectableDataUploadType, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";

import { useIntl } from "open-pioneer:react-hooks";

export function DataUploadTypeFacet(props: { expanded?: boolean; showHelp?: boolean }) {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectableDataUploadType[]>([]);
    const [disabled, setDisable] = useState(false);
    const [showMore, setShowMore] = useState(false);
    const { expanded, showHelp } = props;
    const intl = useIntl();

    useEffect(() => {
        if (searchState.dataUploadTypeFilterDisabled) {
            searchState.setSelectedDataUploadTypes([]);
            setDisable(searchState.dataUploadTypeFilterDisabled);
        } else {
            setDisable(false);
            if (searchState.selectableDataUploadTypes) {
                if (searchState.selectableDataUploadTypes.length > 10) {
                    setEntries(
                        searchState.selectableDataUploadTypes.sort((a, b) => b.count - a.count)
                    );
                } else {
                    setEntries(
                        searchState.selectableDataUploadTypes.sort((a, b) =>
                            a.label.toLocaleUpperCase().localeCompare(b.label.toLocaleUpperCase())
                        )
                    );
                }
            }
        }
    }, [searchState.selectableDataUploadTypes, searchState.dataUploadTypeFilterDisabled]);

    function dataUploadTypeToggled(checked: boolean, dataUploadType: string) {
        if (checked) {
            searchState.setSelectedDataUploadTypes([
                ...searchState.selectedDataUploadTypes,
                dataUploadType
            ]);
        } else {
            searchState.setSelectedDataUploadTypes(
                searchState.selectedDataUploadTypes.filter((e) => e !== dataUploadType)
            );
        }
    }

    const displayedEntries = showMore ? entries : entries.slice(0, 10);

    return !disabled ? (
        <Box className="facet-space-rest">
            <FacetBase
                title={
                    showHelp
                        ? "What kind of data upload type do you wish the repository to have?"
                        : intl.formatMessage({ id: "search.facets.data-upload-type" })
                }
                expanded={expanded}
            >
                {showHelp ? <Box>Note: Data upload types are important because...</Box> : null}
                {displayedEntries.map((entry, i) => (
                    <Box key={i} padding="4px 0px">
                        <FacetCheckbox
                            label={entry.label}
                            count={entry.count}
                            isChecked={entry.selected}
                            onChange={(event) =>
                                dataUploadTypeToggled(event.target.checked, entry.label)
                            }
                        />
                    </Box>
                ))}
                {entries.length > 10 && (
                    <Button
                        onClick={() => setShowMore(!showMore)}
                        size="sm"
                        marginTop="8px"
                    >
                        {showMore ? intl.formatMessage({ id: "search.facets.showLess" }) : intl.formatMessage({ id: "search.facets.showMore" })}
                    </Button>
                )}
            </FacetBase>
        </Box>
    ) : (
        <></>
    );
}
