import { Box, Button } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { SelectableLicense, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";

import { useIntl } from "open-pioneer:react-hooks";

export function LicensesFacet() {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectableLicense[]>([]);
    const [disabled, setDisable] = useState(false);
    const [showMore, setShowMore] = useState(false);

    const intl = useIntl();

    useEffect(() => {
        if (searchState.licensesFilterDisabled) {
            searchState.setSelectedLicenses([]);
            setDisable(searchState.licensesFilterDisabled);
        } else {
            setDisable(false);
            if (searchState.selectableLicenses) {
                if (searchState.selectableLicenses.length > 10) {
                    setEntries(
                        searchState.selectableLicenses.sort((a, b) => b.count - a.count)
                    );
                } else {
                    setEntries(
                        searchState.selectableLicenses.sort((a, b) =>
                            a.label.toLocaleUpperCase().localeCompare(b.label.toLocaleUpperCase())
                        )
                    );
                }
            }
        }
    }, [searchState.selectableLicenses, searchState.licensesFilterDisabled]);

    function licensesToggled(checked: boolean, license: string) {
        if (checked) {
            searchState.setSelectedLicenses([...searchState.selectedLicenses, license]);
        } else {
            searchState.setSelectedLicenses(
                searchState.selectedLicenses.filter((e) => e !== license)
            );
        }
    }

    const displayedEntries = showMore ? entries : entries.slice(0, 10);

    return !disabled ? (
        <Box className="facet-space-rest">
            <FacetBase title={intl.formatMessage({ id: "search.facets.license" })} expanded={true}>
                {displayedEntries.map((entry, i) => (
                    <Box key={i} padding="4px 0px">
                        <FacetCheckbox
                            label={
                                entry.label.startsWith("http")
                                    ? entry.label.split("/").pop()
                                    : entry.label
                            }
                            count={entry.count}
                            isChecked={entry.selected}
                            onChange={(event) => licensesToggled(event.target.checked, entry.label)}
                        />
                    </Box>
                ))}
                {entries.length > 10 && (
                    <Button
                        onClick={() => setShowMore(!showMore)}
                        size="sm"
                        marginTop="8px"
                    >
                        {showMore ? intl.formatMessage({ id: "search.facets.showLess" }) : intl.formatMessage({ id: "search.facets.showMore" })}
                    </Button>
                )}
            </FacetBase>
        </Box>
    ) : (
        <></>
    );
}
