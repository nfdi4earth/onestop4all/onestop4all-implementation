import { Box, Button } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { SelectableSupportedMetadataStandard, useSearchState } from "../../SearchState";
import { FacetBase } from "../FacetBase/FacetBase";
import { FacetCheckbox } from "../FacetBase/FacetCheckbox";

import { useIntl } from "open-pioneer:react-hooks";

export function SupportedMetadataStandardFacet(props: { expanded?: boolean; showHelp?: boolean }) {
    const searchState = useSearchState();
    const [entries, setEntries] = useState<SelectableSupportedMetadataStandard[]>([]);
    const [disabled, setDisable] = useState(false);
    const [showMore, setShowMore] = useState(false);
    const { expanded, showHelp } = props;
    const intl = useIntl();

    useEffect(() => {
        if (searchState.supportedMetadataStandardFilterDisabled) {
            searchState.setSelectedSupportedMetadataStandards([]);
            setDisable(searchState.supportedMetadataStandardFilterDisabled);
        } else {
            setDisable(false);
            if (searchState.selectableSupportedMetadataStandards) {
                if (searchState.selectableSupportedMetadataStandards.length > 10) {
                    setEntries(
                        searchState.selectableSupportedMetadataStandards.sort((a, b) => b.count - a.count)
                    );
                } else {
                    setEntries(
                        searchState.selectableSupportedMetadataStandards.sort((a, b) =>
                            a.label.toLocaleUpperCase().localeCompare(b.label.toLocaleUpperCase())
                        )
                    );
                }
            }
        }
    }, [
        searchState.selectableSupportedMetadataStandards,
        searchState.supportedMetadataStandardFilterDisabled
    ]);

    function selectedMetadataStandardToggled(checked: boolean, metadataStandard: string) {
        if (checked) {
            searchState.setSelectedSupportedMetadataStandards([
                ...searchState.selectedSupportedMetadataStandards,
                metadataStandard
            ]);
        } else {
            searchState.setSelectedSupportedMetadataStandards(
                searchState.selectedSupportedMetadataStandards.filter((e) => e !== metadataStandard)
            );
        }
    }

    const displayedEntries = showMore ? entries : entries.slice(0, 10);

    return !disabled ? (
        <Box className="facet-space-rest">
            <FacetBase
                title={
                    showHelp
                        ? "Which metadata standards should the repository support?"
                        : intl.formatMessage({ id: "search.facets.supported-metadata-standard" })
                }
                expanded={expanded}
            >
                {showHelp ? <Box>Note: Metadata standards are important because...</Box> : null}
                {displayedEntries.map((entry, i) => (
                    <Box key={i} padding="4px 0px">
                        <FacetCheckbox
                            label={
                                entry.label.length > 50
                                    ? `${entry.label.substring(0, 50)}...`
                                    : entry.label
                            }
                            count={entry.count}
                            isChecked={entry.selected}
                            onChange={(event) =>
                                selectedMetadataStandardToggled(event.target.checked, entry.label)
                            }
                        />
                    </Box>
                ))}
                {entries.length > 10 && (
                    <Button
                        onClick={() => setShowMore(!showMore)}
                        size="sm"
                        marginTop="8px"
                    >
                        {showMore ? intl.formatMessage({ id: "search.facets.showLess" }) : intl.formatMessage({ id: "search.facets.showMore" })}
                    </Button>
                )}
            </FacetBase>
        </Box>
    ) : (
        <></>
    );
}
