import {
    Box,
    Flex,
    Icon,
    Menu,
    MenuButton,
    MenuItem,
    MenuList
} from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { DropdownArrowIcon } from "../../../components/Icons";
import { PrimaryColor } from "../../../Theme";
import { SortOption, SortOptions, useSearchState } from "../SearchState";

import { useIntl } from "open-pioneer:react-hooks";

export function SortedBySelector() {
    const [currentSorting, setCurrentSorting] = useState<SortOption>();
    const searchState = useSearchState();

    const intl = useIntl();

    useEffect(() => {
        if (searchState.sorting) {
            setCurrentSorting(searchState.sorting);
        }
    }, [searchState.sorting]);

    function changeSorting(sort: SortOption): void {
        searchState.setSorting(sort);
        setCurrentSorting(sort);
    }

    return (
        <Box fontSize="12px">
            <Menu>
                {/*Desktop view*/}
                <MenuButton as={Box} whiteSpace="nowrap" _hover={{ cursor: "pointer" }}>
                    <Flex alignItems="center" gap="4px">
                        <Box>{intl.formatMessage({ id: "search.sorted-by" })}:</Box>
                        <Box color={PrimaryColor} fontWeight="700">
                            {currentSorting && intl.formatMessage({ id: currentSorting.label })}
                        </Box>
                        <Icon boxSize="3" color={PrimaryColor}>
                            <DropdownArrowIcon />
                        </Icon>
                    </Flex>
                </MenuButton>
                <MenuList>
                    {SortOptions.map((e, i) => (
                        <MenuItem key={i} onClick={() => changeSorting(e)} backgroundColor={searchState.sorting?.term === e.term ? "#90EE90" : ""}>
                            {intl.formatMessage({ id: e.label })}
                        </MenuItem>
                    ))}
                </MenuList>
            </Menu>
        </Box>
    );
}
