import { Box, Button, Container, Flex, Spacer } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";
import { useEffect, useRef, useState } from "react";
import { createSearchParams, useNavigate, useSearchParams } from "react-router-dom";
import { FilterIcon } from "../../components/Icons";
import { SearchBar } from "../../components/SearchBar";
import { BorderColor, PrimaryColor } from "../../Theme";
import { Chips } from "./Chips/Chips";
import { MobileFilterMenu } from "./Facets/MobileFilterMenu/MobileFilterMenu";
import { ResourceTypeFacet } from "./Facets/ResourceTypeFacet/ResourceTypeFacet";
import { SpatialCoverageFacet } from "./Facets/SpatialCoverageFacet/SpatialCoverageFacet";
import { SubjectFacet } from "./Facets/SubjectFacet/SubjectFacet";
import { DataAccessTypeFacet } from "./Facets/DataAccessTypeFacet/DataAccessTypeFacet";
import { DataUploadTypeFacet } from "./Facets/DataUploadTypeFacet/DataUploadTypeFacet";
import { SupportedMetadataStandardFacet } from "./Facets/SupportedMetadataStandardFacet/SupportedMetadataStandardFacet";
import { LicensesFacet } from "./Facets/LicensesFacet/LicensesFacet";
import { TemporalCoverageFacet } from "./Facets/TemporalCoverageFacet/TemporalCoverageFacet";
import { CountryFacet } from "./Facets/CountryFacet/CountryFacet";
import { PublisherFacet } from "./Facets/PublisherFacet/PublisherFacet";
import { ResultCountSelector } from "./ResultCountSelector/ResultCountSelector";
import { ResultPaging } from "./ResultPaging/ResultPaging";
import { SearchResult } from "./SearchResult/SearchResult";
import {
    DEFAULT_PAGE_SIZE,
    UrlSearchParameterType,
    UrlSearchParams,
    useSearchState
} from "./SearchState";
import { SortedBySelector } from "./SortedBySelector/SortedBySelector";
import { DataFormatTypeFacet } from "./Facets/DataFormatTypeFacet/DataFormatTypeFacet";
import { ApiFacet } from "./Facets/ApiFacet/ApiFacet";
import { DataLicenseFacet } from "./Facets/DataLicenseFacet/DataLicenseFacet";

export function SearchView() {
    const searchState = useSearchState();
    const [searchParams] = useSearchParams();
    const navigate = useNavigate();

    const intl = useIntl();
    const loadingText = intl.formatMessage({ id: "resource-type-header.loading" });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        searchState.search();
    }, [searchParams]);

    useEffect(() => {
        /*if (location && location.state && location.state.pathname) {
            searchState.setPrevPath(location.state.prevPath);
        }*/
        const onBackButtonEvent = (e: any) => {
            e.preventDefault();
            //const currentLocation = window.location.pathname;
            //console.log(location.state, searchState.prevPath);
            console.log(searchState.prevPath);
            if (searchState.prevPath === "/search") {
                navigate("/");
            } else {
                navigate(searchState.prevPath);
            }
        };
        window.addEventListener("popstate", onBackButtonEvent);
        return () => {
            window.removeEventListener("popstate", onBackButtonEvent);
        };
    }, [navigate]);


    useEffect(() => {
        const params: UrlSearchParams = {};

        if (searchState.searchTerm) {
            params[UrlSearchParameterType.Searchterm] = searchState.searchTerm;
        }

        if (searchState.selectedResourceTypes.length > 0) {
            params[UrlSearchParameterType.ResourceType] = searchState.selectedResourceTypes;
        }

        if (searchState.selectedSubjects.length > 0) {
            params[UrlSearchParameterType.Subjects] = searchState.selectedSubjects;
        }

        if (searchState.selectableDataAccessTypes.length > 0) {
            params[UrlSearchParameterType.DataAccessTypes] = searchState.selectedDataAccessTypes;
        }

        if (searchState.selectableDataFormatTypes.length > 0) {
            params[UrlSearchParameterType.DataFormatTypes] = searchState.selectedDataFormatTypes;
        }

        if (searchState.selectableDataUploadTypes.length > 0) {
            params[UrlSearchParameterType.DataUploadTypes] = searchState.selectedDataUploadTypes;
        }

        if (searchState.selectableSupportedMetadataStandards.length > 0) {
            params[UrlSearchParameterType.SupportedMetadataStandards] = 
                searchState.selectedSupportedMetadataStandards;
        }

        if (searchState.selectableLicenses.length > 0) {
            params[UrlSearchParameterType.Licenses] = searchState.selectedLicenses;
        }

        if (searchState.selectableCountries.length > 0) {
            params[UrlSearchParameterType.Countries] = searchState.selectedCountries;
        }

        if (searchState.selectablePublisher.length > 0) {
            params[UrlSearchParameterType.Publisher] = searchState.selectedPublisher;
        }

        if (searchState.selectableApi.length > 0) {
            params[UrlSearchParameterType.Api] = searchState.selectedApi;
        }

        if (searchState.selectableDataLicense.length > 0) {
            params[UrlSearchParameterType.DataLicense] = searchState.selectedDataLicense;
        }

        if (searchState.spatialFilter.length === 4) {
            params[UrlSearchParameterType.SpatialFilter] = searchState.spatialFilter.join(",");
        }

        if (searchState.pageSize && searchState.pageSize !== DEFAULT_PAGE_SIZE) {
            params[UrlSearchParameterType.PageSize] = `${searchState.pageSize}`;
        }

        if (searchState.pageStart && searchState.pageStart !== 0) {
            params[UrlSearchParameterType.PageStart] = `${searchState.pageStart}`;
        }

        if (searchState.temporalFilter) {
            params[
                UrlSearchParameterType.TemporalFilter
            ] = `${searchState.temporalFilter.startYear},${searchState.temporalFilter.endYear}`;
        }

        if (searchState.sorting) {
            params[UrlSearchParameterType.SortingFilter] = `${searchState.sorting.term}`;
        }

        navigate({
            pathname: "/search",
            search: `?${createSearchParams({ ...params })}`
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        searchState.searchTerm,
        searchState.selectedResourceTypes,
        searchState.selectedSubjects,
        searchState.selectedDataAccessTypes,
        searchState.selectedDataUploadTypes,
        searchState.selectedDataFormatTypes,
        searchState.selectedSupportedMetadataStandards,
        searchState.selectedLicenses,
        searchState.selectedCountries,
        searchState.selectedPublisher,
        searchState.selectedApi,
        searchState.selectedDataLicense,
        searchState.spatialFilter,
        searchState.pageSize,
        searchState.pageStart,
        searchState.temporalFilter,
        searchState.sorting
    ]);

    const [openMenu, setOpenMenu] = useState(false);

    const menu = useRef(null);

    return (
        <Box className="search-view">
            <Box position="relative">
                <Box className="header-image" />
            </Box>

            <Box
                position="absolute"
                width="100%"
                marginTop={{ base: "-40px", custombreak: "-50px" }}
            >
                <Container maxW={{ base: "100%", custombreak: "80%" }}>
                    <SearchBar />
                </Container>
            </Box>

            <Box height={{ base: "100px", custombreak: "80px" }}></Box>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                <Flex gap="1vw">
                    {searchState.isLoaded ? (
                        <Box flex="1 1 100%" overflow="hidden">
                            <Flex flexDirection={{ base: "column", custombreak: "row" }}>
                                <Box className="results-count">
                                    {searchState.searchResults?.count}{" "}
                                    {intl.formatMessage({
                                        id: "search.results-header"
                                    })}
                                </Box>
                                <Box hideFrom="custombreak" padding="20px 0px">
                                    <ResultPaging />
                                </Box>
                                <Spacer />
                                <Flex
                                    gap="5px"
                                    alignItems="center"
                                    justifyContent="space-between"
                                    padding={{ base: "0 0 15px", custombreak: "0" }}
                                >
                                    <ResultCountSelector />
                                    <Box flex="0 0 1px" bgColor={BorderColor} alignSelf="stretch" />
                                    <SortedBySelector />
                                    <Box
                                        flex="0 0 1px"
                                        bgColor={BorderColor}
                                        alignSelf="stretch"
                                        hideFrom="custombreak"
                                    />
                                    <Box hideFrom="custombreak">
                                        <Button
                                            leftIcon={<FilterIcon />}
                                            variant="ghost"
                                            color={PrimaryColor}
                                            onClick={() => setOpenMenu(true)}
                                        >
                                            <Box hideBelow="500px">Filter</Box>
                                        </Button>
                                    </Box>
                                </Flex>
                            </Flex>
                            <Box hideBelow="custombreak" padding={{ base: "40px 0px" }}>
                                <Chips></Chips>
                            </Box>
                            <Box>
                                {searchState.searchResults?.results.map((e) => {
                                    return (
                                        <Box key={e.id}>
                                            <Box className="seperator" />
                                            <Box padding={{ base: "40px 0px" }}>
                                                <SearchResult item={e} />
                                            </Box>
                                        </Box>
                                    );
                                })}
                            </Box>
                            <Box className="seperator" />
                            <Box hideFrom="custombreak" padding="40px 0px">
                                <ResultPaging />
                            </Box>
                        </Box>
                    ) : (
                        <Box flex="1 1 100%" overflow="hidden">
                            {loadingText}
                        </Box>
                    )}

                    <Flex flex="0 0 30%" hideBelow="custombreak" flexDirection="column">
                        <Box>
                            <ResultPaging />
                        </Box>
                        <Box ref={menu}>
                            <ResourceTypeFacet />
                        </Box>
                        <SubjectFacet expanded={true}/>
                        <DataAccessTypeFacet expanded={true}/>
                        <DataUploadTypeFacet expanded={true}/>
                        <DataFormatTypeFacet expanded={true}/>
                        <CountryFacet expanded={true}/>
                        <PublisherFacet expanded={true} />
                        <ApiFacet expanded={true} />
                        <DataLicenseFacet expanded={true} />
                        <SupportedMetadataStandardFacet expanded={true}/>
                        <LicensesFacet />
                        <SpatialCoverageFacet mapId="spatial-filter-map" />
                        <TemporalCoverageFacet />
                        <Spacer />
                        <Box>
                            <ResultPaging />
                        </Box>
                    </Flex>
                </Flex>
                <MobileFilterMenu
                    openMenu={openMenu}
                    menuClosed={() => setOpenMenu(false)}
                ></MobileFilterMenu>
            </Container>
        </Box>
    );
}
