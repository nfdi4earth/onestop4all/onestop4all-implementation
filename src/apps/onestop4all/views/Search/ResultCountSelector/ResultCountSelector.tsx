import {
    Box,
    Flex,
    Icon,
    Menu,
    MenuButton,
    MenuItem,
    MenuList
} from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";
import { useEffect, useState } from "react";

import { DropdownArrowIcon } from "../../../components/Icons";
import { PrimaryColor } from "../../../Theme";
import { SELECTABLE_PAGES_SIZES, useSearchState } from "../SearchState";

export function ResultCountSelector() {
    const [currentPageSize, setCurrentPageSize] = useState(10);
    const searchState = useSearchState();
    const pageOptions = SELECTABLE_PAGES_SIZES;

    const intl = useIntl();

    useEffect(() => setCurrentPageSize(searchState.pageSize), [searchState.pageSize]);

    function changeResult(result:number): void {
        searchState.setPageSize(result);
    }

    return (
        <Box fontSize="12px">
            <Menu>
                {/*Desktop view*/}
                <MenuButton as={Box} whiteSpace="nowrap" _hover={{ cursor: "pointer" }}>
                    <Flex alignItems="center" gap="4px">
                        <Box>{intl.formatMessage({ id: "search.result-count-selector" })}</Box>
                        <Box color={PrimaryColor} fontWeight="700">
                            {currentPageSize}
                        </Box>
                        <Icon boxSize="3" color={PrimaryColor}>
                            <DropdownArrowIcon />
                        </Icon>
                    </Flex>
                </MenuButton>
                <MenuList>
                    {pageOptions.map((e, i) => (
                        <MenuItem key={i} onClick={() => changeResult(e)} backgroundColor={searchState.pageSize === e ? "#90EE90" : ""}>
                            {e} {intl.formatMessage({ id: "search.results" })}
                        </MenuItem>
                    ))}
                </MenuList>
            </Menu>
        </Box>
    );
}
