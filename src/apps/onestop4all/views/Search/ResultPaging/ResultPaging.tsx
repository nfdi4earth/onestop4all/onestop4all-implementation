import { Box } from "@open-pioneer/chakra-integration";
import { useEffect, useState } from "react";

import { ResultsNavigation } from "../../../components/ResultsNavigation/ResultsNavigation";
import { useSearchState } from "../SearchState";

import { useIntl } from "open-pioneer:react-hooks";

export function ResultPaging(props: {refer?:any}) {
    const {refer} = props;
    const [page, setPage] = useState<number>();
    const [pageCount, setPageCount] = useState<number>();
    const searchState = useSearchState();

    const intl = useIntl();

    useEffect(() => {
        if (searchState.searchResults) {
            setPage(searchState.pageStart + 1);
            const pages = Math.ceil(searchState.searchResults.count / searchState.pageSize);
            setPageCount(pages);
        }
    }, [searchState.pageSize, searchState.pageStart, searchState.searchResults]);

    function stepBack(): void {
        if (page) {
            searchState.setPageStart(page - 2);
        }
    }

    function stepForward(): void {
        if (page) {
            searchState.setPageStart(page);
        }
    }

    function stepToEnd(): void {
        if (pageCount) {
            searchState.setPageStart(pageCount - 1);
        }
    }

    function stepToStart(): void {
        searchState.setPageStart(0);
    }

    function renderPaging(): import("react").ReactNode {
        if (page !== undefined && pageCount !== undefined) {
            return (
                <ResultsNavigation
                    result={page}
                    of={pageCount}
                    label={intl.formatMessage({ id: "search.page" })}
                    stepBack={stepBack}
                    stepFoward={stepForward}
                    stepToEnd={stepToEnd}
                    stepToStart={stepToStart}
                    refer={refer}
                />
            );
        } else {
            return <></>;
        }
    }

    return <Box>{renderPaging()}</Box>;
}
