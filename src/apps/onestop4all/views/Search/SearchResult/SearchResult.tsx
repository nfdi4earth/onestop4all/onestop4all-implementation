import { Box, Flex, SystemStyleObject } from "@open-pioneer/chakra-integration";
import { DateTime } from "luxon";
import { Link } from "react-router-dom";

import { SearchResultItem } from "../../../services/SearchService";
import { BorderColor, PrimaryColor } from "../../../Theme";
import { ResourceIcon } from "../../Start/ResourceEntry/ResourceIcons";
import { useSearchState } from "../SearchState";

import { useIntl } from "open-pioneer:react-hooks";

export interface SearchResultProps {
    item: SearchResultItem;
}

export function SearchResult(props: SearchResultProps) {
    const { item } = props;

    const searchState = useSearchState();

    const idx = searchState.searchResults?.results.findIndex((r) => r.id === item.id) || 0;
    const resultPage = idx + 1 + searchState.pageSize * searchState.pageStart;

    const intl = useIntl();

    const hoverStyle: SystemStyleObject = {
        cursor: "pointer",
        backgroundColor: "var(--primary-primary-transparent-background)"
    };
    const intlResourceType = intl.formatMessage({ id: `resource-types.${item.resourceType}` });

    return (
        <Link
            to={`/result/${item.id}`}
            state={{ resultPage: resultPage, prevPath: location.pathname }}
            style={{ textDecoration: "none" }}
        >
            <Flex alignItems="center" _hover={hoverStyle}>
                <Box className="search-result" flex="1" overflow={"hidden"}>
                    <Flex gap="8px">
                        <Box className="resource-type">{intlResourceType}</Box>
                        {dateSection()}
                        {locationSection()}
                    </Flex>
                    <Flex gap="8px" padding="8px 0">
                        <Box>
                            <ResourceIcon
                                type={item.resourceType}
                                size={24}
                                color={PrimaryColor}
                            ></ResourceIcon>
                        </Box>
                        <Box flex="0 0 1px" bgColor={BorderColor} alignSelf="stretch" />
                        <Box className="title">
                            {Array.isArray(item.title) ? item.title[0] : item.mainTitle ? item.mainTitle : item.title}{/*item.sourceSystemURL && item.sourceSystemURL[0].startsWith("https://edutrain.n") ? " **************LEARNING RESOURCE" : ""*/}
                        </Box>
                    </Flex>
                    <Box className="abstract">{item.abstract}</Box>
                </Box>
                <Box flex="0 0 75px" hideBelow="custombreak">
                    <svg
                        width="76"
                        height="76"
                        viewBox="0 0 76 76"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path opacity="0.3" d="M44 7.5L75 38L44 68.5" stroke="#05668D" />
                    </svg>
                </Box>
            </Flex>
        </Link>
    );

    function dateSection() {
        if (props.item.publishDate) {
            return (
                <>
                    <Box flex="0 0 1px" bgColor={BorderColor} alignSelf="stretch" />
                    <Box className="date">
                        {intl.formatMessage({ id: "metadata.published" })}
                        {": "}
                        {intl.formatDate(props.item.publishDate, {
                            year: "numeric",
                            month: "long",
                            day: "numeric"
                        })}
                    </Box>
                </>
            );
        }
        /*if (props.item.updateDate) {
            return (
                <>
                    <Box flex="0 0 1px" bgColor={BorderColor} alignSelf="stretch" />
                    <Box className="date">
                        Updated: {DateTime.fromJSDate(props.item.updateDate).toFormat("MMMM yyyy")}
                    </Box>
                </>
            );
        }*/
    }

    function locationSection() {
        return props.item.locality ? (
            <>
                <Box flex="0 0 1px" bgColor={BorderColor} alignSelf="stretch" />
                <Box className="date">{props.item.locality}</Box>
            </>
        ) : null;
    }
}
