import { Link } from "react-router-dom";

import { useIntl } from "open-pioneer:react-hooks";

export function FourOFour() {
    const intl = useIntl();
    //intl variables
    const errorText = intl.formatMessage({ id: "error.page-not-found" });
    const oopsText = intl.formatMessage({ id: "error.oops" });
    const returnToHomeText = intl.formatMessage({ id: "error.return-to-home" });
    const goToHomeText = intl.formatMessage({ id: "error.go-to-home" });
    return (
        <div className="four-o-four-container">
            <h1 className="fourOFourHeader">{errorText}</h1>
            <p>{oopsText}</p>
            <p>{returnToHomeText}</p>
            <Link to="/" className="fourOFourLink">
                {goToHomeText}
            </Link>
        </div>
    );
}
