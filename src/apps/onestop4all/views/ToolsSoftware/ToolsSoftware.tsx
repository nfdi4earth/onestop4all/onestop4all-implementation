/* eslint-disable */
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { Link } from "react-router-dom";

import { Abstract } from "../../components/ResourceType/Abstract/Abstract";
import { ActionButton } from "../../components/ResourceType/ActionButton/ActionButton";
import { Metadata } from "../../components/ResourceType/Metadata/Metadata";
import { SolrSearchResultItem } from "../../services/SearchService";
import { MetadataSourceIcon } from "../../components/Icons";
import { CopyToClipboardButton } from "../../components/ResourceType/ActionButton/CopyToClipboardButton";

import { useIntl } from "open-pioneer:react-hooks";

export interface ToolsSoftwareMetadataResponse extends SolrSearchResultItem {
    name: string;
    description: string;
    codeRepository: string;
    keyword: string;
    license: string;
    uri: string;
    url: string;
    relatedContent: Array<object>;
    programmingLanguage: Array<string>;
    sourceSystem_homepage: string;
    sourceSystem_title: string;
}

export interface ToolsSoftwareViewProps {
    item: ToolsSoftwareMetadataResponse;
}

export function ToolsSoftwareView(props: ToolsSoftwareViewProps) {
    const metadata = props.item;
    const intl = useIntl();
    //intl variables
    const typeText = intl.formatMessage({ id: "metadata.type" });
    const keywordsText = intl.formatMessage({ id: "metadata.keywords" });
    const keywordText = intl.formatMessage({ id: "metadata.keyword" });
    const licensesText = intl.formatMessage({ id: "metadata.licenses" });
    const licenseText = intl.formatMessage({ id: "metadata.license" });
    const programmingLanguagesText = intl.formatMessage({ id: "metadata.programming-languages" });
    const programmingLanguageText = intl.formatMessage({ id: "metadata.programming-language" });
    const visitProjectPageText = intl.formatMessage({ id: "metadata.visit-project-page" });
    const visitCodeRepoText = intl.formatMessage({ id: "metadata.visit-code-repo" });
    const visitMetadataSourceText = intl.formatMessage({ id: "metadata.visit-source" });
    const copyUrlText = intl.formatMessage({ id: "metadata.copy-url" });
    const actionButtonsHeader = intl.formatMessage({ id: "actionButton.header" });

    return (
        <>
            {/* Desktop view */}
            <Flex gap="10%" hideBelow="custombreak">
                <Box w="65%">
                    {metadata.name || metadata.name_alt ? (
                        <Box className="title" pt="15px">
                            {metadata.name
                                ? Array.isArray(metadata.name)
                                    ? metadata.name[0]
                                    : metadata.name
                                : Array.isArray(metadata.name_alt)
                                ? metadata.name_alt[0]
                                : metadata.name_alt}
                        </Box>
                    ) : (
                        ""
                    )}
                    <Box pt="36px">{getMetadata()}</Box>
                    {metadata.description ? (
                        <Box pt="80px">
                            <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                        </Box>
                    ) : null}
                </Box>
                <Box w="25%">{getActions()}</Box>
            </Flex>
            {/* Mobile view */}
            <Box hideFrom="custombreak">
                {metadata.name || metadata.name_alt ? (
                    <Box className="title" pt="15px">
                        {metadata.name
                            ? Array.isArray(metadata.name)
                                ? metadata.name[0]
                                : metadata.name
                            : Array.isArray(metadata.name_alt)
                            ? metadata.name_alt[0]
                            : metadata.name_alt}
                    </Box>
                ) : (
                    ""
                )}
                <Box pt="20px">{getMetadata()}</Box>
                {metadata.description ? (
                    <Box pt="80px">
                        <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                    </Box>
                ) : null}
                <Box>{getActions()}</Box>
            </Box>
        </>
    );

    function getActions() {
        return metadata.url || metadata.codeRepository || metadata.sourceSystem_homepage ? (
            <Box className="actionButtonGroup" pt="50px">
                {metadata.url && metadata.url != "None" ? ( //Found this metadata value by accident. Actually an issue in KH.
                    <>
                        <Box className="actionButtonsHeader" id="actionButtonsHeader">
                            {actionButtonsHeader}
                        </Box>
                        <Link
                            to={metadata.url[0] as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={visitProjectPageText}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                    </>
                ) : null}
                {metadata.codeRepository ? (
                    <>
                        <Link
                            to={metadata.codeRepository[0] as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={visitCodeRepoText}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                        <CopyToClipboardButton data={metadata.url} label={copyUrlText} />
                    </>
                ) : null}
                {metadata.sourceSystem_homepage ? (
                    <Link
                        to={metadata.sourceSystem_homepage[0] as string}
                        className="actionButtonLink"
                        target="_blank"
                    >
                        <ActionButton
                            label={visitMetadataSourceText}
                            icon={<MetadataSourceIcon color="white" />}
                            variant="outline"
                            fun={() => void 0}
                        />
                    </Link>
                ) : null}
            </Box>
        ) : null;
    }

    function getMetadata() {
        return (
            <Metadata
                metadataElements={[
                    {
                        element: "type",
                        tag: typeText,
                        val: metadata.type
                    },
                    {
                        element: "keyword",
                        tag: metadata.keyword?.length > 1 ? keywordsText : keywordText,
                        val: metadata.keyword
                    },
                    {
                        element: "license",
                        tag: metadata.license?.length > 1 ? licensesText : licenseText,
                        val: metadata.license
                    },
                    {
                        element: "porgrammingLanguage",
                        tag:
                            metadata.programmingLanguage?.length > 1
                                ? programmingLanguagesText
                                : programmingLanguageText,
                        val: metadata.programmingLanguage
                    }
                ]}
                visibleElements={2}
                expandedByDefault={true}
            />
        );
    }
}
