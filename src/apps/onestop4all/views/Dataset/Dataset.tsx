/* eslint-disable */
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { Link } from "react-router-dom";

import { Abstract } from "../../components/ResourceType/Abstract/Abstract";
import { ActionButton } from "../../components/ResourceType/ActionButton/ActionButton";
import { Metadata } from "../../components/ResourceType/Metadata/Metadata";
import { Location } from "../../components/ResourceType/Location/Location";
import { SolrSearchResultItem } from "../../services/SearchService";

import { useIntl } from "open-pioneer:react-hooks";

export interface DatasetMetadataResponse extends SolrSearchResultItem {
    title: string;
    description: string;
    keyword: string;
}

export interface DatasetViewProps {
    item: DatasetMetadataResponse;
}

export function DatasetView(props: DatasetViewProps) {
    const metadata = props.item;

    const intl = useIntl();
    //intl variables
    const authorsText = intl.formatMessage({ id: "metadata.authors" });
    const authorText = intl.formatMessage({ id: "metadata.author" });
    const keywordsText = intl.formatMessage({ id: "metadata.keywords" });
    const keywordText = intl.formatMessage({ id: "metadata.keyword" });
    const typeText = intl.formatMessage({ id: "metadata.type" });
    const distributionText = intl.formatMessage({ id: "metadata.view-distribution" });
    const actionButtonsHeader = intl.formatMessage({ id: "actionButton.header" });
    const locationText = intl.formatMessage({ id: "metadata.location" });
    const publishedText = intl.formatMessage({ id: "metadata.published" });
    const visitDatasetText = intl.formatMessage({ id: "metadata.visit-dataset" });

    return (
        <>
            {/* Desktop view */}
            <Flex gap="10%" hideBelow="custombreak">
                <Box w="65%">
                    {metadata.title || metadata.altLabel ? (
                        <Box className="title" pt="15px">
                            {metadata.title
                                ? Array.isArray(metadata.title)
                                    ? metadata.title[0]
                                    : metadata.title
                                : Array.isArray(metadata.altLabel)
                                ? metadata.altLabel[0]
                                : metadata.altLabel}
                        </Box>
                    ) : (
                        ""
                    )}
                    <Box pt="36px">{getMetadata()}</Box>
                    {metadata.description ? (
                        <Box pt="40px">
                            <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                        </Box>
                    ) : null}
                    {metadata.geometry_wkt ? (
                    <Box pt="40px">
                        <Location
                            address={{
                                tag: locationText,
                                val: metadata.geometry_wkt
                            }}
                            location={metadata.geometry_wkt}
                            mapId="desktop"
                        />
                    </Box>
                ) : null}
                </Box>
                <Box w="25%">
                    {getActions()}
                </Box>
            </Flex>
            {/* Mobile view */}
            <Box hideFrom="custombreak">
                {metadata.title || metadata.altLabel ? (
                    <Box className="title" pt="15px">
                        {metadata.title
                            ? Array.isArray(metadata.title)
                                ? metadata.title[0]
                                : metadata.title
                            : Array.isArray(metadata.altLabel)
                            ? metadata.altLabel[0]
                            : metadata.altLabel}
                    </Box>
                ) : (
                    ""
                )}
                {metadata.geometry_wkt ? (
                    <Box pt="80px">
                        <Location
                            address={{
                                tag: locationText,
                                val: metadata.geometry_wkt
                            }}
                            location={metadata.geometry_wkt}
                            mapId="mobile"
                        />
                    </Box>
                ) : null}
                <Box pt="20px">{getMetadata()}</Box>
                <Box>{getActions()}</Box>
            </Box>
        </>
    );

    function getActions() {
        return metadata.homepage || metadata.distribution ? (
            <Box className="actionButtonGroup" pt="50px">
                {metadata.homepage ? (
                    <>
                        <Box className="actionButtonsHeader" id="actionButtonsHeader">
                            {actionButtonsHeader}
                        </Box>
                        <Link
                            to={metadata.homepage[0] as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={visitDatasetText}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                    </>
                ) : null}
                {metadata.distribution ? (
                    <>
                        <Box className="actionButtonsHeader" id="actionButtonsHeader">
                            {actionButtonsHeader}
                        </Box>
                        <Link
                            to={metadata.distribution[0] as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={distributionText}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                    </>
                ) : null}
            </Box>
        ) : null;
    }

    function getMetadata() {
        return (
            <Metadata
                metadataElements={[
                    {
                        element: "author",
                        tag: metadata.creator?.length > 1 ? authorsText : authorText,
                        val: metadata.creator
                    },
                    {
                        element: "keyword",
                        tag: metadata.keyword?.length > 1 ? keywordsText : keywordText,
                        val: metadata.keyword
                    },
                    {
                        element: "type",
                        tag: typeText,
                        val: metadata.type
                    },
                    {
                        element: "datePublished",
                        tag: publishedText,
                        val: metadata.datePublished
                            ? new Date(metadata.datePublished).toLocaleDateString()
                            : undefined
                    }
                ]}
                visibleElements={2}
                expandedByDefault={true}
            />
        );
    }
}
