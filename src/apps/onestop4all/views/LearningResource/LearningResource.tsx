/* eslint-disable */
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { Link } from "react-router-dom";
import { ExternalLinkIcon } from "@chakra-ui/icons";

import { Metadata } from "../../components/ResourceType/Metadata/Metadata";
import { Abstract } from "../../components/ResourceType/Abstract/Abstract";
import { SolrSearchResultItem } from "../../services/SearchService";
import { ActionButton } from "../../components/ResourceType/ActionButton/ActionButton";
import { CopyToClipboardButton } from "../../components/ResourceType/ActionButton/CopyToClipboardButton";

import { useIntl } from "open-pioneer:react-hooks";

export interface LearningResourceMetadataResponse extends SolrSearchResultItem {
    name: string;
    description: string;
    relatedContent: Array<object>;
    license: string;
    url: string;
    keyword: string; //
    publisher_alt: string;
    publisher: string;
    learningResourceType: string;
    datePublished: string;
    competencyRequired: string;
    about: string;
}

export interface LearningResourceViewProps {
    item: LearningResourceMetadataResponse;
}

export function LearningResourceView(props: LearningResourceViewProps) {
    const metadata = props.item;
    const intl = useIntl();

    // intl variables
    const authorsText = intl.formatMessage({ id: "metadata.authors" });
    const authorText = intl.formatMessage({ id: "metadata.author" });
    const keywordsText = intl.formatMessage({ id: "metadata.keywords" });
    const keywordText = intl.formatMessage({ id: "metadata.keyword" });
    const typeText = intl.formatMessage({ id: "metadata.type" });
    const publishedText = intl.formatMessage({ id: "metadata.published" });
    const licensesText = intl.formatMessage({ id: "metadata.licenses" });
    const licenseText = intl.formatMessage({ id: "metadata.license" });
    const visitSourceText = intl.formatMessage({ id: "metadata.visit-source" });
    const publishersText = intl.formatMessage({ id: "metadata.publishers" });
    const publisherText = intl.formatMessage({ id: "metadata.publisher" });
    const publishersAltText = intl.formatMessage({ id: "metadata.publishers-alt" });
    const publisherAltText = intl.formatMessage({ id: "metadata.publisher-alt" });
    const learningResTypesText = intl.formatMessage({ id: "metadata.learning-res-types" });
    const learningResTypeText = intl.formatMessage({ id: "metadata.learning-res-type" });
    const competencyReqText = intl.formatMessage({ id: "metadata.competency" });
    const aboutText = intl.formatMessage({ id: "metadata.about" });
    const visitWebsiteText = intl.formatMessage({ id: "metadata.visit-website" });
    const copyUrlText = intl.formatMessage({ id: "metadata.copy-url" });
    const actionButtonsHeader = intl.formatMessage({ id: "actionButton.header" });

    return (
        <>
            {/* Desktop view */}
            <Flex gap="10%" hideBelow="custombreak">
                <Box w="65%">
                    {metadata.name || metadata.name_alt ? (
                        <Box className="title" pt="15px">
                            {metadata.name
                                ? Array.isArray(metadata.name)
                                    ? metadata.name[0]
                                    : metadata.name
                                : Array.isArray(metadata.name_alt)
                                ? metadata.name_alt[0]
                                : metadata.name_alt}
                        </Box>
                    ) : (
                        ""
                    )}
                    <Box pt="36px">{getMetadata()}</Box>
                    {metadata.description ? (
                        <Box pt="40px">
                            <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                        </Box>
                    ) : null}
                </Box>
                <Box w="25%">{getActions()}</Box>
            </Flex>
            {/* Mobile view */}
            <Box hideFrom="custombreak">
                {metadata.name || metadata.name_alt ? (
                    <Box className="title" pt="15px">
                        {metadata.name
                            ? Array.isArray(metadata.name)
                                ? metadata.name[0]
                                : metadata.name
                            : Array.isArray(metadata.name_alt)
                            ? metadata.name_alt[0]
                            : metadata.name_alt}
                    </Box>
                ) : (
                    ""
                )}
                <Box pt="20px">{getMetadata()}</Box>
                {metadata.description ? (
                    <Box pt="80px">
                        <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                    </Box>
                ) : null}
                <Box>{getActions()}</Box>
            </Box>
        </>
    );

    function getActions() {
        return metadata.url || metadata.sourceSystem_homepage ? (
            <Box className="actionButtonGroup" pt="50px">
                {metadata.url ? (
                    <>
                        <Box className="actionButtonsHeader" id="actionButtonsHeader">
                            {actionButtonsHeader}
                        </Box>
                        <Link
                            to={metadata.url[0] as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={visitWebsiteText}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                        <CopyToClipboardButton data={metadata.url} label={copyUrlText} />
                    </>
                ) : null}
            </Box>
        ) : null;
    }

    function getMetadata() {
        return (
            <Metadata
                metadataElements={[
                    {
                        element: "author",
                        tag: metadata.author?.length > 1 ? authorsText : authorText,
                        val: metadata.author
                    },
                    {
                        element: "keyword",
                        tag: metadata.keyword?.length > 1 ? keywordsText : keywordText,
                        val: metadata.keyword
                    },
                    {
                        element: "license",
                        tag: metadata.license?.length > 1 ? licensesText : licenseText,
                        val: metadata.license
                    },
                    {
                        element: "type",
                        tag: typeText,
                        val: metadata.type
                    },
                    {
                        element: "publisher",
                        tag: metadata.publisher?.length > 1 ? publishersText : publisherText,
                        val: metadata.publisher
                    },
                    {
                        element: "publisherAlt",
                        tag:
                            metadata.publisher_alt?.length > 1
                                ? publishersAltText
                                : publisherAltText,
                        val: metadata.publisher_alt
                    },
                    {
                        element: "learningResourceType",
                        tag:
                            metadata.learningResourceType?.length > 1
                                ? learningResTypesText
                                : learningResTypeText,
                        val: metadata.learningResourceType
                    },
                    {
                        element: "competencyRequired",
                        tag: competencyReqText,
                        val: metadata.competencyRequired
                    },
                    {
                        element: "datePublished",
                        tag: publishedText,
                        val: metadata.datePublished
                            ? new Date(metadata.datePublished).getUTCFullYear()
                            : undefined
                    },
                    {
                        element: "about",
                        tag: aboutText,
                        val: metadata.about
                    }
                ]}
                visibleElements={2}
                expandedByDefault={true}
            />
        );
    }
}
