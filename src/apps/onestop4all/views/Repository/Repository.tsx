/* eslint-disable */
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { Link } from "react-router-dom";

import { InfoIcon } from "../../components/Icons";
import { Abstract } from "../../components/ResourceType/Abstract/Abstract";
import { ActionButton } from "../../components/ResourceType/ActionButton/ActionButton";
import { Api } from "../../components/ResourceType/Api_Identifier/Api";
import { LastUpdate } from "../../components/ResourceType/Metadata/LastUpdate";
import { Metadata } from "../../components/ResourceType/Metadata/Metadata";
import { SolrSearchResultItem } from "../../services/SearchService";
import { MetadataSourceIcon } from "../../components/Icons";
import { CopyToClipboardButton } from "../../components/ResourceType/ActionButton/CopyToClipboardButton";

import { useIntl } from "open-pioneer:react-hooks";

export interface RepositoryMetadataResponse extends SolrSearchResultItem {
    title: string;
    description: string;
    homepage: string;
    theme: string;
    keyword: string;
    contactPoint_email: string;
    contactPoint_url: string;
    publisher: string;
    publisher_homepage: string;
    altLabel: string;
    catalogAccessType: string;
    catalogLicense: string;
    catalogAccessRestriction: string;
    dataAccessType: string;
    dataLicense: string;
    dataUploadRestriction: string;
    dataUploadType: string;
    dataAccessRestriction: string;
    publisher_alt: string;
    supportsMetadataStandard: string;
    supportsMetadataStandard_homepage: string;
    subjectArea: string; 
    uri: string;
    relatedContent: Array<string>;
    relatedResources: Array<object>;
    repositoryType: string; 
    dateModified: string;
    sourceSystem_id: string;
    assignsIdentifierScheme: string;
    hasAPI_conformsTo: string[];
    hasAPI_url: string[];
    contentType: string;
    hasCertificate: string;
    supportsVersioning: string;
}

export interface RepositoryViewProps {
    item: RepositoryMetadataResponse;
}

export function RepositoryView(props: RepositoryViewProps) {
    const metadata = props.item;

    const intl = useIntl();
    //intl variables
    const urlsText = intl.formatMessage({ id: "metadata.urls" });
    const urlText = intl.formatMessage({ id: "metadata.url" });
    const themesText = intl.formatMessage({ id: "metadata.themes" });
    const themeText = intl.formatMessage({ id: "metadata.theme" });
    const keywordsText = intl.formatMessage({ id: "metadata.keywords" });
    const keywordText = intl.formatMessage({ id: "metadata.keyword" });
    const contactMailsText = intl.formatMessage({ id: "metadata.contact-mails" });
    const contactMailText = intl.formatMessage({ id: "metadata.contact-mail" });
    const contactUrlsText = intl.formatMessage({ id: "metadata.contact-urls" });
    const contactUrlText = intl.formatMessage({ id: "metadata.contact-url" });
    const contact = intl.formatMessage({ id: "footer.contact" });
    const publishersText = intl.formatMessage({ id: "metadata.publishers" });
    const publisherText = intl.formatMessage({ id: "metadata.publisher" });
    const publishersAltText = intl.formatMessage({ id: "metadata.publishers-alt" });
    const publisherAltText = intl.formatMessage({ id: "metadata.publisher-alt" });
    const altLabelsText = intl.formatMessage({ id: "metadata.alt-labels" });
    const altLabelText = intl.formatMessage({ id: "metadata.alt-label" });
    const catalogAccessTypesText = intl.formatMessage({ id: "metadata.catalog-access-types" });
    const catalogAccessTypeText = intl.formatMessage({ id: "metadata.catalog-access-type" });
    const catalogLicensesText = intl.formatMessage({ id: "metadata.catalog-licenses" });
    const catalogLicenseText = intl.formatMessage({ id: "metadata.catalog-license" });
    const catalogAccessRestsText = intl.formatMessage({
        id: "metadata.catalog-access-restrictions"
    });
    const catalogAccessRestText = intl.formatMessage({ id: "metadata.catalog-access-restriction" });
    const dataAccessTypesText = intl.formatMessage({ id: "metadata.data-access-types" });
    const dataAccessTypeText = intl.formatMessage({ id: "metadata.data-access-type" });
    const dataLicensesText = intl.formatMessage({ id: "metadata.data-licenses" });
    const dataLicenseText = intl.formatMessage({ id: "metadata.data-license" });
    const dataUploadRestsText = intl.formatMessage({ id: "metadata.data-upload-restrictions" });
    const dataUploadRestText = intl.formatMessage({ id: "metadata.data-upload-restriction" });
    const dataAccessRestsText = intl.formatMessage({ id: "metadata.data-access-restrictions" });
    const dataAccessRestText = intl.formatMessage({ id: "metadata.data-access-restriction" });
    const dataUploadTypesText = intl.formatMessage({ id: "metadata.data-upload-types" });
    const dataUploadTypeText = intl.formatMessage({ id: "metadata.data-upload-type" });
    const supportsMetadataStandardsText = intl.formatMessage({
        id: "metadata.supports-metadata-standards"
    });
    const supportsMetadataStandardText = intl.formatMessage({
        id: "metadata.supports-metadata-standard"
    });
    const typeText = intl.formatMessage({ id: "metadata.type" });
    const assignsIdentifierSchemesText = intl.formatMessage({
        id: "metadata.assigns-identifier-schemes"
    });
    const assignsIdentifierSchemeText = intl.formatMessage({
        id: "metadata.assigns-identifier-scheme"
    });

    const visitRepoText = intl.formatMessage({ id: "metadata.visit-repo" });
    const openUserPolicyText = intl.formatMessage({ id: "metadata.open-user-policy" });
    const visitMetadataSourceText = intl.formatMessage({ id: "metadata.visit-source" });
    const copyUrlText = intl.formatMessage({ id: "metadata.copy-url" });
    const actionButtonsHeader = intl.formatMessage({ id: "actionButton.header" });
    const hasCertificateText = intl.formatMessage({ id: "metadata.hasCertificate" });
    const hasCertificatesText = intl.formatMessage({ id: "metadata.hasCertificates" });
    const contentTypeText = intl.formatMessage({ id: "metadata.contentType" });
    const contentTypesText = intl.formatMessage({ id: "metadata.contentTypes" });
    const supportsVersioningText = intl.formatMessage({ id: "metadata.supportsVersioning" });
    const repositoryTypeText = intl.formatMessage({ id: "metadata.repositoryType" });
    const repositoryTypesText = intl.formatMessage({ id: "metadata.repositoryTypes" });
    const subjectAreaText = intl.formatMessage({ id: "metadata.subjectArea" });
    const subjectAreasText = intl.formatMessage({ id: "metadata.subjectAreas" });

    return (
        <>
            {/* Desktop view */}
            <Flex gap="10%" hideBelow="custombreak">
                <Box w="65%">
                    {metadata.title || metadata.altLabel ? (
                        <Box className="title" pt="15px">
                            {metadata.title
                                ? Array.isArray(metadata.title)
                                    ? metadata.title[0]
                                    : metadata.title
                                : Array.isArray(metadata.altLabel)
                                ? metadata.altLabel[0]
                                : metadata.altLabel}
                        </Box>
                    ) : (
                        ""
                    )}
                    <Box pt="36px">{getMetadata()}</Box>
                    {metadata.description ? (
                        <Box pt="40px">
                            <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                        </Box>
                    ) : null}
                </Box>
                <Box w="25%">
                    {getActions()}
                    {metadata.dateModified ? (
                        <Box pt="33">
                            <LastUpdate date={metadata.dateModified} />
                        </Box>
                    ) : null}
                    {metadata.hasAPI_conformsTo && metadata.hasAPI_url ? (
                        <Box pt="33px">
                            <Api apis={metadata.hasAPI_conformsTo} urls={metadata.hasAPI_url} />
                        </Box>
                    ) : null}
                </Box>
            </Flex>
            {/* Mobile view */}
            <Box hideFrom="custombreak">
                {metadata.title || metadata.altLabel ? (
                    <Box className="title" pt="15px">
                        {metadata.title
                            ? Array.isArray(metadata.title)
                                ? metadata.title[0]
                                : metadata.title
                            : Array.isArray(metadata.altLabel)
                            ? metadata.altLabel[0]
                            : metadata.altLabel}
                    </Box>
                ) : (
                    ""
                )}
                <Box pt="20px">{getMetadata()}</Box>
                {metadata.dateModified && (
                    <Box pt="18px">
                        <LastUpdate date={metadata.dateModified} />
                    </Box>
                )}
                {metadata.description ? (
                    <Box pt="40px">
                        <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                    </Box>
                ) : null}
                {metadata.hasAPI_conformsTo && metadata.hasAPI_url ? (
                    <Box pt="33px">
                        <Api apis={metadata.hasAPI_conformsTo} urls={metadata.hasAPI_url} />
                    </Box>
                ) : null}
                <Box>{getActions()}</Box>
            </Box>
        </>
    );

    function getActions() {
        return metadata.homepage || metadata.dataLicense ? (
            <Box className="actionButtonGroup" pt="50px">
                {metadata.homepage ? (
                    <>
                        <Box className="actionButtonsHeader" id="actionButtonsHeader">
                            {actionButtonsHeader}
                        </Box>
                        <Link
                            to={metadata.homepage[0] as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={visitRepoText}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                    </>
                ) : null}
                {metadata.dataLicense ? (
                    <Link
                        to={metadata.dataLicense[0] as string}
                        className="actionButtonLink"
                        target="_blank"
                    >
                        <ActionButton
                            label={openUserPolicyText}
                            icon={<InfoIcon />}
                            variant="outline"
                            fun={() => void 0}
                        />
                    </Link>
                ) : null}
                {metadata.id.split("-")[0] == "r3d" ? (
                    <Link
                        to={
                            ("https://www.re3data.org/repository/" +
                                metadata.sourceSystem_id) as string
                        }
                        className="actionButtonLink"
                        target="_blank"
                    >
                        <ActionButton
                            label={visitMetadataSourceText}
                            icon={<MetadataSourceIcon color="white" />}
                            variant="outline"
                            fun={() => void 0}
                        />
                    </Link>
                ) : null}
                {metadata.homepage ? (
                    <CopyToClipboardButton data={metadata.homepage} label={copyUrlText} />
                ) : null}
            </Box>
        ) : null;
    }

    function getMetadata() {
        return (
            <Metadata
                metadataElements={[
                    {
                        element: "theme",
                        tag: metadata.theme?.length > 1 ? themesText : themeText,
                        val: metadata.theme
                    },
                    {
                        element: "keyword",
                        tag: metadata.keyword?.length > 1 ? keywordsText : keywordText,
                        val: metadata.keyword
                    },
                    /*{
                        element: "label",
                        tag: "Label",
                        val: "NFDI4Earth Label"
                    },*/
                    {
                        element: "contact",
                        tag: contact,
                        val: { mail: metadata.contactPoint_email, urls: metadata.contactPoint_url }
                    },
                    /*{
                        element: "contactUrl",
                        tag:
                            metadata.contactPoint_url?.length > 1
                                ? contactUrlsText
                                : contactUrlText,
                        val: metadata.contactPoint_url
                    },*/
                    {
                        element: "publisher",
                        tag: metadata.publisher?.length ? publishersText : publisherText,
                        val: {
                            publishers: metadata.publisher,
                            publishers_homepage: metadata.publisher_homepage
                        }
                    },
                    {
                        element: "publisherAlt",
                        tag:
                            metadata.publisher_alt?.length > 1
                                ? publishersAltText
                                : publisherAltText,
                        val: metadata.publisher_alt
                    },
                    {
                        element: "alternativeLabels",
                        tag: metadata.altLabel?.length > 1 ? altLabelsText : altLabelText,
                        val: metadata.altLabel
                    },
                    {
                        element: "subjectArea",
                        tag: metadata.subjectArea?.length > 1 ? subjectAreasText : subjectAreaText,
                        val: metadata.subjectArea
                    },
                    {
                        element: "repositoryType",
                        tag:
                            metadata.repositoryType?.length > 1
                                ? repositoryTypesText
                                : repositoryTypeText,
                        val: metadata.repositoryType
                    },
                    {
                        element: "catalogAccessType",
                        tag:
                            metadata.catalogAccessType?.length > 1
                                ? catalogAccessTypesText
                                : catalogAccessTypeText,
                        val: metadata.catalogAccessType
                    },
                    {
                        element: "catalogAccessRestriction",
                        tag:
                            metadata.catalogAccessRestriction?.length > 1
                                ? catalogAccessRestsText
                                : catalogAccessRestText,
                        val: metadata.catalogAccessRestriction
                    },
                    {
                        element: "catalogLicense",
                        tag:
                            metadata.catalogLicense?.length > 1
                                ? catalogLicensesText
                                : catalogLicenseText,
                        val: metadata.catalogLicense
                    },
                    {
                        element: "dataAccessType",
                        tag:
                            metadata.dataAccessType?.length > 1
                                ? dataAccessTypesText
                                : dataAccessTypeText,
                        val: metadata.dataAccessType
                    },
                    {
                        element: "dataAccessRestriction",
                        tag:
                            metadata.dataAccessRestriction?.length > 1
                                ? dataAccessRestsText
                                : dataAccessRestText,
                        val: metadata.dataAccessRestriction
                    },
                    {
                        element: "dataUploadType",
                        tag:
                            metadata.dataUploadType?.length > 1
                                ? dataUploadTypesText
                                : dataUploadTypeText,
                        val: metadata.dataUploadType
                    },
                    {
                        element: "dataUploadRestriction",
                        tag:
                            metadata.dataUploadRestriction?.length > 1
                                ? dataUploadRestsText
                                : dataUploadRestText,
                        val: metadata.dataUploadRestriction
                    },
                    {
                        element: "dataLicense",
                        tag: metadata.dataLicense?.length > 1 ? dataLicensesText : dataLicenseText,
                        val: metadata.dataLicense
                    },
                    {
                        element: "supportsMetadataStandard",
                        tag:
                            metadata.supportsMetadataStandard?.length > 1
                                ? supportsMetadataStandardsText
                                : supportsMetadataStandardText,
                        val: {
                            publishers: metadata.supportsMetadataStandard,
                            publishers_homepage: metadata.supportsMetadataStandard_homepage
                        }
                    },
                    {
                        element: "hasCertificate",
                        tag:
                            metadata.hasCertificate?.length > 1
                                ? hasCertificatesText
                                : hasCertificateText,
                        val: metadata.hasCertificate
                    },
                    {
                        element: "contentType",
                        tag: metadata.contentType?.length > 1 ? contentTypesText : contentTypeText,
                        val: metadata.contentType
                    },
                    {
                        element: "type",
                        tag: typeText,
                        val: metadata.type
                    },
                    {
                        element: "supportsVersioning",
                        tag: supportsVersioningText,
                        val: metadata.supportsVersioning
                    },
                    {
                        element: "assignsIdentifierScheme",
                        tag:
                            metadata.assignsIdentifierScheme?.length > 1
                                ? assignsIdentifierSchemesText
                                : assignsIdentifierSchemeText,
                        val: metadata.assignsIdentifierScheme
                    }
                ]}
                visibleElements={7}
                expandedByDefault={true}
            />
        );
    }
}
