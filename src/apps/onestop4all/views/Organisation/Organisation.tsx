/* eslint-disable */
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { Link } from "react-router-dom";

import { ActionButton } from "../../components/ResourceType/ActionButton/ActionButton";
import { Identifier } from "../../components/ResourceType/Api_Identifier/Identifier";
import { Location } from "../../components/ResourceType/Location/Location";
import { Metadata } from "../../components/ResourceType/Metadata/Metadata";
import { SolrSearchResultItem } from "../../services/SearchService";
import { MetadataSourceIcon } from "../../components/Icons";
import { CopyToClipboardButton } from "../../components/ResourceType/ActionButton/CopyToClipboardButton";

import { useIntl } from "open-pioneer:react-hooks";

export interface OrganisationMetadataResponse extends SolrSearchResultItem {
    name: string;
    name_alt: string;
    homepage: string;
    uri: string;
    rorId: string;
    geometry: string;
    sameAs: string;
    countryName: string;
    locality: string;
    subOrganizationOf: string;
    identifier: Array<object>;
    relatedContent: Array<object>;
    otherIdentifiers: object;
    nfdi4EarthContactPerson_name: string;
    nfdi4EarthContactPerson_orcidId: string;
    //nfdi4EarthContactPerson_uri: string; probably not needed
    nfdi4EarthContactPerson_email: string;
    //nfdi4EarthContactPerson_id: string; probably not needed
    nfdi4EarthContactPerson_affiliation: string;
    sourceSystem_homepage: string;
    sourceSystem_title: string;
    sourceSystem_id: string;
}

export interface OrganisationViewProps {
    item: OrganisationMetadataResponse;
}

export function OrganisationView(props: OrganisationViewProps) {
    const metadata = props.item;

    const name = metadata.nfdi4EarthContactPerson_name;
    const email = metadata.nfdi4EarthContactPerson_email;
    const orcid = metadata.nfdi4EarthContactPerson_orcidId;
    const nfdiContact = {
        name: name ? name : undefined,
        orcid: orcid ? orcid : undefined,
        email: email ? email : undefined
    };
    // intl
    const intl = useIntl();
    // intl variables
    const urlsText = intl.formatMessage({ id: "metadata.urls" });
    const urlText = intl.formatMessage({ id: "metadata.url" });
    const altNamesText = intl.formatMessage({ id: "metadata.alt-names" });
    const altNameText = intl.formatMessage({ id: "metadata.alt-name" });
    const typeText = intl.formatMessage({ id: "metadata.type" });
    const sameAsText = intl.formatMessage({ id: "metadata.same-as" });
    const countryNamesText = intl.formatMessage({ id: "metadata.country-names" });
    const countryNameText = intl.formatMessage({ id: "metadata.country-name" });
    const localitiesText = intl.formatMessage({ id: "metadata.localities" });
    const localityText = intl.formatMessage({ id: "metadata.locality" });
    const subOrgaOfText = intl.formatMessage({ id: "metadata.sub-orga-of" });
    const NFDIContactText = intl.formatMessage({ id: "metadata.nfdi-contact" });
    const visitSourceText = intl.formatMessage({ id: "metadata.visit-source" });
    const visitWebsiteText = intl.formatMessage({ id: "metadata.visit-website" });
    const copyUrlText = intl.formatMessage({ id: "metadata.copy-url" });
    const locationText = intl.formatMessage({ id: "metadata.location" });
    const actionButtonsHeader = intl.formatMessage({ id: "actionButton.header" });

    return (
        <>
            {/* Desktop view */}
            <Flex gap="10%" hideBelow="custombreak">
                <Box w="65%">
                    {metadata.name || metadata.name_alt ? (
                        <Box className="title" pt="15px">
                            {metadata.name
                                ? Array.isArray(metadata.name)
                                    ? metadata.name[0]
                                    : metadata.name
                                : Array.isArray(metadata.name_alt)
                                ? metadata.name_alt[0]
                                : metadata.name_alt}
                        </Box>
                    ) : (
                        ""
                    )}
                    <Box pt="36px">{getMetadata()}</Box>
                    {metadata.otherIdentifiers ? (
                        <Box pt="33px">
                            <Identifier identifiers={metadata.otherIdentifiers} />
                        </Box>
                    ) : null}
                    {metadata.geometry ? (
                    <Box pt="40px">
                        <Location
                            address={{
                                tag: locationText,
                                val: metadata.geometry
                            }}
                            location={metadata.geometry}
                            mapId="desktop"
                        />
                    </Box>
                ) : null}
                </Box>
                <Box w="25%">
                    {getActions()}
                </Box>
            </Flex>
            {/* Mobile view */}
            <Box hideFrom="custombreak">
                {metadata.name || metadata.name_alt ? (
                    <Box className="title" pt="15px">
                        {metadata.name
                            ? Array.isArray(metadata.name)
                                ? metadata.name[0]
                                : metadata.name
                            : Array.isArray(metadata.name_alt)
                            ? metadata.name_alt[0]
                            : metadata.name_alt}
                    </Box>
                ) : (
                    ""
                )}
                <Box pt="20px">{getMetadata()}</Box>
                {metadata.geometry ? (
                    <Box pt="40px">
                        <Location
                            address={{
                                tag: locationText,
                                val: metadata.geometry
                            }}
                            location={metadata.geometry}
                            mapId="mobile"
                        />
                    </Box>
                ) : null}
                <Box>{getActions()}</Box>
            </Box>
        </>
    );

    function getMetadata() {
        return (
            <Metadata
                metadataElements={[
                    {
                        element: "url",
                        tag: metadata.homepage?.length > 1 ? urlsText : urlText,
                        val: metadata.homepage
                    },
                    {
                        element: "alternativeName",
                        tag: metadata.name_alt?.length > 1 ? altNamesText : altNameText,
                        val: metadata.name_alt
                    },
                    {
                        element: "type",
                        tag: typeText,
                        val: metadata.type
                    },
                    {
                        element: "sameAs",
                        tag: sameAsText,
                        val: metadata.sameAs
                    },
                    {
                        element: "location",
                        tag: "Location",
                        val: metadata.locality + ", " + metadata.countryName
                    },
                    {
                        element: "subOrganizationOf",
                        tag: subOrgaOfText,
                        val: metadata.subOrganizationOf
                    },
                    {
                        element: "nfdi",
                        tag: NFDIContactText,
                        val:
                            nfdiContact.name ||
                            nfdiContact.email ||
                            nfdiContact.orcid
                    }
                ]}
                visibleElements={2}
                expandedByDefault={true}
            />
        );
    }

    function getActions() {
        return metadata.homepage || metadata.sourceSystem_homepage ? (
            <Box className="actionButtonGroup" pt="50px">
                {metadata.homepage ? (
                    <>
                        <Box className="actionButtonsHeader" id="actionButtonsHeader">
                            {actionButtonsHeader}
                        </Box>
                        <Link
                            to={metadata.homepage[0] as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={visitWebsiteText}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                        <CopyToClipboardButton
                            data={metadata.homepage[0] ? metadata.homepage[0] : metadata.homepage}
                            label={copyUrlText}
                        />
                    </>
                ) : null}
                {metadata.rorId ? (
                    <Link
                        to={
                            ("https://ror.org/" + metadata.rorId) as string
                        }
                        className="actionButtonLink"
                        target="_blank"
                    >
                        <ActionButton
                            label={visitSourceText}
                            icon={<MetadataSourceIcon color="white" />}
                            variant="outline"
                            fun={() => void 0}
                        />
                    </Link>
                ) : null}
            </Box>
        ) : null;
    }
}
