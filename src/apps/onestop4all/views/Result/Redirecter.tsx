import { useNavigate, useParams } from "react-router-dom";
import { useService } from "open-pioneer:react-hooks";
import { HowToResponse } from "../Start/HowTo/HowToEntryContent";
import { SearchService } from "../../services";

export const Redirecter = () => {
    const markdownFile = useParams().markdown;
    const navigate = useNavigate();
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;

    searchSrvc.getChapter(markdownFile + ".md").then((result) => {
        const res = result.response as HowToResponse;
        const id = res && res.docs && res.docs[0] ? res.docs[0].id : "";
        navigate({ pathname: "/result/" + id });
    });

    return <></>;
};
