import { Box, Container, Divider, Flex } from "@open-pioneer/chakra-integration";
import { useIntl, useService } from "open-pioneer:react-hooks";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { BackToSearchLink } from "../../components/BackToSearchLink/BackToSearchLink";
import { RelatedContent } from "../../components/ResourceType/RelatedContent/RelatedContent";
import { ResourceTypeLabel } from "../../components/ResourceTypeLabel/ResourceTypeLabel";
import { ResultsNavigation } from "../../components/ResultsNavigation/ResultsNavigation";
import ScrollToTop from "../../components/ScrollToTop";
import { SearchBar } from "../../components/SearchBar";
import Skeleton from "../../components/Skeleton/Skeleton";
import { getResourceType, ResourceType } from "../../services/ResourceTypeUtils";
import { SearchService, SolrSearchResultItem } from "../../services/SearchService";
import { PublicationMetadataResponse, PublicationView } from "../Publication/Publication";
import {
    LearningResourceMetadataResponse,
    LearningResourceView
} from "../LearningResource/LearningResource";
import { LHB_ArticleMetadataResponse, LHB_ArticleView } from "../LHB_Article/LHB_Article";
import { OrganisationMetadataResponse, OrganisationView } from "../Organisation/Organisation";
import { RepositoryMetadataResponse, RepositoryView } from "../Repository/Repository";
import { useSearchState } from "../Search/SearchState";
import { StandardMetadataResponse, StandardView } from "../Standard/Standard";
import { ToolsSoftwareMetadataResponse, ToolsSoftwareView } from "../ToolsSoftware/ToolsSoftware";
import { DatasetMetadataResponse, DatasetView } from "../Dataset/Dataset";
import { DataServiceView } from "../DataService/DataService";
import ScrollToTopBtn from "react-scroll-to-top";
import { UpIcon } from "../../components/Icons";
import { ServiceView } from "../Service/Service";

export function Result() {
    const resultId = useParams().id as string;
    const navigate = useNavigate();

    /*if (resultId === "lhb-docs-FAQ.md") {
        navigate(`/faq/`);
    }*/

    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const [searchResult, setSearchResult] = useState<SolrSearchResultItem>();
    const [resourceType, setResourceType] = useState<ResourceType>();
    const [loading, setLoading] = useState(true);

    const [result, setResult] = useState<number>();
    const [resultCount, setResultCount] = useState<number>();
    const [relatedResources, setRelatedResources] = useState<object>([]);
    const searchState = useSearchState();

    const intl = useIntl();

    useEffect(() => {
        if (history.state && history.state.usr && history.state.usr.resultPage) {
            // console.log(`Current page is ${history.state.usr.resultPage}`);
            setResult(history.state.usr.resultPage);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [history.state.usr]);

    useEffect(() => {
        setLoading(true);
        searchSrvc.getMetadata(resultId).then((result) => {
            if (result.results[0]) {
                console.log(result.results[0]);
                if (result.results[0].relatedContent?.length > 0) {
                    const relatedResources = [] as object[];
                    result.results[0].relatedContent.forEach((relatedResourceId: string) => {
                        searchSrvc.getMetadata(relatedResourceId).then((res) => {
                            const relResTmp = relatedResources;
                            relResTmp.push(res.results);
                            setRelatedResources(relResTmp);
                            if (
                                relatedResources?.length ===
                                result?.results[0]?.relatedContent.length
                            ) {
                                result.results[0].relatedResources = relatedResources;
                                setSearchResult(result.results[0]);
                                setResourceType(getResourceType(result.results[0]));
                                setLoading(false);
                            }
                        });
                    });
                } else {
                    setSearchResult(result.results[0]);
                    setResourceType(getResourceType(result.results[0]));
                    setLoading(false);
                }
            } else {
                navigate(`*`);
            }
        });
    }, [resultId, searchSrvc, navigate]);

    useEffect(() => {
        if (searchState.searchResults) {
            setResultCount(searchState.searchResults.count);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function getResourceView(): import("react").ReactNode {
        switch (resourceType) {
            case ResourceType.Repos: {
                const item = searchResult as RepositoryMetadataResponse;
                return <RepositoryView item={item} />;
            }
            // case ResourceType.Datasets:
            //     // TODO: needs to be implemented
            //     return <Box>Dataset</Box>;
            case ResourceType.Organisations: {
                const item = searchResult as OrganisationMetadataResponse;
                return <OrganisationView item={item} />;
            }
            case ResourceType.LHB_Articles: {
                const item = searchResult as LHB_ArticleMetadataResponse;
                return <LHB_ArticleView item={item} />;
            }
            case ResourceType.Tools: {
                const item = searchResult as ToolsSoftwareMetadataResponse;
                return <ToolsSoftwareView item={item} />;
            }
            case ResourceType.Standards: {
                const item = searchResult as StandardMetadataResponse;
                return <StandardView item={item} />;
            }
            case ResourceType.Learning_Resource: {
                const item = searchResult as LearningResourceMetadataResponse;
                return <LearningResourceView item={item} />;
            }
            case ResourceType.Publications: {
                const item = searchResult as PublicationMetadataResponse;
                return <PublicationView item={item} />;
            }
            case ResourceType.Datasets: {
                const item = searchResult as DatasetMetadataResponse;
                return <DatasetView item={item} />;
            }
            case ResourceType.DataService: {
                const item = searchResult as DatasetMetadataResponse;
                return <DataServiceView item={item} />;
            }
            case ResourceType.Service: {
                const item = searchResult as DatasetMetadataResponse;
                return <ServiceView item={item} />;
            }
            default:
                throw new Error(`Unknown resourceType: '${resourceType}'`);
        }
    }

    function stepBack(): void {
        if (result) {
            fetchResultId(result - 1);
        }
    }

    function stepForward(): void {
        if (result) {
            fetchResultId(result + 1);
        }
    }

    function stepToEnd(): void {
        if (resultCount) {
            fetchResultId(resultCount);
        }
    }

    function stepToStart(): void {
        if (result) {
            fetchResultId(1);
        }
    }

    function fetchResultId(result: number) {
        searchSrvc
            .doSearch({
                pageSize: 1,
                pageStart: result - 1,
                resourceTypes: searchState.selectedResourceTypes,
                subjects: searchState.selectedSubjects,
                searchTerm: searchState.searchTerm,
                spatialFilter: searchState.spatialFilter,
                countryNames: searchState.selectedCountries,
                publisher: searchState.selectedPublisher,
                hasAPI_conformsTo: searchState.selectedApi,
                dataLicense:searchState.selectedDataLicense
            })
            .then((res) => {
                if (res.results[0]) {
                    navigate(`/result/${res.results[0].id}`, { state: { resultPage: result } });
                    setResult(result);
                }
            });
    }

    function renderPaging(): import("react").ReactNode {
        if (result !== undefined && resultCount !== undefined) {
            return (
                <ResultsNavigation
                    result={result}
                    of={resultCount}
                    label={intl.formatMessage({ id: "search.result" })}
                    stepBack={stepBack}
                    stepFoward={stepForward}
                    stepToEnd={stepToEnd}
                    stepToStart={stepToStart}
                />
            );
        } else {
            return <></>;
        }
    }

    return (
        <Box className="search-view">
            <ScrollToTop />
            <Box position="relative">
                <Box className="header-image" />
            </Box>

            <Box
                position="absolute"
                width="100%"
                marginTop={{ base: "-40px", custombreak: "-50px" }}
            >
                <Container maxW={{ base: "100%", custombreak: "80%" }}>
                    <SearchBar />
                </Container>
            </Box>

            <Box height={{ base: "60px", custombreak: "80px" }}></Box>

            <Container maxW={{ base: "100%", custombreak: "80%" }}>
                {/* Desktop Header */}
                <Flex gap="10%" hideBelow="custombreak">
                    <Box w="65%">
                        <Flex alignItems="center" gap="12px">
                            <BackToSearchLink
                                visible={result !== undefined && resultCount !== undefined}
                                prevPath={history.state.usr?.prevPath}
                            />
                            <Divider className="resTypeHeaderLine" />
                            <ResourceTypeLabel
                                resType={resourceType}
                                loading={loading}
                                iconAlign="right"
                            />
                        </Flex>
                        {loading ? <Skeleton /> : <></>}
                    </Box>
                    <Box w="25%">{renderPaging()}</Box>
                </Flex>

                {/* Mobile Header */}
                <Box hideFrom="custombreak">
                    <Box pt="50px">{renderPaging()}</Box>
                    <Flex alignItems="center" gap="12px" pt={"20px"}>
                        <ResourceTypeLabel
                            resType={resourceType}
                            loading={loading}
                            iconAlign="left"
                        />
                        <Divider />
                    </Flex>
                </Box>

                <Box hideBelow="custombreak">
                    <ScrollToTopBtn smooth top={500} component={                                 
                        <Flex
                            alignItems="center"
                            direction="column"
                            gap="4px"
                            position={"fixed"} right={"-35%"} top={"30%"}
                            style={{ marginLeft: "40%", marginRight: "40%" }}
                        >
                            <>
                                <Box>
                                    <UpIcon />
                                </Box>
                                <Box className="metadataShowHide">
                                    Up
                                </Box>
                            </>
                        </Flex>
                    } />
                </Box>

                {/* Content */}
                {!loading && (
                    <>
                        <Box>{getResourceView()}</Box>
                        {searchResult?.relatedResources?.length > 0 ? (
                            <Box pt={{ base: "40px", custombreak: "80px" }}>
                                <RelatedContent
                                    relatedContentItems={searchResult?.relatedResources}
                                />
                            </Box>
                        ) : null}
                    </>
                )}

                {/* Desktop footer */}
                <Flex gap="10%" alignItems="center" pt="120px" hideBelow="custombreak">
                    <Divider className="seperator" w="65%" />
                    <Box w="25%">{renderPaging()}</Box>
                </Flex>

                {/* Mobile footer */}
                <Box hideFrom="custombreak">
                    <Box pt={"10"}>{renderPaging()}</Box>
                    <Flex alignItems="center" gap="12px" pt="25px">
                        <Divider />
                        <BackToSearchLink
                            visible={result !== undefined && resultCount !== undefined}
                            prevPath={history.state.usr?.prevPath}
                        />
                        <Divider />
                    </Flex>
                </Box>
            </Container>
        </Box>
    );
}
