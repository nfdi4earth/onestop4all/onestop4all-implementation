/* eslint-disable */
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { Link } from "react-router-dom";

import { Abstract } from "../../components/ResourceType/Abstract/Abstract";
import { ActionButton } from "../../components/ResourceType/ActionButton/ActionButton";
import { Metadata } from "../../components/ResourceType/Metadata/Metadata";
import { Location } from "../../components/ResourceType/Location/Location";
import { SolrSearchResultItem } from "../../services/SearchService";

import { useIntl } from "open-pioneer:react-hooks";
import { CopyToClipboardButton } from "../../components/ResourceType/ActionButton/CopyToClipboardButton";
import { MetadataSourceIcon } from "../../components/Icons";

export interface DatasetMetadataResponse extends SolrSearchResultItem {
    title: string;
    description: string;
    keyword: string;
}

export interface DatasetViewProps {
    item: DatasetMetadataResponse;
}

export function DataServiceView(props: DatasetViewProps) {
    const metadata = props.item;

    const intl = useIntl();
    const keywordsText = intl.formatMessage({ id: "metadata.keywords" });
    const keywordText = intl.formatMessage({ id: "metadata.keyword" });
    const typeText = intl.formatMessage({ id: "metadata.type" });
    const distributionText = intl.formatMessage({ id: "metadata.view-distribution" });
    const actionButtonsHeader = intl.formatMessage({ id: "actionButton.header" });
    const locationText = intl.formatMessage({ id: "metadata.location" });
    const publishedText = intl.formatMessage({ id: "metadata.published" });
    const modifiedText = intl.formatMessage({ id: "metadata.modified" });
    const visitDataServiceText = intl.formatMessage({ id: "metadata.visit-dataservice" });
    const endpointURLText = intl.formatMessage({ id: "metadata.endpoint-url" });
    const endpointDescriptionText = intl.formatMessage({ id: "metadata.endpoint-description" });
    const conformsToText = intl.formatMessage({ id: "metadata.conforms-to" });
    const landingPageText = intl.formatMessage({ id: "metadata.landing-page" });
    const versionText = intl.formatMessage({ id: "metadata.version" });
    const contactUrlText = intl.formatMessage({ id: "metadata.contact-url" });
    const contactMailText = intl.formatMessage({ id: "metadata.contact-mail" });
    const copyUrlText = intl.formatMessage({ id: "metadata.copy-url" });
    const visitSourceText = intl.formatMessage({ id: "metadata.visit-source" });

    return (
        <>
            {/* Desktop view */}
            <Flex gap="10%" hideBelow="custombreak">
                <Box w="65%">
                    {metadata.title || metadata.altLabel ? (
                        <Box className="title" pt="15px">
                            {metadata.title
                                ? Array.isArray(metadata.title)
                                    ? metadata.title[0]
                                    : metadata.title
                                : Array.isArray(metadata.altLabel)
                                ? metadata.altLabel[0]
                                : metadata.altLabel}
                        </Box>
                    ) : (
                        ""
                    )}
                    <Box pt="36px">{getMetadata()}</Box>
                    {metadata.description ? (
                        <Box pt="40px">
                            <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                        </Box>
                    ) : null}
                    {metadata.geometry_wkt ? (
                    <Box pt="40px">
                        <Location
                            address={{
                                tag: locationText,
                                val: metadata.geometry_wkt
                            }}
                            location={metadata.geometry_wkt}
                            mapId="desktop"
                        />
                    </Box>
                ) : null}
                </Box>
                <Box w="25%">
                    {getActions()}
                </Box>
            </Flex>
            {/* Mobile view */}
            <Box hideFrom="custombreak">
                {metadata.title || metadata.altLabel ? (
                    <Box className="title" pt="15px">
                        {metadata.title
                            ? Array.isArray(metadata.title)
                                ? metadata.title[0]
                                : metadata.title
                            : Array.isArray(metadata.altLabel)
                            ? metadata.altLabel[0]
                            : metadata.altLabel}
                    </Box>
                ) : (
                    ""
                )}
                {metadata.geometry_wkt ? (
                    <Box pt="80px">
                        <Location
                            address={{
                                tag: locationText,
                                val: metadata.geometry_wkt
                            }}
                            location={metadata.geometry_wkt}
                            mapId="mobile"
                        />
                    </Box>
                ) : null}
                <Box pt="20px">{getMetadata()}</Box>
                <Box>{getActions()}</Box>
            </Box>
        </>
    );

    function getActions() {
        return metadata.landingPage ? (
            <Box className="actionButtonGroup" pt="50px">
                {metadata.landingPage ? (
                    <>
                        <Box className="actionButtonsHeader" id="actionButtonsHeader">
                            {actionButtonsHeader}
                        </Box>
                        <Link
                            to={metadata.landingPage[0] as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={visitDataServiceText}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                        <CopyToClipboardButton
                            data={metadata.landingPage[0] ? metadata.landingPage[0] : metadata.landingPage}
                            label={copyUrlText}
                        />
                    </>
                ) : null}
                {metadata.endpointDescription ? (
                    <Link
                        to={metadata.endpointDescription[0] as string}
                        className="actionButtonLink"
                        target="_blank"
                    >
                        <ActionButton
                            label={visitSourceText}
                            icon={<MetadataSourceIcon color="white" />}
                            variant="outline"
                            fun={() => void 0}
                        />
                    </Link>
                ) : null}
            </Box>
        ) : null;
    }

    function getMetadata() {
        return (
            <Metadata
                metadataElements={[
                    {
                        element: "keyword",
                        tag: metadata.keyword?.length > 1 ? keywordsText : keywordText,
                        val: metadata.keyword
                    },
                    {
                        element: "conformsTo",
                        tag: conformsToText,
                        val: metadata.conformsTo
                    },
                    {
                        element: "type",
                        tag: typeText,
                        val: metadata.type
                    },
                    {
                        element: "version",
                        tag: versionText,
                        val: metadata.version
                    },
                    {
                        element: "datePublished",
                        tag: publishedText,
                        val: metadata.datePublished
                            ? new Date(metadata.datePublished).toLocaleDateString()
                            : undefined
                    },
                    {
                        element: "dateModified",
                        tag: modifiedText,
                        val: metadata.dateModified
                            ? new Date(metadata.dateModified).toLocaleDateString()
                            : undefined
                    },
                    {
                        element: "contactPoint",
                        tag: contactUrlText,
                        val: metadata.contactPoint_url
                    },
                    {
                        element: "contactMail",
                        tag: contactMailText,
                        val: metadata.contactPoint_email
                    }
                ]}
                visibleElements={2}
                expandedByDefault={true}
            />
        );
    }
}
