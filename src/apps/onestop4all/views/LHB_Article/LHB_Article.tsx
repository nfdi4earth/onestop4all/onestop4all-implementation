/* eslint-disable */
import { Box, Flex } from "@open-pioneer/chakra-integration";
import parse from "html-react-parser";
import { useIntl, useService } from "open-pioneer:react-hooks";
import { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import rehypeCitation from "rehype-citation";
import rehypeStringify from "rehype-stringify";
import remarkGfm from "remark-gfm";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import rehypeRaw from "rehype-raw";
import { unified } from "unified";

import { MetadataSourceIcon } from "../../components/Icons";
import { ActionButton } from "../../components/ResourceType/ActionButton/ActionButton";
import { LastUpdate } from "../../components/ResourceType/Metadata/LastUpdate";
import { Metadata } from "../../components/ResourceType/Metadata/Metadata";
import { Support } from "../../components/ResourceType/Support/Support";
import { TOC } from "../../components/ResourceType/TOC/TOC";
import { getLinkType, getTags, parseMarkdown, processVideo } from "../../services/MarkdownUtils";
import { SearchService, SolrSearchResultItem } from "../../services/SearchService";
import { HowToResponse } from "../Start/HowTo/HowToEntryContent";
import { FaqContent } from "../Start/Faq/FaqContent";

export interface LHB_ArticleMetadataResponse extends SolrSearchResultItem {
    name: string;
    description: string;
    author: string;
    articleBody: string;
    keyword: string;
    relatedContent: Array<object>;
    sourceSystem_id: string;
    targetGroup: string;
    hasPart: string;
    audience: string;
}

export interface ArticleViewProps {
    item: LHB_ArticleMetadataResponse;
}

export function LHB_ArticleView(props: ArticleViewProps) {
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const metadata = props.item;
    const markdown = metadata.articleBody[0];
    const [markdownContent, setMdCon] = useState("");

    const opts = searchSrvc.getRehypeCitationOptions();

    const [bibliography, setBibliography] = useState(opts.proxy + opts.bib);

    const [citationFileFormat, setCitationFileFormat] = useState(opts.cff);

    const rehypeCitationOptions = { bibliography, citationFileFormat, linkCitations: true };

    const elementRef = useRef<HTMLInputElement>(null);

    //NOTE: setIdsInHtml enriches the html parsed from markdown with IDs. Thought this is necessary for the TOC but it's not needed for now but might be useful in the future.
    /*const setIdsInHtml = (html: Document, tag: string) => {
        const htmlTags = html.getElementsByTagName(tag);
        if (html && htmlTags.length > 0) {
            for (let i = 0; i < htmlTags.length; i++) {
                const idString = html
                    .getElementsByTagName(tag)
                    [i]?.innerHTML.toLocaleLowerCase()
                    .replaceAll(" ", "_");
                if (htmlTags && Array.isArray(htmlTags) && htmlTags[i] && htmlTags[i].id) {
                    htmlTags[i].id = idString + "_" + i;
                }
            }
            return html;
        } else {
            return html;
        }
    };*/

    useEffect(() => {
        unified()
            .use(remarkParse)
            .use(remarkGfm)
            .use(remarkRehype, {allowDangerousHtml: true})
            .use(rehypeRaw)
            .use(rehypeCitation, rehypeCitationOptions)
            .use(rehypeStringify)
            .process(markdown)
            .then((file) => {
                let text = processVideo(file.value as string, opts.videoFolder);
                const html = parseMarkdown(text);
                const htmlTags = getTags(html);
                if (html && htmlTags && htmlTags.length > 0) {
                    for (let i = 0; i < htmlTags.length; i++) {
                        const tmp = htmlTags[i];
                        const tag = html.getElementsByTagName("a")[i];
                        const link = tag?.href;
                        const url = html.URL;
                        const linkType = getLinkType(link as string, url as string);
                        if (tmp && linkType) {
                            tmp.target = "_blank";
                            tmp.rel = "noopener";
                            if (linkType == "mail" || linkType == "url") {
                                setMdCon(html.body.innerHTML as string);
                            }
                            if (linkType === "bib_reference") {
                                tmp.href = "javascript:void(0)";
                                tmp.innerHTML = '<button class="bib-reference-button">' + tmp.innerHTML + '</button>';
                                tmp.target = "";
                                tmp.rel = "";
                                setMdCon(html.body.innerHTML as string);
                            }
                            if (linkType === "fig_reference") {
                                tmp.href = "javascript:void(0)";
                                tmp.innerHTML = '<button class="fig-reference-button" data-link="' + link + '">' + tmp.innerHTML + '</button>';                                tmp.target = "";
                                tmp.rel = "";
                                setMdCon(html.body.innerHTML as string);
                            }
                            if (linkType == "markdown") {
                                const markdownLink = html
                                    .getElementsByTagName("a")
                                    [i]?.href.split("/")
                                    .pop();
                                if (markdownLink) {
                                    searchSrvc.getChapter(markdownLink).then((result) => {
                                        const res = result.response as HowToResponse;
                                        const id =
                                            res && res.docs && res.docs[0] ? res.docs[0].id : "";
                                        tmp.href = "/result/" + id;
                                        setMdCon(html.body.innerHTML as string);
                                    });
                                }
                            }
                            if (linkType == "cordra") {
                                const id = tmp.href.split("/").pop();
                                tmp.href = "/result/" + id;
                                tmp.target = "_self";
                                setMdCon(html.body.innerHTML as string);
                            }
                            if (linkType == "pdf_docx") {
                                const id = tmp.href.split("/").pop();
                                tmp.href = opts.pdfFolder + id;
                                setMdCon(html.body.innerHTML as string);
                            }
                            if (linkType == "video") {
                                const id = tmp.href.split("/").pop();
                                tmp.href = opts.videoFolder + id;
                                setMdCon(html.body.innerHTML as string);
                            }
                        }
                    }
                }
                setMdCon(html.body.innerHTML as string);
            });
    }, [markdown]);

    useEffect(() => {
        if (elementRef.current) {
            elementRef.current.querySelectorAll(".bib-reference-button").forEach((button) => {
                button.addEventListener("click", () => {
                    const references = ["references", "referenzen", "bibliography", "bibliographie"];
                    const htmlCollection = elementRef.current?.children[0]?.children;
                    if (htmlCollection && htmlCollection.length > 0) {
                        for (let i = 0; i < htmlCollection.length; i++) {
                            const element = htmlCollection[i];
                            if (element && references.includes(element.innerHTML.toLowerCase())) {
                                element.scrollIntoView({ behavior: "smooth", block: "center" });
                            }
                        }
                    }
                });
            });

            elementRef.current.querySelectorAll(".fig-reference-button").forEach((button) => {
                button.addEventListener("click", () => {
                    const linkType = button.getAttribute("data-link");
                    const fig_ref = linkType?.split("#").pop();
                    const htmlCollection = elementRef.current?.children[0]?.children;
                    if (fig_ref && htmlCollection && htmlCollection.length > 0) {
                        for (let i = 0; i < htmlCollection.length; i++) {
                            const element = htmlCollection[i];
                            if (element && element.innerHTML.includes(fig_ref)) {
                                element.scrollIntoView({ behavior: "smooth", block: "center" });
                            }
                        }
                    }
                });
            });
        }
    }, [markdownContent]);
    

    // Internationalization
    const intl = useIntl();
    // intl variables
    const authorsText = intl.formatMessage({ id: "metadata.authors" });
    const authorText = intl.formatMessage({ id: "metadata.author" });
    const descText = intl.formatMessage({ id: "metadata.desc" });
    const keywordsText = intl.formatMessage({ id: "metadata.keywords" });
    const keywordText = intl.formatMessage({ id: "metadata.keyword" });
    const typeText = intl.formatMessage({ id: "metadata.type" });
    const targetGroupsText = intl.formatMessage({ id: "metadata.target-groups" });
    const targetGroupText = intl.formatMessage({ id: "metadata.target-group" });
    const audiencesText = intl.formatMessage({ id: "metadata.audiences" });
    const audienceText = intl.formatMessage({ id: "metadata.audience" });
    const hasPartsText = intl.formatMessage({ id: "metadata.has-parts" });
    const hasPartText = intl.formatMessage({ id: "metadata.has-part" });
    const actionButtonsHeader = intl.formatMessage({ id: "actionButton.header" });

    const visitSourceText = intl.formatMessage({ id: "metadata.visit-source" });

    return (
        <>
            {/* Mobile view */}
            <Box hideFrom="1400px">
                <Box>{getToc()}</Box>
                {metadata.name || metadata.name_alt ? (
                    <Box className="title" pt="15px">
                        {metadata.name
                            ? Array.isArray(metadata.name)
                                ? metadata.name[0]
                                : metadata.name
                            : Array.isArray(metadata.name_alt)
                            ? metadata.name_alt[0]
                            : metadata.name_alt}
                    </Box>
                ) : (
                    ""
                )}
                <Box pt="30px">{getMetadata()}</Box>
                {metadata.dateModified && (
                    <Box pt="18px">
                        <LastUpdate date={metadata.dateModified} />
                    </Box>
                )}
                <Box pt="30px" ref={elementRef} overflow="scroll">
                    {markdownContent != "" ? <div>{parse(markdownContent)}</div> : null}
                </Box>
                {/* TODO: add related content here */}
                <Box pt="20px">
                    <Support />
                </Box>
                <Box>{getActions()}</Box>
            </Box>
            {/* Desktop view */}
            <Flex gap="10%" hideBelow="1400px">
                <Box w="65%">
                    {metadata.name || metadata.name_alt ? (
                        <Box className="title" pt="15px">
                            {metadata.name
                                ? Array.isArray(metadata.name)
                                    ? metadata.name[0]
                                    : metadata.name
                                : Array.isArray(metadata.name_alt)
                                ? metadata.name_alt[0]
                                : metadata.name_alt}
                        </Box>
                    ) : (
                        ""
                    )}
                    <Box pt="36px">{getMetadata()}</Box>
                    <Box pt="40px" ref={elementRef}>
                        {
                            metadata.id === "lhb-docs-FAQ.md" ? <FaqContent /> : markdownContent != "" ? <div>{parse(markdownContent)}</div> : null} 
                    </Box>
                    <Box pt="40px">
                        <Support />
                    </Box>
                </Box>
                <Box w="25%">
                    {getActions()}
                    {metadata.dateModified && (
                        <Box pt="33">
                            <LastUpdate date={metadata.dateModified} />
                        </Box>
                    )}
                    {getToc()}
                </Box>
            </Flex>
        </>
    );

    function getToc() {
        return markdown && elementRef.current && elementRef.current.children ? (
            <TOC elementRef={elementRef} sourceId={metadata.sourceSystem_id} />
        ) : null;
    }

    function getActions() {
        return metadata.sourceSystem_id ? (
            <Box className="actionButtonGroup" pt="50px">
                <Box className="actionButtonsHeader" id="actionButtonsHeader">
                    {actionButtonsHeader}
                </Box>
                <Link
                    to={(opts.lhbUrl + metadata.sourceSystem_id) as string}
                    className="actionButtonLink"
                    target="_blank"
                >
                    <ActionButton
                        label={visitSourceText}
                        icon={<MetadataSourceIcon color="white" />}
                        variant="outline"
                        fun={() => void 0}
                    />
                </Link>
            </Box>
        ) : null;
    }

    function getMetadata() {
        return (
            <Metadata
                metadataElements={[
                    {
                        element: "author",
                        tag: metadata.author?.length > 1 ? authorsText : authorText,
                        val: metadata.author
                    },
                    {
                        element: "description",
                        tag: descText,
                        val: metadata.description
                    },
                    {
                        element: "keyword",
                        tag: metadata.keyword?.length > 1 ? keywordsText : keywordText,
                        val: metadata.keyword
                    },
                    {
                        element: "type",
                        tag: typeText,
                        val: metadata.type
                    },
                    {
                        element: "targetGroup",
                        tag: metadata.targetGroup?.length > 1 ? targetGroupsText : targetGroupText,
                        val: metadata.targetGroup
                    },
                    {
                        element: "audience",
                        tag: metadata.audience?.length > 1 ? audiencesText : audienceText,
                        val: metadata.audience
                    },
                    {
                        element: "hasPart",
                        tag: metadata.hasPart?.length > 1 ? hasPartsText : hasPartText,
                        val: metadata.hasPart
                    }
                ]}
                visibleElements={2}
                expandedByDefault={true}
            />
        );
    }
}
