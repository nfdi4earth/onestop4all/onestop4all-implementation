/* eslint-disable */
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { Link } from "react-router-dom";

import { Abstract } from "../../components/ResourceType/Abstract/Abstract";
import { ActionButton } from "../../components/ResourceType/ActionButton/ActionButton";
import { Metadata } from "../../components/ResourceType/Metadata/Metadata";
import { SolrSearchResultItem } from "../../services/SearchService";
import { SpecificationIcon, MetadataSourceIcon } from "../../components/Icons";
import { CopyToClipboardButton } from "../../components/ResourceType/ActionButton/CopyToClipboardButton";

import { useIntl } from "open-pioneer:react-hooks";

export interface StandardMetadataResponse extends SolrSearchResultItem {
    title: string;
    description: string;
    website: string;
    theme: Array<string>;
    parentStandard: string;
    relatedContent: Array<object>;
    uri: string;
    childStandard: string;
    document: string;
    id: string;
    sourceSystem_id: string;
    sourceSystem_title: string;
    xsd: string;
}

export interface StandardViewProps {
    item: StandardMetadataResponse;
}

export function StandardView(props: StandardViewProps) {
    const metadata = props.item;

    const intl = useIntl();
    //intl variables
    const themesText = intl.formatMessage({ id: "metadata.themes" });
    const themeText = intl.formatMessage({ id: "metadata.theme" });
    const typeText = intl.formatMessage({ id: "metadata.type" });
    const visitWebsiteText = intl.formatMessage({ id: "metadata.visit-website" });
    const specificationText = intl.formatMessage({ id: "metadata.view-specification" });
    const visitMetadataSourceText = intl.formatMessage({ id: "metadata.visit-source" });
    const copyUrlText = intl.formatMessage({ id: "metadata.copy-url" });
    const actionButtonsHeader = intl.formatMessage({ id: "actionButton.header" });

    return (
        <>
            {/* Desktop view */}
            <Flex gap="10%" hideBelow="custombreak">
                <Box w="65%">
                    {metadata.title || metadata.altLabel ? (
                        <Box className="title" pt="15px">
                            {metadata.title
                                ? Array.isArray(metadata.title)
                                    ? metadata.title[0]
                                    : metadata.title
                                : Array.isArray(metadata.altLabel)
                                ? metadata.altLabel[0]
                                : metadata.altLabel}
                        </Box>
                    ) : (
                        ""
                    )}
                    <Box pt="36px">{getMetadata()}</Box>
                    {metadata.description ? (
                        <Box pt="80px">
                            <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                        </Box>
                    ) : null}
                </Box>
                <Box w="25%">{getActions()}</Box>
            </Flex>
            {/* Mobile view */}
            <Box hideFrom="custombreak">
                {metadata.title || metadata.altLabel ? (
                    <Box className="title" pt="15px">
                        {metadata.title
                            ? Array.isArray(metadata.title)
                                ? metadata.title[0]
                                : metadata.title
                            : Array.isArray(metadata.altLabel)
                            ? metadata.altLabel[0]
                            : metadata.altLabel}
                    </Box>
                ) : (
                    ""
                )}
                <Box pt="20px">{getMetadata()}</Box>
                <Box>{getActions()}</Box>
            </Box>
        </>
    );

    function getActions() {
        return metadata.website || metadata.document || metadata.sourceSystem_id ? (
            <Box className="actionButtonGroup" pt="50px">
                {metadata.website ? (
                    <>
                        <Box className="actionButtonsHeader" id="actionButtonsHeader">
                            {actionButtonsHeader}
                        </Box>
                        <Link
                            to={metadata.website[0] as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={visitWebsiteText}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                    </>
                ) : null}
                {metadata.document ? (
                    <Link
                        to={metadata.document[0] as string}
                        className="actionButtonLink"
                        target="_blank"
                    >
                        <ActionButton
                            label={specificationText}
                            icon={<SpecificationIcon color="white" />}
                            variant="solid"
                            fun={() => void 0}
                        />
                    </Link>
                ) : null}
                {metadata.sourceSystem_id ? (
                    <Link
                        to={"https://rdamsc.bath.ac.uk/" + metadata.sourceSystem_id.replace(":","/") as string}
                        className="actionButtonLink"
                        target="_blank"
                    >
                        <ActionButton
                            label={visitMetadataSourceText}
                            icon={<MetadataSourceIcon color="white" />}
                            variant="outline"
                            fun={() => void 0}
                        />
                    </Link>
                ) : null}
                {metadata.website ? (
                    <CopyToClipboardButton data={metadata.website} label={copyUrlText} />
                ) : null}
            </Box>
        ) : null;
    }

    function getMetadata() {
        return (
            <Metadata
                metadataElements={[
                    {
                        element: "parentStandard",
                        tag:
                            metadata.parentStandard?.length > 1
                                ? "Parent standards"
                                : "Parent standard",
                        val: metadata.parentStandard
                    },
                    {
                        element: "theme",
                        tag: metadata.theme?.length > 1 ? themesText : themeText,
                        val: metadata.theme
                    },
                    {
                        element: "type",
                        tag: typeText,
                        val: metadata.type
                    },
                    {
                        element: "childStandard",
                        tag:
                            metadata.childStandard?.length > 1
                                ? "Child standards"
                                : "Child standard",
                        val: metadata.childStandard
                    },
                    {
                        element: "csd",
                        tag: metadata.xsd?.length > 1 ? "XSDs" : "XSD",
                        val: metadata.xsd
                    }
                ]}
                visibleElements={3}
                expandedByDefault={true}
            />
        );
    }
}
