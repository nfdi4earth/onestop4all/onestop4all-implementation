/* eslint-disable */
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";
import { Link } from "react-router-dom";

import { Abstract } from "../../components/ResourceType/Abstract/Abstract";
import { ActionButton } from "../../components/ResourceType/ActionButton/ActionButton";
import { CopyToClipboardButton } from "../../components/ResourceType/ActionButton/CopyToClipboardButton";
import { LastUpdate } from "../../components/ResourceType/Metadata/LastUpdate";
import { Metadata } from "../../components/ResourceType/Metadata/Metadata";
import { SolrSearchResultItem } from "../../services/SearchService";

export interface PublicationMetadataResponse extends SolrSearchResultItem {
    name: string;
    dateModified: string;
    description: string;
    author: string;
    keyword: string;
    relatedContent: Array<object>;
    sourceSystem_id: string;
    datePublished: string;
    license: string;
    additionalType: string;
}

export interface PublicationViewProps {
    item: PublicationMetadataResponse;
}

export function PublicationView(props: PublicationViewProps) {
    const metadata = props.item;
    const doiBaseUrl = "https://www.doi.org/";

    const intl = useIntl();

    // intl variables
    const authorsText = intl.formatMessage({ id: "metadata.authors" });
    const authorText = intl.formatMessage({ id: "metadata.author" });
    const keywordsText = intl.formatMessage({ id: "metadata.keywords" });
    const keywordText = intl.formatMessage({ id: "metadata.keyword" });
    const typeText = intl.formatMessage({ id: "metadata.type" });
    const publishedText = intl.formatMessage({ id: "metadata.published" });
    const licensesText = intl.formatMessage({ id: "metadata.licenses" });
    const licenseText = intl.formatMessage({ id: "metadata.license" });
    const additionalTypesText = intl.formatMessage({ id: "metadata.add-types" });
    const additionalTypeText = intl.formatMessage({ id: "metadata.add-type" });
    // const visitSourceText = intl.formatMessage({ id: "metadata.visit-source" });
    const actionButtonsHeader = intl.formatMessage({ id: "actionButton.header" });

    return (
        <>
            {/* Desktop view */}
            <Flex gap="10%" hideBelow="custombreak">
                <Box w="65%">
                    {metadata.name || metadata.name_alt ? (
                        <Box className="title" pt="15px">
                            {metadata.name
                                ? Array.isArray(metadata.name)
                                    ? metadata.name[0]
                                    : metadata.name
                                : Array.isArray(metadata.name_alt)
                                ? metadata.name_alt[0]
                                : metadata.name_alt}
                        </Box>
                    ) : (
                        ""
                    )}
                    <Box pt="36px">{getMetadata()}</Box>
                    {metadata.description ? (
                        <Box pt="40px">
                            <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                        </Box>
                    ) : null}
                </Box>
                <Box w="25%">
                    {getActions()}
                    {metadata.dateModified ? (
                        <Box pt="33">
                            <LastUpdate date={metadata.dateModified} />
                        </Box>
                    ) : null}
                </Box>
            </Flex>
            {/* Mobile view */}
            <Box hideFrom="custombreak">
                {metadata.name || metadata.name_alt ? (
                    <Box className="title" pt="15px">
                        {metadata.name
                            ? Array.isArray(metadata.name)
                                ? metadata.name[0]
                                : metadata.name
                            : Array.isArray(metadata.name_alt)
                            ? metadata.name_alt[0]
                            : metadata.name_alt}
                    </Box>
                ) : (
                    ""
                )}
                <Box pt="30px">{getMetadata()}</Box>
                {metadata.dateModified && (
                    <Box pt="18px">
                        <LastUpdate date={metadata.dateModified} />
                    </Box>
                )}
                {metadata.description ? (
                    <Box pt="40px">
                        <Abstract abstractText={Array.isArray(metadata.description) ? metadata.description : [metadata.description]} />
                    </Box>
                ) : null}
                <Box>{getActions()}</Box>
            </Box>
        </>
    );

    function getMetadata() {
        return (
            <Metadata
                metadataElements={[
                    {
                        element: "author",
                        tag: metadata.author?.length > 1 ? authorsText : authorText,
                        val: metadata.author
                    },
                    {
                        element: "keyword",
                        tag: metadata.keyword?.length > 1 ? keywordsText : keywordText,
                        val: metadata.keyword
                    },
                    {
                        element: "type",
                        tag: typeText,
                        val: metadata.type
                    },
                    {
                        element: "datePublished",
                        tag: publishedText,
                        val: new Date(metadata.datePublished).toLocaleDateString()
                    },
                    {
                        element: "license",
                        tag: metadata.license?.length > 1 ? licensesText : licenseText,
                        val: metadata.license
                    },
                    {
                        element: "additionalType",
                        tag:
                            metadata.additionalType?.length > 1
                                ? additionalTypesText
                                : additionalTypeText,
                        val: metadata.additionalType
                    }
                ]}
                visibleElements={2}
                expandedByDefault={true}
            />
        );
    }

    function getActions() {
        return metadata.sourceSystem_id ? (
            <Box className="actionButtonGroup" pt="50px">
                {metadata.sourceSystem_id ? (
                    <>
                        <Box className="actionButtonsHeader" id="actionButtonsHeader">
                            {actionButtonsHeader}
                        </Box>
                        <Link
                            to={(doiBaseUrl + metadata.sourceSystem_id) as string}
                            className="actionButtonLink"
                            target="_blank"
                        >
                            <ActionButton
                                label={intl.formatMessage({
                                    id: "metadata.read-article"
                                })}
                                icon={<ExternalLinkIcon color="white" />}
                                variant="solid"
                                fun={() => void 0}
                            />
                        </Link>
                        <CopyToClipboardButton
                            data={doiBaseUrl + metadata.sourceSystem_id}
                            label={intl.formatMessage({ id: "metadata.copy-url" })}
                        />
                    </>
                ) : null}
            </Box>
        ) : null;
    }
}
