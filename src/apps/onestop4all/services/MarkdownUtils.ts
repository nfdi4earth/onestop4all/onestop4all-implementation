/* eslint-disable */

export interface HtmlTag {
    href?: string;
    target: string;
    rel: string;
}

export function parseMarkdown(markdown: string) {
    const parser = new DOMParser();
    const html = parser.parseFromString(markdown, "text/html");
    return html;
}

export function processVideo(markdown: string, videoFolder: string) {
  if (markdown.includes('<img src="videos/')) {
    return markdown.replace(
      /<img\s+src="videos\/([^"]+)"\s+alt="">/g,
      (_match, filename) =>
        `<video src="${videoFolder}${filename}" width="640" height="360" controls></video>`
    );
  }
  return markdown;
}

export function getTags(html: Document) {
    const htmlTags = html.getElementsByTagName("a");
    return htmlTags;
}

export function getLinkType(link: string, url: string) {
    return link?.includes("mailto") && !link?.includes(".md") && !link?.includes("http")
        ? "mail"
        : link?.includes(".mp4")
        ? "video"
        : link?.includes("http") &&
          !link?.includes(url) &&
          !link?.includes("mailto") &&
          !link?.includes("cordra.knowledgehub.") &&
          !link?.includes("pdf/") &&
          !link?.includes(".docx")
        ? "url"
        : link?.includes("#bib")
        ? "bib_reference"
        : link?.includes("#") && 
          !link.includes("#bib")
        ? "fig_reference"
        : link?.includes(".md") && 
          !link?.includes("mailto") && 
          !link?.includes("cordra.knowledgehub.") && 
          !link?.includes("livinghandbook") 
        ? "markdown"
        : link?.includes("cordra.knowledgehub.")
        ? "cordra"
        : link?.includes("pdf/") && link?.includes(".docx")
        ? "pdf_docx"
        : link?.includes("pdf/") && link?.includes(".pdf")
        ? "pdf_docx"
        : undefined;
}
