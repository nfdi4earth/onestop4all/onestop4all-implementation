import "@open-pioneer/runtime";

import { ServiceOptions } from "@open-pioneer/runtime";
import {
    ResourceType,
    getHandler,
    mapFromResourceType,
    mapToResourceType
} from "./ResourceTypeUtils";

export interface SearchResultItem {
    id: string;
    title: string;
    resourceType: ResourceType;
    publishDate?: Date;
    updateDate?: Date;
    locality?: string;
    abstract: string;
    url: string;
    sourceSystemURL?: string;
    mainTitle?: string;
}

export interface SearchRequestParams {
    searchTerm?: string;
    resourceTypes?: string[];
    dataAccessTypes?: string[];
    dataFormatTypes?: string[];
    dataUploadTypes?: string[];
    supportedMetadataStandards?: string[];
    licenses?: string[];
    subjects?: string[];
    pageSize?: number;
    pageStart?: number;
    spatialFilter?: number[];
    temporalFilter?: TemporalFilter;
    temporalConfig?: TemporalConfig;
    sorting?: string;
    doiOption?: boolean;
    countryNames?: string[];
    publisher?: string[];
    hasAPI_conformsTo?: string[];
    dataLicense?: string[];
}

export interface TemporalConfig {
    startYear: number;
    endYear: number;
    gap: string;
}

export interface TemporalFilter {
    startYear: number;
    endYear: number;
}

export interface TemporalFacet {
    dateStr: string;
    count: number;
}

export interface SolrSearchResultItem {
    id: string;
    type: string[];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
}

interface SolrSearchResponse {
    numFound?: number;
    docs: SolrSearchResultItem[];
}

interface SolrFacetResponse {
    facet_fields: {
        [key: string]: [];
    };
    facet_ranges: {
        [key: string]: {
            counts: [];
        };
    };
}

export interface SubjectEntry {
    label: string;
    count: number;
}

export interface DataAccessTypeEntry {
    label: string;
    count: number;
}

export interface DataFormatTypeEntry {
    label: string;
    count: number;
}

export interface DataUploadTypeEntry {
    label: string;
    count: number;
}

export interface SupportedMetadataStandardEntry {
    label: string;
    count: number;
}

export interface LicensesEntry {
    label: string;
    count: number;
}

export interface CountriesEntry {
    label: string;
    count: number;
}

export interface PublisherEntry {
    label: string;
    count: number;
}

export interface DataLicenseEntry {
    label: string;
    count: number;
}

export interface ApiEntry {
    label: string;
    count: number;
}

export interface Facets {
    subjects: SubjectEntry[];
    resourceType: {
        resourceType: ResourceType;
        count: number;
    }[];
    temporal: TemporalFacet[];
    dataAccessType: DataAccessTypeEntry[];
    dataFormatType: DataFormatTypeEntry[];
    dataUploadType: DataUploadTypeEntry[];
    supportedMetadataStandard: SupportedMetadataStandardEntry[];
    licenses: LicensesEntry[];
    countries: CountriesEntry[];
    publisher: PublisherEntry[];
    hasAPI_conformsTo: ApiEntry[];
    dataLicense: DataAccessTypeEntry[];
}

export interface SearchResult {
    count: number;
    results: SearchResultItem[];
    facets: Facets;
}

export interface SolrConfig {
    url: string;
    coreSelector: string;
    auth: string;
    proxy: string;
    getFaqList: string;
    getFaq: string;
    getLhbStructure: string;
    bibliography: string;
    citationFileFormat: string;
    pdfFolder: string;
    videoFolder: string;
    lhbUrl: string;
    feedbackUrl: string;
    imageFolder: string;
    booster_title: string;
    booster_keyword: string;
    base_url: string;
}
export interface SupportFormConfig {
    api: {
        url: string,
        routes: {
            challenge: string,
            submit: string
        }
    };
}

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace JSX {
        interface IntrinsicElements {
            "altcha-widget": React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
        }
    }
}

declare module "react" {
  interface HTMLAttributes<T> {
    // extends React's HTMLAttributes
    challengeurl?: string;
    strings?: string;
    hidelogo?: boolean;
    hidefooter?: boolean;
  }
}

const SOLR_SUBJECT_FACET_FIELD = "subjectArea_str";
const SOLR_RESOURCE_TYPE_FACET_FIELD = "type";
const SOLR_TEMPORAL_FACET_RANGE_FIELD = "datePublished";
const SOLR_DataAccessType_FACET_FIELD = "dataAccessType";
const SOLR_DataFormatType_FACET_FIELD = "contentType_str";
const SOLR_DOI_FACET_FIELD = "assignsIdentifierScheme";
const SOLR_DataUploadType_FACET_FIELD = "dataUploadType";
const SOLR_SupportedMetadataStandard_FACET_FIELD = "supportsMetadataStandard_str";
const SOLR_LICENSES_FACET_FIELD = "software_license_str";
const SOLR_COUNTRIES_FACET_FIELD = "countryName_str";
const SOLR_PUBLISHER_FACET_FIELD = "publisher_str";
const SOLR_API_FACET_FIELD = "hasAPI_conformsTo_str";
const SOLR_DATALICENSE_FACET_FIELD = "dataLicense_str";

export class SearchService {
    private config: SolrConfig;
    public sf_config: SupportFormConfig;

    private resourceTypeFacetBlacklist: string[] = ["person_nested"];

    constructor(opts: ServiceOptions) {
        if (opts.properties.solr) {
            this.config = opts.properties.solr as SolrConfig;
        } else {
            throw new Error("Configuration for solr is missing.");
        }
        if (opts.properties.supportForm) {
            this.sf_config = opts.properties.supportForm as SupportFormConfig;
        } else {
            throw new Error("Configuration for support form is missing.");
        }
    }

    doSearch(searchParams: SearchRequestParams): Promise<SearchResult> {
        //console.log("Search with following parameters: " + JSON.stringify(searchParams));

        const queryParams = this.createQueryParams();

        this.addSearchterm(searchParams.searchTerm, queryParams);

        this.addPaging(searchParams.pageSize, searchParams.pageStart, queryParams);

        this.addFacet(queryParams);

        this.addResourceTypes(searchParams.resourceTypes, queryParams);

        this.addSubjects(searchParams.subjects, queryParams);

        this.addDataAccessTypes(searchParams.dataAccessTypes, queryParams);

        this.addDataFormatTypes(searchParams.dataFormatTypes, queryParams);

        this.addDoi(searchParams.doiOption, queryParams);

        this.addDataUploadTypes(searchParams.dataUploadTypes, queryParams);

        this.addSupportedMetadataStandard(searchParams.supportedMetadataStandards, queryParams);

        this.addLicenses(searchParams.licenses, queryParams);

        this.addCountries(searchParams.countryNames, queryParams);

        this.addPublisher(searchParams.publisher, queryParams);

        this.addApi(searchParams.hasAPI_conformsTo, queryParams);

        this.addDataLicense(searchParams.dataLicense, queryParams);

        this.addSpatialFilter(searchParams.spatialFilter, queryParams);

        this.addSorting(searchParams.sorting, queryParams);

        this.addTemporalFilter(
            searchParams.temporalFilter,
            queryParams,
            searchParams.temporalConfig
        );


        const url = `${this.config.url}?${queryParams.toString()}`;
        // console.log(url);

        return fetch(url, {
            headers: this.getHeaders()
        }).then((response) =>
            response
                .json()
                .then(
                    (responseData: {
                        response: SolrSearchResponse;
                        facet_counts: SolrFacetResponse;
                    }) => {
                        const { response } = responseData;
                        if (response.numFound !== undefined && response.docs !== undefined) {
                            //console.log(response);
                            return {
                                count: response.numFound,
                                results: this.createResultEntries(response.docs),
                                facets: this.createFacets(responseData.facet_counts)
                            };
                        } else {
                            throw new Error("Unexpected response: " + JSON.stringify(responseData));
                        }
                    }
                )
        );
    }

    getMetadata(resourceId: string) {
        const queryParams = this.createQueryParams();
        if (resourceId) {
            queryParams.set("ids", resourceId);
            this.addChildQueryParams(queryParams);
        }
        const url = `${this.config.url}?${queryParams.toString()}`;
        return fetch(url, {
            headers: this.getHeaders()
        }).then((response) =>
            response.json().then((responseData: { response: SolrSearchResponse }) => {
                const { response } = responseData;
                if (response.numFound !== undefined && response.docs !== undefined) {
                    return {
                        count: response.numFound,
                        results: response.docs
                    };
                } else {
                    throw new Error("Unexpected response: " + JSON.stringify(responseData));
                }
            })
        );
    }

    sendSupportRequest(name: string, email: string, subject: string, content: string, institution: string, category: string, altcha: string) {
        console.log("Send support form request");
        const url = this.sf_config.api.url + this.sf_config.api.routes.submit;
        const data = {
            contact_name: name,
            contact_email: email,
            subject: subject,
            message: content,
            institution: institution,
            category: category,
            altcha: altcha,
        };
        const options = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
        };
        return fetch(url, options).then((response) =>
            response.json().then((responseData: {message: ""}) => {
                if (responseData) {
                    return responseData.message;
                } else {
                    throw new Error("Unexpected response: " + JSON.stringify(responseData));
                }
            })
        );
    }

    getFaqList() {
        const url = this.config.proxy + this.config.getFaqList;
        return fetch(url, {
            headers: this.getHeaders()
        }).then((response) =>
            response.text().then((responseData: string) => {
                if (responseData) {
                    return responseData;
                } else {
                    throw new Error("Unexpected response: " + JSON.stringify(responseData));
                }
            })
        );
    }

    getFaq(faqId: string) {
        const url = this.config.proxy + this.config.getFaq + faqId;
        return fetch(url, {
            headers: this.getHeaders()
        }).then((response) =>
            response.text().then((responseData: string) => {
                if (responseData) {
                    return responseData;
                } else {
                    throw new Error("Unexpected response: " + JSON.stringify(responseData));
                }
            })
        );
    }

    /*getHowToEntry(howToEntry: string) {
        //console.log("start fetching entry point for id: " + howToEntry);
        const url =
            this.config.proxy +
            `https://git.rwth-aachen.de/api/v4/projects/79252/repository/files/docs%2f` +
            howToEntry +
            `/raw`;
        return fetch(url, this.getHeaders()).then((response) =>
            response.text().then((responseData: string) => {
                if (responseData) {
                    return responseData;
                } else {
                    throw new Error("Unexpected response: " + JSON.stringify(responseData));
                }
            })
        );
    }*/

    getLhbStructure() {
        const url = this.config.proxy + this.config.getLhbStructure;
        return fetch(url, {
            headers: this.getHeaders()
        }).then((response) =>
            response.text().then((responseData: string) => {
                if (responseData) {
                    return responseData;
                } else {
                    throw new Error("Unexpected response: " + JSON.stringify(responseData));
                }
            })
        );
    }

    getChapter(chapter: string) {
        const url =
            `${this.config.url}?ident=true&q.op=OR&q=sourceSystem_id%3A"` +
            chapter +
            `"`;
        return fetch(url, {
            headers: this.getHeaders()
        }).then((response) =>
            response.json().then((responseData: { response: object }) => {
                if (responseData) {
                    return responseData;
                } else {
                    throw new Error("Unexpected response: " + JSON.stringify(responseData));
                }
            })
        );
    }

    getRehypeCitationOptions() {
        const bibliography = this.config.proxy + this.config.bibliography;
        const citationFileFormat = this.config.citationFileFormat;
        const pdfFolder = this.config.pdfFolder;
        const videoFolder = this.config.videoFolder;
        const lhbUrl = this.config.lhbUrl;
        return {
            bib: bibliography,
            cff: citationFileFormat,
            proxy: this.config.proxy,
            pdfFolder: pdfFolder,
            videoFolder: videoFolder,
            lhbUrl: lhbUrl
        };
    }

    getBaseUrl() {
        return this.config.base_url;
    }

    private getHeaders(): {Authorization?: string;} {
        if (this.config.auth != "") {
            return { Authorization: "Basic " + btoa(this.config.auth) };
        } else return {};
    }

    getFeedbackUrl() {
        return { feedbackUrl: this.config.feedbackUrl };
    }

    getImageUrl() {
        return { imageUrl: this.config.imageFolder };
    }

    private addSpatialFilter(spatialFilter: number[] | undefined, queryParams: URLSearchParams) {
        if (spatialFilter && spatialFilter.length > 0) {
            if (spatialFilter.length === 4) {
                const [minLon, minLat, maxLon, maxLat] = spatialFilter;
                queryParams.append("fq", `geometry:[${minLat},${minLon} TO ${maxLat},${maxLon}]`);
            }
        }
    }

    private addTemporalFilter(
        temporalFilter: TemporalFilter | undefined,
        queryParams: URLSearchParams,
        temporalConfig?: TemporalConfig
    ) {
        if (temporalFilter) {
            queryParams.append(
                "fq",
                `datePublished:[${temporalFilter.startYear} TO ${temporalFilter.endYear}]`
            );
        }
        if (temporalConfig) {
            queryParams.set("facet.range", SOLR_TEMPORAL_FACET_RANGE_FIELD);
            queryParams.set("facet.range.start", `${temporalConfig.startYear}-01-01T00:00:00Z`);
            queryParams.set("facet.range.end", `${temporalConfig.endYear + 1}-01-01T00:00:00Z`);
            queryParams.set("facet.range.gap", temporalConfig.gap);
        }
    }

    private addResourceTypes(resourceTypes: string[] | undefined, queryParams: URLSearchParams) {
        if (resourceTypes?.length) {
            const mapping = resourceTypes.map((e) => mapFromResourceType(e as ResourceType));
            queryParams.append(
                "fq",
                `${SOLR_RESOURCE_TYPE_FACET_FIELD}:(${mapping.map((e) => `"${e}"`).join(" OR ")})`
            );
        }
    }

    private addSubjects(subjects: string[] | undefined, queryParams: URLSearchParams) {
        if (subjects?.length) {
            queryParams.append(
                "fq",
                `${SOLR_SUBJECT_FACET_FIELD}:(${subjects.map((e) => `"${e}"`).join(" OR ")})`
            );
        }
    }

    private addDataAccessTypes(
        dataAccessTypes: string[] | undefined,
        queryParams: URLSearchParams
    ) {
        if (dataAccessTypes?.length) {
            queryParams.append(
                "fq",
                `${SOLR_DataAccessType_FACET_FIELD}:(${dataAccessTypes
                    .map((e) => `"${e}"`)
                    .join(" OR ")})`
            );
        }
    }

    private addDataFormatTypes(
        dataFormatTypes: string[] | undefined,
        queryParams: URLSearchParams
    ) {
        if (dataFormatTypes?.length) {
            queryParams.append(
                "fq",
                `${SOLR_DataFormatType_FACET_FIELD}:(${dataFormatTypes
                    .map((e) => `"${e}"`)
                    .join(" OR ")})`
            );
        }
    }

    private addDoi(
        doiOption: boolean | undefined,
        queryParams: URLSearchParams
    ) {
        if (doiOption) {
            queryParams.append(
                "fq",
                `${SOLR_DOI_FACET_FIELD}:("http://purl.org/spar/datacite/doi")`
            );
        }
    }

    private addCountries(
        countries: string[] | undefined,
        queryParams: URLSearchParams
    ) {
        if (countries?.length) {
            queryParams.append(
                "fq",
                `${SOLR_COUNTRIES_FACET_FIELD}:(${countries
                    .map((e) => `"${e}"`)
                    .join(" OR ")})`
            );
        }
    }

    private addPublisher(
        publisher: string[] | undefined,
        queryParams: URLSearchParams
    ) {
        if (publisher?.length) {
            queryParams.append(
                "fq",
                `${SOLR_PUBLISHER_FACET_FIELD}:(${publisher
                    .map((e) => `"${e}"`)
                    .join(" OR ")})`
            );
        }
    }

    private addApi(
        hasAPI_conformsTo: string[] | undefined,
        queryParams: URLSearchParams
    ) {
        if (hasAPI_conformsTo?.length) {
            queryParams.append(
                "fq",
                `${SOLR_API_FACET_FIELD}:(${hasAPI_conformsTo
                    .map((e) => `"${e}"`)
                    .join(" OR ")})`
            );
        }
    }

    private addDataLicense(
        dataLicense: string[] | undefined,
        queryParams: URLSearchParams
    ) {
        if (dataLicense?.length) {
            queryParams.append(
                "fq",
                `${SOLR_DATALICENSE_FACET_FIELD}:(${dataLicense
                    .map((e) => `"${e}"`)
                    .join(" OR ")})`
            );
        }
    }

    private addDataUploadTypes(
        dataUploadTypes: string[] | undefined,
        queryParams: URLSearchParams
    ) {
        if (dataUploadTypes?.length) {
            queryParams.append(
                "fq",
                `${SOLR_DataUploadType_FACET_FIELD}:(${dataUploadTypes
                    .map((e) => `"${e}"`)
                    .join(" OR ")})`
            );
        }
    }

    private addSupportedMetadataStandard(
        supportedMetadataStandards: string[] | undefined,
        queryParams: URLSearchParams
    ) {
        if (supportedMetadataStandards?.length) {
            queryParams.append(
                "fq",
                `${SOLR_SupportedMetadataStandard_FACET_FIELD}:(${supportedMetadataStandards
                    .map((e) => `"${e}"`)
                    .join(" OR ")})`
            );
        }
    }

    private addLicenses(licenses: string[] | undefined, queryParams: URLSearchParams) {
        if (licenses?.length) {
            queryParams.append(
                "fq",
                `${SOLR_LICENSES_FACET_FIELD}:(${licenses.map((e) => `"${e}"`).join(" OR ")})`
            );
        }
    }

    private addFacet(queryParams: URLSearchParams) {
        // add parameter to request facets
        queryParams.set("facet", "true");
        // parameter to get facet for resource type
        queryParams.set("facet.field", SOLR_RESOURCE_TYPE_FACET_FIELD);
        queryParams.append("facet.field", SOLR_SUBJECT_FACET_FIELD);
        queryParams.append("facet.field", SOLR_DataAccessType_FACET_FIELD);
        queryParams.append("facet.field", SOLR_DataFormatType_FACET_FIELD);
        queryParams.append("facet.field", SOLR_DataUploadType_FACET_FIELD);
        queryParams.append("facet.field", SOLR_SupportedMetadataStandard_FACET_FIELD);
        queryParams.append("facet.field", SOLR_LICENSES_FACET_FIELD);
        queryParams.append("facet.field", SOLR_DOI_FACET_FIELD);
        queryParams.append("facet.field", SOLR_COUNTRIES_FACET_FIELD);
        queryParams.append("facet.field", SOLR_PUBLISHER_FACET_FIELD);
        queryParams.append("facet.field", SOLR_API_FACET_FIELD);
        queryParams.append("facet.field", SOLR_DATALICENSE_FACET_FIELD);
        queryParams.set("facet.limit", "1000");
    }

    private addPaging(
        pageSize: number | undefined,
        pageStart: number | undefined,
        queryParams: URLSearchParams
    ) {
        if (pageSize !== undefined) {
            queryParams.set("rows", pageSize.toString());
            if (pageStart !== undefined) {
                queryParams.set("start", (pageStart * pageSize).toString());
            }
        }
    }

    private addSorting(sorting: string | undefined, queryParams: URLSearchParams) {
        if (sorting !== undefined && sorting !== "") {
            queryParams.set("sort", sorting);
        }
    }

    private addSearchterm(searchTerm: string | undefined, queryParams: URLSearchParams) {
        if (searchTerm) {
            queryParams.set("q", searchTerm);
            console.log("bosster title: ", this.config.booster_title, " bosster_keyword: ", this.config.booster_keyword);
            queryParams.set("qf", `title^${this.config.booster_title} keyword^${this.config.booster_keyword} collector`);
            //queryParams.set("df", "collector");
        } else {
            queryParams.set("q", "*:*");
        }
        this.addChildQueryParams(queryParams);
    }

    private addChildQueryParams(queryParams: URLSearchParams) {
        queryParams.set("fl", "*, [child author]");
        queryParams.append("fq", '-type:"person_nested"');
    }

    private createQueryParams(): URLSearchParams {
        const queryParams: URLSearchParams = new URLSearchParams();
        queryParams.set("ident", "true");
        queryParams.set("q.op", "OR");
        queryParams.set("defType", "edismax");
        queryParams.set("bq", "isEdutrain:true^1000");
        return queryParams;
    }

    private createFacets(facet_counts: SolrFacetResponse): Facets {
        return {
            subjects: this.createSubjectFacets(facet_counts),
            resourceType: this.createResourceTypeFacet(facet_counts),
            temporal: this.createTemporalFacet(facet_counts),
            dataAccessType: this.createDataAccessTypeFacet(facet_counts),
            dataFormatType: this.createDataFormatTypeFacet(facet_counts),
            dataUploadType: this.createDataUploadTypeFacet(facet_counts),
            supportedMetadataStandard: this.createSupportedMetadataStandardFacet(facet_counts),
            licenses: this.createLicensesFacet(facet_counts),
            countries: this.createCountriesFacet(facet_counts),
            publisher: this.createPublisherFacet(facet_counts),
            hasAPI_conformsTo: this.createApiFacet(facet_counts),
            dataLicense: this.createDataLicenseFacet(facet_counts)
        };
    }

    private createDataAccessTypeFacet(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_DataAccessType_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createDataFormatTypeFacet(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_DataFormatType_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createDataUploadTypeFacet(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_DataUploadType_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createSupportedMetadataStandardFacet(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_SupportedMetadataStandard_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createLicensesFacet(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_LICENSES_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createCountriesFacet(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_COUNTRIES_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createPublisherFacet(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_PUBLISHER_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createApiFacet(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_API_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createDataLicenseFacet(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_DATALICENSE_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createSubjectFacets(
        facet_counts: SolrFacetResponse
    ): { label: string; count: number }[] {
        const themeFacets = facet_counts.facet_fields[SOLR_SUBJECT_FACET_FIELD];
        return themeFacets ? this.createFragments(themeFacets) : [];
    }

    private createResourceTypeFacet(facet_counts: SolrFacetResponse) {
        const resourceTypeFacet = facet_counts.facet_fields["type"];
        if (resourceTypeFacet) {
            return this.createFragments(resourceTypeFacet).map((e) => ({
                resourceType: mapToResourceType(e.label),
                count: e.count
            }));
        }
        return [];
    }

    private createTemporalFacet(facet_counts: SolrFacetResponse) {
        const facetCounts = facet_counts.facet_ranges[SOLR_TEMPORAL_FACET_RANGE_FIELD]?.counts;
        if (facetCounts) {
            return this.createFragments(facetCounts).map((e) => ({
                dateStr: e.label.substring(0, 4),
                count: e.count
            }));
        }
        return [];
    }

    private createFragments(facetResponse: Array<string | number>) {
        type FacetFragment = { label: string; count: number };
        const entries: FacetFragment[] = [];
        for (let i = 0; i < facetResponse.length; i += 2) {
            const [label, count] = facetResponse.slice(i, i + 2);
            if (typeof label === "string" && typeof count === "number") {
                const idx = this.resourceTypeFacetBlacklist.findIndex((e) => e === label);
                if (idx === -1) {
                    entries.push({ label, count });
                }
            }
        }
        return entries;
    }

    private createResultEntries(docs: SolrSearchResultItem[]): SearchResultItem[] {
        return docs.map((item) => getHandler(item).handle(item));
    }
}

declare module "@open-pioneer/runtime" {
    interface ServiceRegistry {
        "onestop4all.SearchService": SearchService;
    }
}
