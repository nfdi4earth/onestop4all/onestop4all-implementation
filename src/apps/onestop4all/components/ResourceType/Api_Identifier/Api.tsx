import { Box } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";

export const Api = (props: { apis: Array<string>; urls: Array<string> }) => {
    const { apis, urls } = props;
    const intl = useIntl();
    const type = intl.formatMessage({ id: "metadata.type" });
    const types = intl.formatMessage({ id: "metadata.types" });

    return (
        <>
            {apis.length > 0 || urls.length > 0 ? (
                <Box>
                    <div className="api_identifierHeader">API</div>
                    <Box pt="12px">
                        {apis.length > 0 ? (
                            <div>
                                <span className="api_key">
                                    {apis.length > 1 ? types : type}:&nbsp;
                                </span>
                                {apis.map((api, i) => (
                                    <>
                                        <span className="api_val">{api}</span>
                                        {i < apis.length - 1 ? <>,&nbsp;</> : ""}
                                    </>
                                ))}
                            </div>
                        ) : null}
                        {urls.length > 0 ? (
                            <div>
                                <span className="api_key">
                                    {urls.length > 1 ? "URLs" : "URL"}:&nbsp;
                                </span>
                                {urls.map((url, i) => (
                                    <>
                                        <a href={url} target="_blank" rel="noreferrer" key={i}>
                                            <span className="api_url">{url}</span>
                                        </a>
                                        {i < urls.length - 1 ? (
                                            <>
                                                ,&nbsp;
                                                <br />
                                            </>
                                        ) : (
                                            ""
                                        )}
                                    </>
                                ))}
                            </div>
                        ) : null}
                    </Box>
                </Box>
            ) : null}
        </>
    );
};
