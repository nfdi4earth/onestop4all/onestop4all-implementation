import { Box } from "@open-pioneer/chakra-integration";
import { isUrl, MetadataUrl } from "./PersonalInfo";
import { Link } from "react-router-dom";

import { mapToResourceType } from "../../../services/ResourceTypeUtils"; 

export const Type = (props: { tag: string; val: Array<string> }) => {
    const { tag, val } = props;

    return (
        <Box className="metadataSection">
            <div className="seperator"></div>
            {tag ? <span className="metadataTag">{tag}:&nbsp;</span> : null}
            {typeof val == "object" ? (
                val.map((elem: string, i: number) => (
                    <span className="metadataValue" key={i}>
                        {isUrl(elem) ? (
                            <span>
                                <Link style={{ color: "#05668D", textDecoration: "underline" }} target="_blank" to={"/search?resourcetype=" + mapToResourceType(elem)}>
                                    {mapToResourceType(elem)}
                                </Link>
                                {i < val.length - 1 ? "; " : ""}
                            </span>
                        ) : (
                            elem + (i < val.length - 1 ? ";" : "")
                        )}
                        &nbsp;
                    </span>
                ))
            ) : (
                <span className="metadataValue">
                    {!isUrl(val) ? (
                        val
                    ) : (
                        <MetadataUrl item={val} type="url" />
                    )}
                </span>
            )}
        </Box>
    );
};
