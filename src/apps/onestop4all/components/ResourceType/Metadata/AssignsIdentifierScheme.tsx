import { Box } from "@open-pioneer/chakra-integration";
import { isUrl, MetadataUrl } from "./PersonalInfo";

export const AssignsIdentifierScheme = (props: { tag: string; val: Array<string> }) => {
    const { tag, val } = props;

    return (
        <Box className="metadataSection">
            <div className="seperator"></div>
            {tag ? <span className="metadataTag">{tag}:&nbsp;</span> : null}
            {typeof val == "object" ? (
                val.map((elem: string, i: number) => (
                    <span className="metadataValue" key={i}>
                        {
                            elem.split("/").pop() + (i < val.length - 1 ? ";" : "")
                        }
                        &nbsp;
                    </span>
                ))
            ) : (
                <span className="metadataValue">
                    {!isUrl(val) ? (
                        val
                    ) : (
                        <MetadataUrl item={val} type="url" />
                    )}
                </span>
            )}
        </Box>
    );
};
