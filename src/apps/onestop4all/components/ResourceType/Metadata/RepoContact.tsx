/* eslint-disable */
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { MetadataUrl, isEmail, isUrl } from "./PersonalInfo";

export const RepoContact = ({ mails = [], urls = [], tag }: { mails: string[]; urls: string[]; tag: string }) => {
    if (!mails.length && !urls.length) return null;

    return (
        <Box className="metadataSection">
            <div className="seperator"></div>
            <Flex>
                <span className="metadataTag">{tag}:&nbsp;</span>
                <Flex className="repoValue">
                    {mails.map((email, i) =>
                        isEmail(email) ? (
                            <span key={i}>
                                <MetadataUrl item={email} type="mail" />
                                {i < mails.length - 1 ? ", " : ""}
                            </span>
                        ) : (
                            <span key={i}>{email}</span>
                        )
                    )}

                    {Boolean(mails.length && urls.length) && "; "}

                    {urls.map((url, i) =>
                        isUrl(url) ? (
                            <a key={i} href={url} rel="noreferrer" target="_blank">
                                {url}
                            </a>
                        ) : (
                            <span key={i}>{url}</span>
                        )
                    )}
                </Flex>
            </Flex>
        </Box>
    );
};
