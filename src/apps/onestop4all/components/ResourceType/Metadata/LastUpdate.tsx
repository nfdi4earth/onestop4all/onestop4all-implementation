import { Box } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";

export const LastUpdate = (props: { date: string }) => {
    const date = new Date(props.date).toLocaleDateString();
    const intl = useIntl();

    return (
        <Box>
            <span className="lastUpdateKey">
                {" "}
                {intl.formatMessage({ id: "search.last-update" })}{" "}
            </span>
            <span className="lastUpdateValue">{date}</span>
        </Box>
    );
};
