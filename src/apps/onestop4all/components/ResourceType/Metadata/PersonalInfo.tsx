import { Image } from "@open-pioneer/chakra-integration";

export function isUrl(url: string) {
    const urlRegex = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i;
    return urlRegex.test(url);
}

export function isEmail(address: string) {
    const emailRegex = /^[^@\s]+@[^@\s]+\.[^@\s]+$/;
    return emailRegex.test(address);
}

export const MetadataUrl = ({ item, type }: { item: string; type: string }) => (
    <a
        href={type === "mail" ? `mailto:${item}` : item}
        className="metadataLink"
        rel="noreferrer"
        target="_blank"
    >
        {item.replace(/^mailto:/i, "")}
    </a>
);

export const PersonalInfo = ({ name, orcid, email }: { name: string; orcid: string; email: string }) => (
    <>
        {name && (
            <>
                {name}
                <span style={{ marginRight: "2px" }} />
            </>
        )}

        {email && (
            <>
                &nbsp;(
                {isEmail(email) ? <MetadataUrl item={email} type="mail" /> : email}
                )&nbsp;
            </>
        )}

        {Boolean(orcid) && (
            <a href={isUrl(orcid) ? orcid : `https://orcid.org/${orcid}`} rel="noreferrer" target="_blank">
                <Image className="orcid" alt="ORCID icon" src="/orcid.png" />
            </a>
        )}
    </>
);
