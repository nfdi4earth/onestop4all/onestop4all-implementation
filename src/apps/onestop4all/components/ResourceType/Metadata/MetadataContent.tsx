import { Box } from "@open-pioneer/chakra-integration";
import { Keywords } from "./Keywords";
import { Authors } from "./Authors";
import { NfdiContact } from "./NfdiContact";
import { RepoContact } from "./RepoContact";
import { Publisher } from "./Publisher";
import { Source } from "./Source";
import { Misc } from "./Misc";
import { Type } from "./Type";
import { AssignsIdentifierScheme } from "./AssignsIdentifierScheme";

export const MetadataContent = (props: {
    metadataElements: object;
    start: number;
    end: number;
}) => {
    const { start, end } = props;
    const metadataElements = Object.values(props.metadataElements);

    return (
        <Box>
            {metadataElements
                .slice(start, end)
                .map((e, i) =>
                    e.val && e.tag && e.element ? (
                        e.element == "keyword" ? (
                            <Keywords key={i} list={e.val} tag={e.tag} element={e.element} />
                        ) : e.element == "author" || e.tag == "Resource providers" ? (
                            <Authors key={i} authors={e.val} tag={e.tag} />
                        ) : e.element == "theme" ? (
                            <Keywords key={i} list={e.val} tag={e.tag} element={e.element} />
                        ) : e.element == "nfdi" ? (
                            <NfdiContact key={i} contact={e.val} tag={e.tag} />
                        ) : e.element == "contact" ? (
                            <RepoContact key={i} mails={e.val.mail} urls={e.val.urls} tag={e.tag} />
                        ) : e.element == "publisher" || e.element == "supportsMetadataStandard" ? (
                            <Publisher
                                key={i}
                                publishers={e.val.publishers}
                                publishers_homepage={e.val.publishers_homepage}
                                tag={e.tag}
                            />
                        ) : e.element == "source" ? (
                            <Source key={i} source={e.val} />
                        ) : e.element == "type" ? ( 
                            <Type key={i} tag={e.tag} val={e.val} />
                        ) : e.element == "assignsIdentifierScheme" ? ( 
                            <AssignsIdentifierScheme key={i} tag={e.tag} val={e.val} />
                        ) : (
                            <Misc key={i} tag={e.tag} val={e.val} />
                        )
                    ) : null
                )}
        </Box>
    );
};
