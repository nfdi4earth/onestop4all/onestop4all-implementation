import { Box, Flex } from "@open-pioneer/chakra-integration";
import { MetadataUrl, PersonalInfo, isEmail, isUrl } from "./PersonalInfo";

export const Publisher = (props: {
    publishers: string[];
    publishers_homepage: string[];
    tag: string;
}) => {
    const { publishers, publishers_homepage, tag } = props;

    return (
        <>
            {publishers && publishers.length > 0 ? (
                <Box className="metadataSection">
                    <div className="seperator"></div>
                    <span className="metadataTag">{tag}:&nbsp;</span>
                    {publishers.map((publisher, i) =>
                        publishers_homepage && publishers_homepage[i] !== undefined ? (
                            <>
                                <a
                                    className="metadataValue"
                                    key={i}
                                    href={publishers_homepage[i]}
                                    rel="noreferrer"
                                    target="_blank"
                                >
                                    {publisher}
                                </a>
                                {i < publishers.length - 1 ? <>;&nbsp;</> : ""}
                            </>
                        ) : (
                            <>
                                <div className="metadataValue" key={i}>
                                    {publisher}
                                </div>
                                {i < publishers.length - 1 ? <>;&nbsp;</> : ""}
                            </>
                        )
                    )}
                </Box>
            ) : null}
        </>
    );
};
