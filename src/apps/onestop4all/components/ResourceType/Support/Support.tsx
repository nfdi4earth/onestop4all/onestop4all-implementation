import { Box } from "@open-pioneer/chakra-integration";
import { ActionButton } from "../ActionButton/ActionButton";
import { UserSupportIconWhite } from "../../Icons";
import { useState } from "react";
import { SupportForm } from "../../SupportForm/SupportForm";

import { useIntl } from "open-pioneer:react-hooks";

export const Support = () => {
    const [openSupportForm, setOpenSupportForm] = useState(false);

    const intl = useIntl();
    return (
        <>
            <Box>
                <div className="supportHeader">{intl.formatMessage({ id: "support.header" })}</div>
                <div className="supportText">{intl.formatMessage({ id: "support.text" })}</div>
                <ActionButton
                    label={intl.formatMessage({ id: "support.button" })}
                    icon={<UserSupportIconWhite />}
                    variant="solid"
                    fun={() => setOpenSupportForm(true)}
                />
            </Box>
            <SupportForm openForm={openSupportForm} menuClosed={() => setOpenSupportForm(false)} />
        </>
    );
};
