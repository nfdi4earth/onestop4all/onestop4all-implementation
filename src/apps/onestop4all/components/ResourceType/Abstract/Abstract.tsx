/* eslint-disable */
import { useEffect, useRef, useState } from "react";
import { useService } from "open-pioneer:react-hooks";
import parse from "html-react-parser";

import { Box } from "@open-pioneer/chakra-integration";
import { getLinkType, getTags, parseMarkdown } from "../../../services/MarkdownUtils";
import rehypeStringify from "rehype-stringify";
import remarkGfm from "remark-gfm";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import rehypeRaw from "rehype-raw";
import { unified } from "unified";
import { HowToResponse } from "../../../views/Start/HowTo/HowToEntryContent";
import { SearchService } from "../../../services/SearchService";

export const Abstract = (props: { abstractText: string [] }) => {
    const { abstractText } = props;
    const [markdownContents, setMdCon] = useState<string[]>([]);
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const opts = searchSrvc.getRehypeCitationOptions();

    useEffect(() => {
        const newMarkdownContents: string[] = [];

        abstractText.forEach((markdown) => {
            unified()
                .use(remarkParse)
                .use(remarkGfm)
                .use(remarkRehype, {allowDangerousHtml: true})
                .use(rehypeRaw)
                //.use(rehypeCitation, rehypeCitationOptions)
                .use(rehypeStringify)
                .process(markdown)
                .then((file) => {
                    const html = parseMarkdown(file.value as string);
                    const htmlTags = getTags(html);
                    if (html && htmlTags && htmlTags.length > 0) {
                        for (let i = 0; i < htmlTags.length; i++) {
                            const tmp = htmlTags[i];
                            const tag = html.getElementsByTagName("a")[i];
                            const link = tag?.href;
                            const url = html.URL;
                            const linkType = getLinkType(link as string, url as string);
                            if (tmp && linkType) {
                                tmp.target = "_blank";
                                tmp.rel = "noopener";
                                if (linkType === "bib_reference") {
                                    tmp.href = "javascript:void(0)";
                                    tmp.innerHTML = '<button class="bib-reference-button">' + tmp.innerHTML + '</button>';
                                    tmp.target = "";
                                    tmp.rel = "";
                                }
                                if (linkType === "fig_reference") {
                                    tmp.href = "javascript:void(0)";
                                    tmp.innerHTML = '<button class="fig-reference-button" data-link="' + link + '">' + tmp.innerHTML + '</button>';                                tmp.target = "";
                                    tmp.rel = "";
                                }
                                if (linkType == "markdown") {
                                    const markdownLink = html
                                        .getElementsByTagName("a")
                                        [i]?.href.split("/")
                                        .pop();
                                    if (markdownLink) {
                                        searchSrvc.getChapter(markdownLink).then((result) => {
                                            const res = result.response as HowToResponse;
                                            const id =
                                                res && res.docs && res.docs[0] ? res.docs[0].id : "";
                                            tmp.href = "/result/" + id;
                                        });
                                    }
                                }
                                if (linkType == "cordra") {
                                    const id = tmp.href.split("/").pop();
                                    tmp.href = "/result/" + id;
                                }
                                if (linkType == "pdf_docx") {
                                    const id = tmp.href.split("/").pop();
                                    tmp.href = opts.pdfFolder + id;
                                }
                                if (linkType == "video") {
                                    const id = tmp.href.split("/").pop();
                                    tmp.href = opts.videoFolder + id;
                                }
                            }
                        }
                    }
                    newMarkdownContents.push(html.body.innerHTML as string);
                    setMdCon([...newMarkdownContents]);
                });
        });
    }, [abstractText]);

    return (
        <Box>
            <div className="abstractSectionHeader">Abstract</div>
            {markdownContents.map((content, index) => (
                <div key={index} className="abstractText">{parse(content)}</div>
            ))}
        </Box>
    );
};
