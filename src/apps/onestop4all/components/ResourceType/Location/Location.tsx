import { useState } from "react";
import { Box, Flex } from "@open-pioneer/chakra-integration";
import { Map } from "../Map/Map";
import { GoToLocationBtnIcon } from "../../Icons";
import { ActionButton } from "../ActionButton/ActionButton";

import { useIntl } from "open-pioneer:react-hooks";

export const Location = (props: { address: any; location: string; mapId: string }) => {
    //for some reason, defining address as an object instead of any results in an error....
    const { address, location, mapId } = props;
    const [triggerPositioning, setTriggerPositioning] = useState(0);

    const intl = useIntl();
    return (
        <Box>
            <div className="metadataSectionHeader">{address.tag}</div>
            <Flex 
                gap={{ base: "0px", custombreak: "10px" }}
                direction={{ base: "column", custombreak: "inherit" }}
                marginBottom={"10px"}
            >   
                {address.val}     
            </Flex>
            <Flex 
                gap={{ base: "0px", custombreak: "10px" }}
                direction={{ base: "column", custombreak: "inherit" }}
                marginBottom={"10px"}
            >   
                <ActionButton
                    label={intl.formatMessage({ id: "metadata.go-to-location" })}
                    icon={<GoToLocationBtnIcon />}
                    variant="outline"
                    fun={() => {
                        setTriggerPositioning((trigger) => trigger + 1);
                    }}
                />    
            </Flex>
            <Map
                geometry={location}
                height="35vh"
                triggerPositioning={triggerPositioning}
                mapId={mapId}
            />
        </Box>
    );
};
