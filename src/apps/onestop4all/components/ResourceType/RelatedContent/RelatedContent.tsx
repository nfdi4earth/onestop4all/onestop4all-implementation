/* eslint-disable */
import { Box, Flex, Divider } from "@open-pioneer/chakra-integration";
import { SimpleGrid } from "@chakra-ui/react";

import { RelatedContentEntry } from "./RelatedContentEntry";
import { useIntl } from "open-pioneer:react-hooks";
import { useState } from "react";

export const RelatedContent = (props: { relatedContentItems: object }) => {
    const relatedContentItemsList = Object.values(props.relatedContentItems);
    const intl = useIntl();
    const [itemAvailable, setItemAvailable] = useState(false);

    relatedContentItemsList.forEach((elem) => {
        if (elem.length > 0 && !elem[0].title && elem[0].name) {
            elem[0].title = elem[0].name;
            delete elem[0].name;
        }
    });

    relatedContentItemsList.sort((a, b) => {
        if (a.length > 0) {
            if (!itemAvailable) {
                setItemAvailable(true);
            }
            const nameA = a[0] && (a[0].title
            ? a[0].title[0].toUpperCase()
            : a[0].name
            ? a[0].name[0].toUpperCase()
            : "");
            const nameB = b[0] && (b[0].title
                ? b[0].title[0].toUpperCase()
                : b[0].name
                ? b[0].name[0].toUpperCase()
                : "");
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
        }
        return 0;
    });

    return (
        <>
            {itemAvailable ? (<Box className="relatedContentSection">
                <Flex alignItems="center" gap="40px" display="flex">
                    <Box className="relatedContentSectionHeader">
                        {intl.formatMessage({ id: "search.related-content" })}
                    </Box>
                </Flex>
                <SimpleGrid columns={[1, 2, 3, 4]} spacing={10} width={"100%"}>
                    {relatedContentItemsList.slice(0, 8).map((e, i) =>
                        e.length > 0 ?
                        (e[0].title || e[0].type || e[0].id ? (
                            <Flex key={i}>
                                <Divider className="relatedContentLine" orientation="vertical" />
                                <RelatedContentEntry {...e[0]} />
                            </Flex>
                        ) : null) : null
                    )}
                </SimpleGrid>
            </Box>) : null}
        </>
    );
};
