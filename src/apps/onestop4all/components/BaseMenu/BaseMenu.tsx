import {
    Box,
    Drawer,
    DrawerContent,
    DrawerOverlay,
    Hide,
    HStack,
    IconButton,
    Link,
    Spacer,
    useDisclosure
} from "@open-pioneer/chakra-integration";
import { useIntl, useService } from "open-pioneer:react-hooks";
import { ReactNode, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { LanguageToggler } from "../Header/LanguageToggler";
import { Login } from "../Header/Login";
import { UserSupportLink } from "../Header/UserSupportLink";
import { MenuCloseIcon } from "../Icons";
import { MenuHandler } from "../../services";

export function BaseMenu() {
    const intl = useIntl();
    const { isOpen, onOpen, onClose } = useDisclosure();
    const navigate = useNavigate();

    const menuHandler = useService("onestop4all.MenuHandler") as MenuHandler;

    useEffect(() => {
        const openMenuListener = menuHandler.on("open-menu", () => onOpen());
        return () => openMenuListener.destroy();
    });

    function createBlock(header: string, children: ReactNode): ReactNode {
        return (
            <Box
                className="block"
                padding={{ base: "10px 20px 10px 20px;", custombreak: "20px 20px;" }}
            >
                <Box
                    className="block-header"
                    fontSize={{ base: "24px", custombreak: "26px" }}
                    paddingBottom={{ base: "10px", custombreak: "10px" }}
                >
                    {header}
                </Box>
                <div className="block-content">{children}</div>
            </Box>
        );
    }

    function goToFaq() {
        onClose();
        navigate("/faq/");
    }

    return (
        <Drawer
            isOpen={isOpen}
            placement="right"
            onClose={onClose}
            size={{ base: "customMenu", custombreak: "xs" }}
            blockScrollOnMount={false}
        >
            <DrawerOverlay bg={"var(--chakra-colors-blackAlpha-200)"} />
            <DrawerContent className="navigation-menu">
                <HStack padding={{ base: "10px 52px 0px", custombreak: "22px 52px 10px" }}>
                    <Spacer></Spacer>
                    <IconButton
                        aria-label="Search database"
                        variant="ghost"
                        colorScheme="teal"
                        icon={<MenuCloseIcon boxSize={8} />}
                        onClick={onClose}
                    />
                </HStack>
                <Box overflow="auto">
                    <Hide above="custombreak">
                        <Box
                            className="initial-block"
                            padding={{ base: "0 70px 10px", custombreak: "0 70px 40px" }}
                            //marginTop={{ base: "0px", custombreak: "0px" }}
                            lineHeight="36px"
                        >
                            {/*<UserSupportLink></UserSupportLink>
                            <Login></Login>*/}
                            {/*<LanguageToggler />*/}
                        </Box>
                    </Hide>
                    <div className="seperator"></div>
                    {createBlock(
                        intl.formatMessage({
                            id: "basemenu.get-connected"
                        }),
                        <>
                            <Link
                                href="https://www.nfdi4earth.de/about-us"
                                target="_blank"
                                rel="noreferrer"
                            >
                                {intl.formatMessage({
                                    id: "basemenu.about-us"
                                })}
                            </Link>
                            <Link
                                href="https://www.nfdi4earth.de/about-us/consortium"
                                target="_blank"
                                rel="noreferrer"
                            >
                                {intl.formatMessage({
                                    id: "basemenu.partners"
                                })}
                            </Link>
                            <Link
                                href="https://www.nfdi4earth.de/2coordinate/coordination-office"
                                target="_blank"
                                rel="noreferrer"
                            >
                                {intl.formatMessage({
                                    id: "basemenu.contact"
                                })}
                            </Link>
                        </>
                    )}
                    <div className="seperator"></div>
                    {createBlock(
                        "Support",
                        <>
                            <UserSupportLink hideIcon={true} header={false} />
                            <Link
                                target="_blank"
                                rel="noreferrer"
                                onClick={() => goToFaq()}
                                style={{ textDecoration: "underline" }}
                            >
                                FAQ
                            </Link>
                        </>
                    )}
                    <div className="seperator"></div>
                    {createBlock(
                        intl.formatMessage({
                            id: "basemenu.legal-information"
                        }),
                        <>
                            <Link
                                href="https://www.nfdi4earth.de/legal-notice"
                                target="_blank"
                                rel="noreferrer"
                            >
                                {intl.formatMessage({
                                    id: "basemenu.legal-information"
                                })}
                            </Link>
                            <Link
                                href="https://www.nfdi4earth.de/privacy-policy"
                                target="_blank"
                                rel="noreferrer"
                            >
                                {intl.formatMessage({
                                    id: "basemenu.privacy"
                                })}
                            </Link>
                        </>
                    )}
                    <div className="seperator"></div>
                </Box>
            </DrawerContent>
        </Drawer>
    );
}
