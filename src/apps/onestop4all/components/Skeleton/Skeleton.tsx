import { Box, Skeleton, Stack } from "@open-pioneer/chakra-integration";

export default function HowToEntryContent() {
    return (
        <Box pt="30px">
            <Box pb="40px">
                <Skeleton height="30px" />
            </Box>
            <Stack>
                <Skeleton height="20px" />
                <Skeleton height="20px" />
                <Skeleton height="20px" />
            </Stack>
        </Box>
    );
}
