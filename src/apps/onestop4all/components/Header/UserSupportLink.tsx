import { Box, HStack, Icon, Link } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";
import { useSearchParams } from "react-router-dom"; 
import { useEffect, useState } from "react";

import { PrimaryColor } from "../../Theme";
import { UserSupportIcon } from "../Icons";
import { SupportForm } from "../SupportForm/SupportForm";

export interface UserSupportLinkProps {
    hideIcon?: boolean;
    header?: boolean;
}

export function UserSupportLink({ hideIcon = false, header = true }: UserSupportLinkProps) {
    const intl = useIntl();
    const [openSupportForm, setOpenSupportForm] = useState(false);
    const [searchParams, setSearchParams] = useSearchParams();

    useEffect(() => {
        if (searchParams.has("support")) {
            setOpenSupportForm(true); 
        }
    }, [searchParams]);

    return (
        <>
            <HStack onClick={() => setOpenSupportForm(true)} _hover={{ cursor: "pointer" }}>
                {!hideIcon && (
                    <Icon boxSize={6} color={PrimaryColor}>
                        <UserSupportIcon />
                    </Icon>
                )}
                <Box hideBelow="custombreak">
                    {header ? (
                        <Link whiteSpace="nowrap">
                            {intl.formatMessage({ id: "header.user-support" })}
                        </Link>
                    ) : (
                        <Link whiteSpace="nowrap" style={{ textDecoration: "underline" }}>
                            {intl.formatMessage({ id: "header.user-support" })}
                        </Link>
                    )}
                </Box>
                <Box hideFrom="custombreak">
                    {header ? (
                        <Link whiteSpace="nowrap">
                            {intl.formatMessage({ id: "header.user-support-mobile" })}
                        </Link>
                    ) : (
                        <Link whiteSpace="nowrap" style={{ textDecoration: "underline" }}>
                            {intl.formatMessage({ id: "header.user-support" })}
                        </Link>
                    )}
                </Box>
            </HStack>
            <SupportForm
                openForm={openSupportForm}
                menuClosed={() => setOpenSupportForm(false)}
            ></SupportForm>
        </>
    );
}
