import { Box, Divider, Hide, HStack, Flex } from "@open-pioneer/chakra-integration";
import { useNavigate } from "react-router-dom";

import { Feedback } from "./Feedback";
import { LanguageToggler } from "./LanguageToggler";
import { Logo } from "./Logo";
import { MenuButton } from "./MenuButton";
import { UserSupportLink } from "./UserSupportLink";

//import { Login } from "./Login";
export const Header = () => {
    const navigate = useNavigate();

    const backToStart = () => {
        navigate("/");
    };

    return (
        <>
            <HStack
                justifyContent="space-between"
                alignItems="center"
                margin="6px 0px"
                padding={{ base: "6px 1px 0px", custombreak: "36px 1px 10px" }}
            >
                <Box _hover={{ cursor: "pointer" }} onClick={backToStart}>
                    <Logo />
                </Box>
                <div id="feedback1">
                    <Feedback fontSize="16pt" />
                </div>
                <Flex gap={{ base: "10px", custombreak: "30px" }}>
                    <UserSupportLink />
                    {/*<LanguageToggler />*/}
                    <MenuButton />
                </Flex>
            </HStack>
            <Divider className="separator" />
            <div id="feedback2">
                <Feedback fontSize="16pt" />
                <Divider className="separator" />
            </div>
        </>
    );
};
