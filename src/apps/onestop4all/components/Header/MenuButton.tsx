import { Box, HStack, Link, Image } from "@open-pioneer/chakra-integration";
import { useService } from "open-pioneer:react-hooks";
import { useIntl } from "open-pioneer:react-hooks";

import { PrimaryColor } from "../../Theme";

export interface MenuButtonProps {
    hideIcon?: boolean;
    header?: boolean;
}

export function MenuButton({ hideIcon = false, header = true }: MenuButtonProps) {
    const intl = useIntl();
    const menuHandler = useService("onestop4all.MenuHandler");
    return (
        <>
            <HStack onClick={() => menuHandler.open()} _hover={{ cursor: "pointer" }}>
                {!hideIcon && (
                    <Image boxSize={6}  color={PrimaryColor} className="bg-icon-05668D" alt="Bg icon 05668D" src="/bg-icon-05668D.png" />
                )}
                <Box hideBelow="custombreak">
                    {header ? (
                        <Link whiteSpace="nowrap">
                            {intl.formatMessage({ id: "header.about-nfdi4earth" })}
                        </Link>
                    ) : (
                        <Link whiteSpace="nowrap" style={{ textDecoration: "underline" }}>
                            {intl.formatMessage({ id: "header.about-nfdi4earth" })}
                        </Link>
                    )}
                </Box>
                <Box hideFrom="custombreak">
                    {header ? (
                        <Link whiteSpace="nowrap">
                            {intl.formatMessage({ id: "header.about-nfdi4earth-mobile" })}
                        </Link>
                    ) : (
                        <Link whiteSpace="nowrap" style={{ textDecoration: "underline" }}>
                            {intl.formatMessage({ id: "header.about-nfdi4earth" })}
                        </Link>
                    )}
                </Box>
            </HStack>
        </>
    );
}
