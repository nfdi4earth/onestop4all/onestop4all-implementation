import { Box, Text} from "@open-pioneer/chakra-integration";
import { useIntl, useService } from "open-pioneer:react-hooks";
import { SearchService } from "../../services";

export const Feedback = (props: { fontSize: string }) => {
    const intl = useIntl();
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;
    const opts = searchSrvc.getFeedbackUrl();

    return (
        <Box w="100%">
            <div style={{ textAlign: "center", fontSize: props.fontSize }}>
                {intl.formatMessage({
                    id: "header.feedback"
                })}
                &nbsp;
                <a href={opts.feedbackUrl} className="link" target="_blank" rel="noreferrer">
                    {intl.formatMessage({
                        id: "header.feedbackLink"
                    })}
                </a>!
            </div>
        </Box>
    );
};
