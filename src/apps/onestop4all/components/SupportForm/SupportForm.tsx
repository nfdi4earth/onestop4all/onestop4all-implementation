import { ServiceOptions } from "@open-pioneer/runtime";
import {
    Box,
    Button,
    Flex,
    FormControl,
    FormLabel,
    Icon,
    Input,
    Modal,
    Select,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    Textarea,
    useDisclosure,
    Text, 
    Link, 
    Image, 
} from "@open-pioneer/chakra-integration";
import { useSearchParams } from "react-router-dom"; 
import { EmailIcon } from "@chakra-ui/icons";
import "altcha";
import { useEffect, useState } from "react";
import { useService } from "open-pioneer:react-hooks";
import { UserSupportIcon } from "../Icons";

import { useIntl } from "open-pioneer:react-hooks";
import { SearchService } from "../../services";

export interface SupportFormProps {
    openForm: boolean;
    menuClosed: () => void;
}

// https://mailtrap.io/blog/react-contact-form/

export const SupportForm = (props: SupportFormProps) => {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const { openForm, menuClosed } = props;
    const searchSrvc = useService("onestop4all.SearchService") as SearchService;

    const intl = useIntl();

    const [isEmailValid, setIsEmailValid] = useState(true);
    const [isNameFilled, setIsNameFilled] = useState(true);
    const [isSubjectFilled, setIsSubjectFilled] = useState(true);
    const [isContentFilled, setIsContentFilled] = useState(true);
    const [status, setStatus] = useState<string>("status");
    const [searchParams, setSearchParams] = useSearchParams();
    const [modalOpened, setModalOpened] = useState(false); 

    function validateEmail(email: string): boolean {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }

    function handleSubmit(event: { preventDefault: () => void; currentTarget: HTMLFormElement | undefined; }): void {
        event.preventDefault();
        const formData : { [key: string]: any } = {};
        for (const [key, value] of new FormData(event.currentTarget).entries()) {
            formData[key] = value;
        }
        const { name, email, subject, content, institution, category, altcha } = formData;
        const isEmailValid = validateEmail(email);
        const isNameFilled = name.trim() !== "";
        const isSubjectFilled = subject.trim() !== "";
        const isContentFilled = content.trim() !== "";

        if (isEmailValid && isNameFilled && isSubjectFilled && isContentFilled) {
            searchSrvc.sendSupportRequest(name, email, subject, content, institution, category, altcha).then((result) => {
                if (result) {
                    setStatus(result);
                }
                if (result.search(/message sent/i) != -1 ) {
                    setTimeout(closeForm, 3000);
                }
            });
        } else {
            setIsEmailValid(isEmailValid);
            setIsNameFilled(isNameFilled);
            setIsSubjectFilled(isSubjectFilled);
            setIsContentFilled(isContentFilled);
        }
    }

    function closeForm(): void {
        setStatus("status"); 
        setIsEmailValid(true);
        setIsNameFilled(true);
        setIsSubjectFilled(true);
        setIsContentFilled(true);

        menuClosed();
        onClose();
        searchParams.delete("support");
        setSearchParams(searchParams);
        setModalOpened(false); 
    }

    useEffect(() => {
        if (openForm) {
            onOpen(); 
        }
    }, [openForm]);

    // intl messages
    const userSupport = intl.formatMessage({ id: "supportform.user-support" });
    const yourName = intl.formatMessage({ id: "supportform.your-name" });

    const emptyName = intl.formatMessage({ id: "supportform.empty-name" });
    const yourMail = intl.formatMessage({ id: "supportform.your-mail" });

    const emptyMail = intl.formatMessage({ id: "supportform.empty-mail" });
    const subject = intl.formatMessage({ id: "supportform.subject" });
    const emptySubject = intl.formatMessage({ id: "supportform.empty-subject" });
    const content = intl.formatMessage({ id: "supportform.content" });
    const emptyContent = intl.formatMessage({ id: "supportform.empty-content" });
    const institution = intl.formatMessage({ id: "supportform.institution" });
    const category = intl.formatMessage({ id: "supportform.category" });

    const choose = intl.formatMessage({ id: "supportform.categories.choose"});
    const repositories = intl.formatMessage({ id: "supportform.categories.repositories"});
    const information = intl.formatMessage({ id: "supportform.categories.information"});
    const tools = intl.formatMessage({ id: "supportform.categories.tools-and-software"});
    const services = intl.formatMessage({ id: "supportform.categories.services"});
    const data_analysis = intl.formatMessage({ id: "supportform.categories.data-analysis"});
    const something_different = intl.formatMessage({ id: "supportform.categories.something-different"});

    const placeholderSubject = intl.formatMessage({ id: "supportform.placeholder.subject"});
    const placeholderName = intl.formatMessage({ id: "supportform.placeholder.name"});
    const placeholderEmail = intl.formatMessage({ id: "supportform.placeholder.email"});
    const placeholderInstitution = intl.formatMessage({ id: "supportform.placeholder.institution"}); 
    const placeholderContent = intl.formatMessage({ id: "supportform.placeholder.content"}); 

    const textQuestion = intl.formatMessage({ id: "supportform.text.question"});
    const textCall = intl.formatMessage({ id: "supportform.text.call"});
    const textexplanation1 = intl.formatMessage({ id: "supportform.text.explanation1"});
    const textexplanation2 = intl.formatMessage({ id: "supportform.text.explanation2"});

    const sendRequest = intl.formatMessage({ id: "supportform.send-request" });
    const closeFormText = intl.formatMessage({ id: "supportform.close-form" });
    const messageSent = intl.formatMessage({ id: "supportform.message-sent" });
    const messageNotSent = intl.formatMessage({ id: "supportform.message-not-sent" });
    const captchaMessage = intl.formatMessage({ id: "supportform.captcha" });
    const altchaMessage = JSON.stringify({label: captchaMessage});
    const altchaChallengeUrl = searchSrvc.sf_config.api.url + searchSrvc.sf_config.api.routes.challenge;
    return (
        <Modal isOpen={isOpen} onClose={closeForm} size={{ base: "sm", md: "2xl", lg: "3xl", xl: "5xl" }}>
            <ModalOverlay />
            <ModalContent>
                <form onSubmit={handleSubmit}>
                    <ModalHeader>
                        <Flex alignItems="center" gap="8px">
                            <Icon boxSize={6} color={"black"}>
                                <UserSupportIcon />
                            </Icon>
                            <Box> {userSupport}</Box>
                        </Flex>
                    </ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Box p={4} display={{ md: "flex" }}>
                            <Box flex={{ base: "1", md: "2" }} mt={{ base: 4, md: 0 }} ml={{ md: 6 }} alignItems="center" textAlign="center" bg="#a4e5ec30" borderRadius="lg">
                                <EmailIcon mt={4} boxSize={20} color="#05668D"/>
                                <Text m={3} color="black">   
                                    {textQuestion}
                                </Text>
                                <Text m={3} color="black" as="b">
                                    {textCall}
                                </Text>
                                <Text mt={3} color="black">
                                    {textexplanation1}
                                    
                                    <Link
                                        m={1}
                                        display="block"
                                        fontSize="lg"
                                        lineHeight="normal"
                                        fontWeight="semibold"
                                        color="#05668D"
                                        target="_blank"
                                        href="https://nfdi4earth.de/2facilitate/user-support-network"
                                    >
                                        NFDI4Earth Helpdesk
                                    </Link>
                                    
                                    {textexplanation2} 
                                </Text>
                                <Box mt="10%" mb="10%" ml={5} mr={5}>
                                    <Image color="#05668D" className="bg-icon-05668D" alt="Bg icon 05668D" src="/bg-icon-05668D.png" display={{ base: "none", md: "block" }} mx="auto" my="auto" alignSelf="center"/>
                                </Box>                             
                            </Box>
                            
                            <Box flex={{ base: "1", md: "3" }} mt={{ base: 4, md: 0 }} ml={{ md: 6 }}>
                                <FormControl>
                                    <FormLabel>{subject}</FormLabel>
                                    <Input
                                        type="text"
                                        name="subject"
                                        isInvalid={!isSubjectFilled}
                                        placeholder={placeholderSubject}
                                    />
                                    {!isSubjectFilled && <p color="red">{emptySubject}</p>}

                                    <FormLabel pt={"1rem"}>{category}</FormLabel>
                                    <Select name="category" placeholder={choose}>
                                        <option value="Repositories">{repositories}</option>
                                        <option value="Information and Metadata">{information}</option>
                                        <option value="Tools and Software">{tools}</option>
                                        <option value="Services">{services}</option>
                                        <option value="Data and Analysis">{data_analysis}</option>
                                        <option value="Something completely different">{something_different}</option>
                                    </Select>

                                    <FormLabel pt={"1rem"}>{yourName}</FormLabel>
                                    <Input
                                        type="text"
                                        name="name"
                                        isInvalid={!isNameFilled}
                                        placeholder={placeholderName}
                                    />
                                    {!isNameFilled && <p color="red">{emptyName}</p>}

                                    <FormLabel pt={"1rem"}>{yourMail}</FormLabel>
                                    <Input
                                        type="email"
                                        name="email"
                                        isInvalid={!isEmailValid}
                                        placeholder={placeholderEmail}
                                    />
                                    {!isEmailValid && <p color="red">{emptyMail}</p>}

                                    <FormLabel pt={"1rem"}>{institution}</FormLabel>
                                    <Input
                                        type="text"
                                        name="institution"
                                        placeholder={placeholderInstitution}
                                    />

                                    <FormLabel pt={"1rem"}>{content}</FormLabel>
                                    <Textarea
                                        name="content"
                                        placeholder={placeholderContent}
                                        isInvalid={!isContentFilled}
                                    />
                                    {!isContentFilled && <p color="red">{emptyContent}</p>}
                                    <altcha-widget
                                        challengeurl={altchaChallengeUrl}
                                        strings={altchaMessage}
                                        hidelogo
                                        hidefooter>
                                    </altcha-widget>
                                </FormControl>
                                {status === "Message sent!" ? (
                                    <b>{messageSent}</b>
                                ) : status.search("Invalid Altcha payload") !== -1 ? (
                                    <p><b>The captcha is invalid OR outdated, please reload the contact form</b><br /><i>Maybe you want to copy your message to the clipboard!?!</i></p>
                                ) : status !== "status" ? (
                                    <b>{messageNotSent}</b>
                                ) : (
                                    ""
                                )}
                            </Box>
                        </Box>
                    </ModalBody>
                    <ModalFooter>
                        <Button type="submit" mr={3}>{sendRequest}</Button>
                        <Button colorScheme="blue" mr={3} onClick={closeForm}>
                            {closeFormText}
                        </Button>
                    </ModalFooter>
                </form>
            </ModalContent>
        </Modal>
    );
};