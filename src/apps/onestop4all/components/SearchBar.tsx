import {
    Box,
    Button,
    HStack,
    VStack,
    Flex,
    IconButton,
    Input,
    Select
} from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";
import { useEffect, useState } from "react";
import { createSearchParams, useLocation, useNavigate } from "react-router-dom";

import { ResourceType } from "../services/ResourceTypeUtils";
import { BorderColor, PrimaryColor } from "../Theme";
import {
    UrlSearchParameterType,
    UrlSearchParams,
    useSearchState
} from "../views/Search/SearchState";
import { DropdownArrowIcon, SearchIcon } from "./Icons";

export function SearchBar() {
    const [searchTerm, setSearchTerm] = useState<string>("");
    const intl = useIntl();
    const [selectedResource, setSelectResource] = useState("");
    const resourceTypes = Object.values(ResourceType).sort((a, b) => a.localeCompare(b));
    const searchState = useSearchState();
    const navigate = useNavigate();
    const location = useLocation();

    useEffect(() => setSearchTerm(searchState.searchTerm), [searchState.searchTerm]);

    useEffect(() => {
        if (
            searchState.selectedResourceTypes.length === 1 &&
            searchState.selectedResourceTypes[0]
        ) {
            setSelectResource(searchState.selectedResourceTypes[0]);
        } else {
            setSelectResource("");
        }
    }, [searchState.selectedResourceTypes]);

    function startSearch(): void {
        searchState.setSearchTerm(searchTerm);
        if (selectedResource === "") {
            searchState.setSelectedResourceTypes([]);
        } else {
            searchState.setSelectedResourceTypes([selectedResource]);
        }
        if (!location.pathname.endsWith("search")) {
            const params: UrlSearchParams = {};
            params[UrlSearchParameterType.Searchterm] = searchTerm;
            if (!selectedResource) {
                searchState.setSelectedResourceTypes([]);
            }
            navigate({
                pathname: "/search",
                search: `?${createSearchParams({ ...params })}`
            });
        }
    }

    function handleKeyDown(key: string): void {
        if (key === "Enter") {
            startSearch();
        }
    }

    return (
        <Box
            borderWidth={{ base: "10px", custombreak: "15px" }}
            borderColor="rgb(5, 102, 141, 0.7)"
        >
            <div id="searchbar">
                <HStack padding={{ base: "5px 10px", custombreak: "8px 15px" }} w="100%" bg="white">
                    <Select
                        icon={<DropdownArrowIcon />}
                        iconSize="12"
                        variant={!navigator.userAgent.includes("Chrome") ? "unstyled" : undefined}
                        textTransform="uppercase"
                        color={PrimaryColor}
                        placeholder={intl.formatMessage({
                            id: "search.search-bar.dropdownPlaceholder"
                        })}
                        borderColor="white"
                        flex={{ base: "0 0 16rem" }}
                        value={selectedResource}
                        onChange={(event) => setSelectResource(event.target.value)}
                        _hover={{ cursor: "pointer" }}
                    >
                        {createResourceTypeSelectOptions()}
                    </Select>
                    <Box flex="0 0 1px" bgColor={BorderColor} alignSelf="stretch" />
                    <Input
                        placeholder={intl.formatMessage({ id: "search.search-bar.placeholder" })}
                        value={searchTerm}
                        onChange={(event) => setSearchTerm(event.target.value)}
                        onKeyDown={(event) => handleKeyDown(event.key)}
                        borderColor="white"
                    />
                    <Button
                        leftIcon={<SearchIcon boxSize={6} />}
                        variant="solid"
                        hideBelow="custombreak"
                        onClick={() => startSearch()}
                    >
                        <Box>{intl.formatMessage({ id: "search.search-bar.button-label" })}</Box>
                    </Button>
                    <IconButton
                        aria-label="start search"
                        size="sm"
                        hideFrom="custombreak"
                        onClick={() => startSearch()}
                        icon={<SearchIcon />}
                    />
                </HStack>
            </div>
            <div id="searchbarResponsive">
                <Flex bg="white" direction={"column"}>
                    <Select
                        icon={<DropdownArrowIcon />}
                        iconSize="12"
                        variant={!navigator.userAgent.includes("Chrome") ? "unstyled" : undefined}
                        textTransform="uppercase"
                        color={PrimaryColor}
                        placeholder={intl.formatMessage({
                            id: "search.search-bar.dropdownPlaceholder"
                        })}
                        borderColor="white"
                        //flex={{ base: "0 0 1rem" }}
                        value={selectedResource}
                        onChange={(event) => setSelectResource(event.target.value)}
                        _hover={{ cursor: "pointer" }}
                        margin={"2% 0% 0% 3%"}
                        w={"max-content"}
                    >
                        {createResourceTypeSelectOptions()}
                    </Select>
                    <Flex alignItems="center">
                        <Input
                            placeholder={intl.formatMessage({
                                id: "search.search-bar.placeholder-mobile"
                            })}
                            value={searchTerm}
                            onChange={(event) => setSearchTerm(event.target.value)}
                            onKeyDown={(event) => handleKeyDown(event.key)}
                            //borderColor="white"
                            margin={"2% 2% 2% 2%"}
                            outline={"1px solid #05668d"}
                        />
                        <Button
                            leftIcon={<SearchIcon boxSize={6} />}
                            variant="solid"
                            hideBelow="custombreak"
                            onClick={() => startSearch()}
                        >
                            <Box>
                                {intl.formatMessage({ id: "search.search-bar.button-label" })}
                            </Box>
                        </Button>
                        <IconButton
                            aria-label="start search"
                            size="sm"
                            hideFrom="custombreak"
                            onClick={() => startSearch()}
                            icon={<SearchIcon />}
                            marginRight={"1%"}
                        />
                    </Flex>
                </Flex>
            </div>
        </Box>
    );

    function createResourceTypeSelectOptions() {
        return resourceTypes.map((e, i) => (
            <option value={e} key={i}>
                {intl.formatMessage({ id: `resource-types.${e}` })}
            </option>
        ));
    }
}
