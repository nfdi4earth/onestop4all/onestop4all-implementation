import { Box, Flex } from "@open-pioneer/chakra-integration";
import { useIntl } from "open-pioneer:react-hooks";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useSearchState } from "../../views/Search/SearchState";

import { BackIcon } from "../Icons";

export function BackToSearchLink(props: { visible: boolean, prevPath?: string }) {
    const { visible, prevPath } = props;
    const navigate = useNavigate();
    const searchState = useSearchState();

    useEffect(() => {
        if (prevPath) {
            searchState.setPrevPath(prevPath);
        }
    }, [prevPath]);

    function backToSearch() {
        navigate({ pathname: searchState.prevPath });
    }
    const intl = useIntl();
    const backText = intl.formatMessage({ id: "resource-type-header.back" });
    const toResultListText = intl.formatMessage({ id: "resource-type-header.to-result-list" });

    return visible ? (
        <Flex
            fontSize="14px"
            textTransform="uppercase"
            letterSpacing="0.6px"
            onClick={backToSearch}
            gap="12px"
            _hover={{ cursor: "pointer" }}
        >
            <Box onClick={backToSearch} _hover={{ cursor: "pointer" }}>
                <BackIcon />
            </Box>
            <Box>
                <Box display="inline" fontWeight="700" color="var(--primary-primary-main)">
                    {backText}&nbsp;
                </Box>
                <Box whiteSpace="nowrap" display="inline">
                    {toResultListText}
                </Box>
            </Box>
        </Flex>
    ) : (
        <></>
    );
}
