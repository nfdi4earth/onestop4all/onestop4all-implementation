import { Box, Flex } from "@open-pioneer/chakra-integration";
import { useNavigate } from "react-router-dom";
import { useIntl } from "open-pioneer:react-hooks";

import { BackIcon } from "../../components/Icons";

export const BackToStartingpage = () => {
    const intl = useIntl();
    const navigate = useNavigate();

    function backToStart() {
        navigate({ pathname: "/" });
    }

    return (
        <Flex alignItems="center" display="flex" gap="12px" height="32px">
            <Box onClick={backToStart} _hover={{ cursor: "pointer" }}>
                <BackIcon />
            </Box>
            <Box
                className="resTypeHeaderBackBtn"
                onClick={backToStart}
                _hover={{ cursor: "pointer" }}
            >
                <span className="to">
                    {intl.formatMessage({
                        id: "start.back-to-startingpage.back"
                    })}
                    &nbsp;
                </span>
                {intl.formatMessage({
                    id: "start.back-to-startingpage.to"
                })}
            </Box>
        </Flex>
    );
};
