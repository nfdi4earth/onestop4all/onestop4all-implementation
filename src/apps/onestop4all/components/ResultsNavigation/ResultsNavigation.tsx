import { useEffect, useState } from "react";
import { Box, Flex, SystemStyleObject } from "@open-pioneer/chakra-integration";

import {
    ResultsNavigationLeft,
    ResultsNavigationLeftLeft,
    ResultsNavigationRight,
    ResultsNavigationRightRight
} from "../Icons";

import { useIntl } from "open-pioneer:react-hooks";

export interface ResultsNavigationProps {
    result: number;
    of: number;
    label?: string;
    refer?:any;
    stepBack?: () => void;
    stepToStart?: () => void;
    stepFoward?: () => void;
    stepToEnd?: () => void;
}

export const ResultsNavigation = (props: ResultsNavigationProps) => {
    const intl = useIntl();
    const {
        result,
        of,
        label = intl.formatMessage({ id: "search.result" }),
        refer,
        stepBack,
        stepFoward,
        stepToEnd,
        stepToStart,
    } = props;
    const ofText = intl.formatMessage({ id: "search.of" });

    const canStepBack = result > 1;
    const canStepForward = result < of;

    const navigationHoverStyle: SystemStyleObject = {
        cursor: "pointer"
    };

    function jumpToElement(elem:any) {
        if (elem) {
            elem.current.scrollIntoView({ behavior: "smooth", block: "center" });
        } else {
            window.scrollTo(0, 0);
        }
    }

    return (
        <Flex alignItems="center">
            <Box
                _hover={canStepBack ? navigationHoverStyle : {}}
                onClick={() => {
                    canStepBack && stepToStart && stepToStart();
                    jumpToElement(refer);
                }}
                opacity={canStepBack ? "1" : "0.3"}
            >
                <ResultsNavigationLeftLeft />
            </Box>

            <Box className="resultsNavigationLine seperator" mr="16px" />

            <Box
                _hover={canStepBack ? navigationHoverStyle : {}}
                onClick={() => {
                    canStepBack && stepBack && stepBack();
                    jumpToElement(refer);
                }}
                opacity={canStepBack ? "1" : "0.3"}
            >
                <ResultsNavigationLeft />
            </Box>

            <Box className="resultsNavigationLine seperator" mr="16px" />

            <Box className="resultsNavigationText">
                {label} <span className="resultHit">{result}</span> {ofText}{" "}
                <span className="resultHit">{of}</span>
            </Box>

            <Box className="resultsNavigationLine seperator" ml="16px" />

            <Box
                _hover={canStepForward ? navigationHoverStyle : {}}
                onClick={() => {
                    canStepForward && stepFoward && stepFoward();
                    jumpToElement(refer);
                }}
                opacity={canStepForward ? "1" : "0.3"}
            >
                <ResultsNavigationRight />
            </Box>

            <Box className="resultsNavigationLine seperator" ml="16px" />

            <Box
                _hover={canStepForward ? navigationHoverStyle : {}}
                onClick={() => {
                    canStepForward && stepToEnd && stepToEnd();
                    jumpToElement(refer);
                }}
                opacity={canStepForward ? "1" : "0.3"}
            >
                <ResultsNavigationRightRight />
            </Box>
        </Flex>
    );
};
