# at least one empty yaml file must exist for each supported language
# (might be empty if all messages are defined in the packages used by this app)
messages:
  header:
    about-nfdi4earth: "About NFDI4Earth"
    about-nfdi4earth-mobile: "NFDI4Earth"
    user-support: "Nutzer-Support"
    user-support-mobile: "Support"
    login: "Login"
    feedback: "Der OneStop4All ist eine Beta-Version."
    feedbackLink: "Bitte teilen Sie es uns hier mit"

  footer:
    get-connected: NFDI4Earth
    about: Informationen über uns
    partners: Partner
    contact: Kontakt
    legal-information: Rechtliche Informationen
    privacy: Datenschutz
  faq:
    faq: Häufig gestellte Fragen (FAQ)
    desc: Hier finden Sie eine Liste an häufig gestellten Fragen, die vom NFDI4Earth User Support Network zusammengestellt wurden.
    copy: Link kopieren
  search:
    search-bar:
      button-label: "Suche"
      placeholder: "Suche nach Datenbeständen, Software-Tools, Informationen oder Forschungsdatenmaterial"
      placeholder-mobile: "Suche nach Forschungsdatenmaterial"
      dropdownPlaceholder: "Alle Ressourcen"
    results-header: "Ergebnisse für Ihre Suche"
    result-count-selector: "ERGEBNISSE/SEITE:"
    results: "ERGEBNISSE"
    result: "Ergebnis"
    of: "von"
    page: "Seite"
    last-update: "Letztes Update:"
    related-content: "Verwandte Inhalte"
    visit: "Gehe zu"
    sorted-by: "SORTIERUNG"
    relevance: "Relevanz"
    title-az: "Titel (A-Z)"
    title-za: "Titel (Z-A)"
    facets:
      resource-type: "Ressourcentyp"
      subject: "Thema"
      spatial-coverage: "Ort"
      spatial-coverage-disabled: "Der Ort-Filter ist deaktiviert, da die aktuelle Auswahl keine räumlichen Informationen enthält."
      temporal-coverage-disabled: "Der Zeitraum-Filter ist deaktiviert, da die aktuelle Auswahl keine zeitlichen Informationen enthält."
      set-search-area: "Suchbereich eingrenzen"
      del-search-area: "Suchbereich entfernen"
      temporal-coverage: "Publikationszeitpunkt"
      set-timespan: "Zeitraum eingrenzen"
      sel-timespan: "Zeitraum entfernen"
      search-term: "Suchbegriff"
      data-access-type: "Zugriff auf Daten"
      data-upload-type: "Hochladen von Daten"
      country: "Land"
      publisher: "Herausgeber"
      showMore: "Zeige mehr"
      showLess: "Zeige weniger"
      api: "API"
      dataLicense: "Datenlizenz"
      supported-metadata-standards: "Unterstützte Metadaten Standards"
      licenses: "Lizenzen"
      license: "Lizenz"
      supported-metadata-standard: "Unterstützter Metadaten-Standard"
      data-format-type: "Datenformat"
  start:
    banner:
      slogan: "Ihre zentrale Anlaufstelle für FAIRes, offenes und innovatives Forschungsdatenmanagement in den <bold>Erdsystemwissenschaften</bold>."
    mission:
      links:
        NFDI4EarthLinkUrl: https://www.nfdi4earth.de/
        NFDI4EarthLinkLabel: NFDI4Earth
        MissionLinkUrl: https://www.nfdi4earth.de/about-us
        MissionLinkLabel: Mission
        ResourcesLinkUrl: /
        ResourcesLinkLabel: Ressourcen
    how-to:
      title: Wir bieten Beratung zum Forschungsdatenmanagement im ESS
      learn-more-button: Mehr erfahren
    resources:
      title: Wir bieten eine harmonisierte Sicht auf verknüpfte ESS-Ressourcen
      links:
        MembersLinkUrl: https://www.nfdi4earth.de/about-us/consortium
        MembersLinkLabel: NFDI4Earth-Mitgliedern
        CommunityLinkUrl: https://www.nfdi4earth.de/2participate/pilots
        CommunityLinkLabel: ESS-Community
    back-to-startingpage:
      back: Zurück
      to: zur Startseite
    graph:
      title: Wir zeigen, wie die Ressourcen miteinander verbunden sind
      description: Hier erhalten Sie einen grafischen Überblick über verwandte Ressourcen, um mit der Erkundung zu beginnen.
    get-involved:
      title: Wir begrüßen Ihre Teilnahme
  basemenu:
    get-connected: "NFDI4Earth"
    about-us: "Über uns"
    partners: "Partner"
    contact: "Kontakt"
    legal-information: "Rechtliche Informationen"
    privacy: "Datenschutz"
  supportform:
    captcha: "Ich bin kein Roboter."
    user-support: "Nutzer-Support"
    user-support-mobile: "Support"
    your-name: "Name"
    your-mail: "E-Mail-Adresse"
    subject: "Betreff"
    content: "Nachricht"
    empty-name: "Name darf nicht leer sein."
    empty-mail: "Bitte geben Sie eine gültige E-Mail-Adresse an."
    empty-subject: "Betreff darf nicht leer sein."
    empty-content: "Inhalt darf nicht leer sein."
    institution: "Einrichtung"
    category: "Kategorie"
    send-request: "Anfrage senden"
    close-form: "Formular schließen"
    message-sent: "Die Nachricht wurde gesendet! Sie können das Formular nun schließen."
    message-not-sent: "Die Nachricht konnte nicht gesendet werden. Bitte kontaktieren Sie einen Admin."
    categories:
      repositories: "Repositories"
      information: "Informationen und Metadaten"
      tools-and-software: "Tools und Software"
      services: "Services"
      data-analysis: "Daten und Analysierung"
      something-different: "Etwas anderes"
      choose: "Auswählen einer Kategorie"
    placeholder: 
      subject: "Nennen Sie den Grund für die Kontaktaufnahme"
      name: "Nennen Sie Ihren vollen Namen"
      email: "Nennen Sie Ihre Email-Adresse"
      institution: "Nennen Sie Ihre Institution"
      content: "Beschreiben Sie im Detail die Gründe für die Kontaktaufnahme" 
    text: 
      question: "Auf der Suche nach Unterstützung oder Hilfe?"
      call: "Zögern Sie nicht Kontakt aufzunehmen!"
      explanation1: "Ihre Anfrage wird weitergeleitet an den"
      explanation2: "und wird von einer Person mit hoher Expertise im jeweiligen Themengebiet bearbeitet."
  resource-types:
    Learning Resource: "Lernressource"
    Living Handbook Article: "Living Handbook-Artikel"
    Organisation: "Organisation"
    Repository & Archive: "Repository & Archiv"
    Publication: "Publikation"
    Standard: "Standard"
    Tool & Software: "Tool & Software"
    Dataset: "Datensatz"
    Data Service: "Datenservice"
  resource-types-plural:
    Learning Resource: "Lernressourcen"
    Living Handbook Article: "Living Handbook-Artikel"
    Organisation: "Organisationen"
    Repository & Archive: "Repositorien & Archive"
    Publication: "Publikationen"
    Standard: "Standards"
    Tool & Software: "Tools & Software"
    Dataset: "Datensätze"
    Data Service: "Datenservices"
  resource-type-header:
    back: "Zurück"
    to-result-list: "zur Ergebnisliste"
    loading: "Lädt..."
  metadata:
    name: "Metadaten"
    authors: "Autoren"
    author: "Autor"
    keywords: "Schlagworte"
    keyword: "Schlagwort"
    languages: "Sprachen"
    language: "Sprache"
    hasCertificate: "Zertifikat"
    hasCertificates: "Zertifikate"
    contentType: "Inhaltsart"
    contentTypes: "Inhaltsarten"
    supportsVersioning: "Unterstützt Versionierung"
    type: "Typ"
    types: "Typen"
    published: "Veröffentlicht"
    licenses: "Lizenzen"
    license: "Lizenz"
    add-types: "Zusätzliche Typen"
    add-type: "Zusätzlicher Typ"
    visit-source: "Metadaten-Quelle besuchen"
    open: "Alle Metadataen anzeigen"
    close: "Weniger anzeigen"
    publishers: "Herausgeber"
    publisher: "Herausgeber"
    publishers-alt: "Alternative Herausgeber Namen"
    publisher-alt: "Alternativer Herausgeber Namen"
    learning-res-types: "Ressourcenarten"
    learning-res-type: "Ressourcenart"
    competency: "Benötigte Vorkenntnisse"
    about: "Über"
    visit-website: "Webseite besuchen"
    copy-url: "URL kopieren"
    desc: "Beschreibung"
    target-groups: "Zielgruppen"
    target-group: "Zielgruppe"
    audiences: "Publikum"
    audience: "Publikum"
    part-of: "Ist Teil von"
    has-parts: "Hat Teile"
    has-part: "Hat Teil"
    urls: "URLs"
    url: "URL"
    alt-names: "Alternative Namen"
    alt-name: "Alternativer Name"
    same-as: "Gleich wie"
    alt-labels: "Alternative Repository Namen"
    alt-label: "Alternativer Repository Namen"
    country-names: "Ländernamen"
    country-name: "Ländername"
    localities: "Orte"
    locality: "Ort"
    sub-orga-of: "Suborganisation von"
    nfdi-contact: "NFDI-Kontakt"
    location: "Standort"
    go-to-location: "Zum Standort gehen"
    themes: "Themen"
    theme: "Thema"
    contact-mails: "Kontakt-E-Mails"
    contact-mail: "Kontakt-E-Mail"
    contact-urls: "Kontakt-URLs"
    contact-url: "Kontakt-URL"
    catalog-access-types: "Katalog-Zugriffsarten"
    catalog-access-type: "Katalog-Zugriffsart"
    catalog-licenses: "Katalog-Lizenzen"
    catalog-license: "Katalog-Lizenz"
    catalog-access-restrictions: "Katalog-Zugriffsbeschränkungen"
    catalog-access-restriction: "Katalog-Zugriffsbeschränkung"
    data-access-types: "Datenzugriffstypen"
    data-access-type: "Datenzugriffstyp"
    data-licenses: "Datenlizenzen"
    data-license: "Datenlizenz"
    data-upload-restrictions: "Datenupload-Beschränkungen"
    data-upload-restriction: "Datenupload-Beschränkung"
    data-access-restrictions: "Datenzugriffs-Beschränkungen"
    data-access-restriction: "Datenzugriffs-Beschränkung"
    data-upload-types: "Datenuploadtypen"
    data-upload-type: "Datenuploadtyp"
    supports-metadata-standards: "Unterstützt Metadaten-Standards"
    supports-metadata-standard: "Unterstützt Metadaten-Standard"
    assigns-identifier-schemes: "Weist Identifikationsschemata zu"
    assigns-identifier-scheme: "Weist Identifikationsschema zu"
    visit-repo: "Repository besuchen"
    visit-project-page: "Projektseite besuchen"
    visit-code-repo: "Code-Repository besuchen"
    visit-dataset: "Datensatz besuchen"
    view-specification: "Spezifikation ansehen"
    view-distribution: "Distribution ansehen"
    open-user-policy: "Offene User-policy"
    programming-languages: "Programmiersprachen"
    programming-language: "Programmiersprache"
    read-article: "Artikel lesen"
    endpoint-url: "Endpoint URL"
    endpoint-description: "Endpoint description"
    conforms-to: "Konformität"
    landing-page: "Landing page"
    version: "Version"
    repositoryType: "Repositorientyp"
    repositoryTypes: "Repositorytypen"
    subjectArea: "Fach"
    subjectAreas: "Fächer"
  support:
    header: "Sie suchen noch? Wir helfen Ihnen!"
    text: "Wenn dieser Artikel nicht hilfreich ist oder Ihre Frage nicht beantwortet, unterstützen wir Sie gerne."
    button: "Nutzer-Support kontaktieren"
  error:
    page-not-found: "404 - Seite nicht gefunden"
    oops: "Ups! Es sieht so aus, als ob Sie eine Seite erreicht haben, die nicht existiert."
    return-to-home: "Auf die Startseite zurückkehren:"
    go-to-home: "Zurück zur Startseite"
  actionButton:
    header: "Weitere links"
